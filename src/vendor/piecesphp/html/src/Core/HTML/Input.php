<?php

/**
 * Input.php
 */
namespace PiecesPHP\Core\HTML;

use PiecesPHP\Core\HTML\Attribute;

/**
 * Input - Input html
 *
 * Funciona como módulo independiente
 * @category     HTML
 * @package     PiecesPHP\Core
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @version     v.1
 * @copyright   Copyright (c) 2018
 * @info Funciona como módulo independiente
 */
class Input extends \PiecesPHP\Core\HTML\HtmlElement implements \PiecesPHP\Core\HTML\Interfaces\FormElement
{
    /**
     * $type
     *
     * @var string
     */
    protected $type = 'text';
    /**
     * $value
     *
     * @var string
     */
    protected $value = '';
    /**
     * $name
     *
     * @var string
     */
    protected $name = '';
    /**
     * $placeHolder
     *
     * @var string
     */
    protected $placeHolder = '';

    /**
     * __construct
     *
     * @param string $type
     * @param string $value
     * @param string $name
     * @param string $placeHolder
     * @param Attribute|array $attributes Objeto Attribute o un array asociativo como el siguiente:
     * ['attributo'=>'value'] | ['attributo'=>['value','value2'...]] [Attribute,...]
     * @return void
     */
    public function __construct(string $name = '', string $value = null, string $type = null, string $placeHolder = null, $attributes = null)
    {

        parent::__construct('input', '', null, $attributes, false);

        $this->name = $name;

        $this->value = $value;

        if ($type !== null) {
            $this->type = $type;
        }

        $this->placeHolder = $placeHolder;

        $this->setAttribute('name', $this->name);

        $this->setAttribute('type', $this->type);

        if ($this->value !== null) {
            $this->setAttribute('value', $this->value);
        }

        if ($this->placeHolder !== null) {
            $this->setAttribute('placeholder', $this->placeHolder);
        }

    }

    /**
     * setType
     *
     * @param string $type
     * @return void
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }

    /**
     * setValue
     *
     * @param string $value
     * @return void
     */
    public function setValue(string $value)
    {
        $this->value = $value;
    }

    /**
     * getType
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * getValue
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }
}
