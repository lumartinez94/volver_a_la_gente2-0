<?php

/**
 * Button.php
 */
namespace PiecesPHP\Core\HTML;

/**
 * Button - Button html
 * 
 * Funciona como módulo independiente
 * @category 	HTML
 * @package     PiecesPHP\Core
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @version     v.1
 * @copyright   Copyright (c) 2018
 * @info Funciona como módulo independiente
 */
class Button extends \PiecesPHP\Core\HTML\HtmlElement implements \PiecesPHP\Core\HTML\Interfaces\FormElement
{
	/**
	 * $type
	 *
	 * @var string
	 */
	protected $type = 'submit';


	/**
	 * __construct
	 *
	 * @param string $text
	 * @param string $type
	 * @param Attribute|array $attributes Objeto Attribute o un array asociativo como el siguiente:
	 * ['attributo'=>'value'] | ['attributo'=>['value','value2'...]] [Attribute,...]
	 * @return void
	 */
	public function __construct(string $text = '', string $type = null, $attributes = null)
	{

		parent::__construct('button',$text,null,$attributes);

		if ($type !== null) {
			$this->type = $type;
		}

		$this->setAttribute('type', $this->type);

	}

	/**
	 * setType
	 *
	 * @param string $type
	 * @return void
	 */
	public function setType(string $type){
		$this->type = $type;
	}

	/**
	 * getValue
	 *
	 * @return string
	 */
	public function getValue()
	{
		return $this->text;
	}
}
