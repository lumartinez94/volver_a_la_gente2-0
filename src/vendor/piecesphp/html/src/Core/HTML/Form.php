<?php

/**
 * Element.php
 */
namespace PiecesPHP\Core\HTML;

use PiecesPHP\Core\HTML\Attribute;
use PiecesPHP\Core\HTML\Button;
use PiecesPHP\Core\HTML\Collections\FormElementArray;
use PiecesPHP\Core\HTML\HtmlElement;
use PiecesPHP\Core\HTML\Input;
use PiecesPHP\Core\HTML\Interfaces\FormElement;
use PiecesPHP\Core\HTML\Select;

/**
 * Element - Form html
 *
 * Funciona como módulo independiente
 * @category     HTML
 * @package     PiecesPHP\Core
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @version     v.1
 * @copyright   Copyright (c) 2018
 * @info Funciona como módulo independiente
 */
class Form extends \PiecesPHP\Core\HTML\HtmlElement
{
    const ENCTYPE_FORM_URLENCODED = 'application/x-www-form-urlencoded';
    const ENCTYPE_FORM_DATA = 'multipart/form-data';
    const ENCTYPE_TEXT_PLAIN = 'text/plain';

    /**
     * $enctype
     *
     * @var string
     */
    protected $enctype = self::ENCTYPE_FORM_URLENCODED;

    /**
     * $action
     *
     * @var string
     */
    protected $action = '';

    /**
     * $method
     *
     * @var string
     */
    protected $method = 'GET';

    /**
     * __construct
     *
     * @param string $method
     * @param string $action
     * @param string $enctype
     * @param FormElementArray $fields
     * @param mixed $attributes
     * @return void
     */
    public function __construct(string $method = 'GET', string $action = '', string $enctype = 'application/x-www-form-urlencoded', FormElementArray $fields = null, $attributes = null)
    {
        $fields = !is_null($fields) ? $fields->getArrayCopy() : null;

        parent::__construct('form', '', $fields, $attributes);

        $this->method = $method;

        $this->action = $action;

        $this->setAttribute('method', $this->method);

        $this->setAttribute('action', $this->action);
    }

    /**
     * appendFieldSemantic
     *
     * Crea un contenedor de campos al estilo semantic.
     *
     * @param FormElement $element
     * @param string $textLabel
     * @param bool $required
     * @param bool $disabled
     * @return void
     */
    public function appendFieldSemantic(FormElement $element, string $textLabel = null, bool $required = false, bool $disabled = false)
    {
        $field = new HtmlElement('div');
        $classes = ['field'];

        if ($textLabel !== null) {
            $field->appendChild(self::label($textLabel));
        }

        if ($required) {
            $classes[] = 'required';
            $element->setAttribute('required', '');
        }

        if ($disabled) {
            $classes[] = 'disabled';
            $element->setAttribute('disabled', '');
        }

        $field->setAttribute('class', $classes);

        $field->appendChild($element);

        $this->appendChild($field);

    }

    /**
     * label
     *
     * @param string $text
     * @param string $for
     * @param mixed $attributes
     * @return HtmlElement
     */
    public static function label(string $text, string $for = null, $attributes = null)
    {
        $label = HtmlElement::create('label', $text, null, $attributes);
        if ($for !== null) {
            $label->setAttribute('for', $for);
        }
        return $label;
    }

    /**
     * submit
     *
     * @param string $text
     * @param string $class
     * @param Attribute|array $attributes Objeto Attribute o un array asociativo como el siguiente:
     * ['attributo'=>'value'] | ['attributo'=>['value','value2'...]] [Attribute,...]
     * @return Input
     */
    public static function submit(string $text, string $class = '', $attributes = null)
    {
        $button = new Button($text, 'submit', $attributes);
        if (strlen($class) > 0) {
            $button->setAttribute('class', $class);
        }
        return $button;
    }

    /**
     * text
     *
     * @param string $name
     * @param string $value
     * @param string $placeHolder
     * @param Attribute|array $attributes Objeto Attribute o un array asociativo como el siguiente:
     * ['attributo'=>'value'] | ['attributo'=>['value','value2'...]] [Attribute,...]
     * @return Input
     */
    public static function text(string $name = '', string $value = null, string $placeHolder = null, $attributes = null)
    {
        return (new Input($name, $value, 'text', $placeHolder, $attributes));
    }

    /**
     * hidden
     *
     * @param string $name
     * @param string $value
     * @param string $placeHolder
     * @param Attribute|array $attributes Objeto Attribute o un array asociativo como el siguiente:
     * ['attributo'=>'value'] | ['attributo'=>['value','value2'...]] [Attribute,...]
     * @return Input
     */
    public static function hidden(string $name = '', string $value = null, string $placeHolder = null, $attributes = null)
    {
        return (new Input($name, $value, 'hidden', $placeHolder, $attributes));
    }

    /**
     * email
     *
     * @param string $name
     * @param string $value
     * @param string $placeHolder
     * @param Attribute|array $attributes Objeto Attribute o un array asociativo como el siguiente:
     * ['attributo'=>'value'] | ['attributo'=>['value','value2'...]] [Attribute,...]
     * @return Input
     */
    public static function email(string $name = '', string $value = null, string $placeHolder = null, $attributes = null)
    {
        return (new Input($name, $value, 'email', $placeHolder, $attributes));
    }

    /**
     * password
     *
     * @param string $name
     * @param string $value
     * @param string $placeHolder
     * @param Attribute|array $attributes Objeto Attribute o un array asociativo como el siguiente:
     * ['attributo'=>'value'] | ['attributo'=>['value','value2'...]] [Attribute,...]
     * @return Input
     */
    public static function password(string $name = '', string $value = null, string $placeHolder = null, $attributes = null)
    {
        return (new Input($name, $value, 'password', $placeHolder, $attributes));
    }

    /**
     * select
     *
     * @param string $name
     * @param string $selectedValue
     * @param string $defaultText
     * @param string $defaultValue
     * @param array $options
     * @param Attribute|array $attributes Objeto Attribute o un array asociativo como el siguiente:
     * ['attributo'=>'value'] | ['attributo'=>['value','value2'...]] [Attribute,...]
     * @return Select
     */
    public static function select(string $name, string $selectedValue = null, string $defaultText = 'Seleccione un elemento', string $defaultValue = '', array $options = [], $attributes = null)
    {
        $select = new Select($name, $selectedValue, $defaultText, $defaultValue, $attributes);

        if (count($options) > 0) {
            $select->setOptions($options);
        }

        return $select;
    }
}
