<?php

/**
 * Select.php
 */
namespace PiecesPHP\Core\HTML;

use PiecesPHP\Core\HTML\Interfaces\FormElement;

/**
 * Select - Select html
 *
 * Funciona como módulo independiente
 * @category     HTML
 * @package     PiecesPHP\Core
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @version     v.1
 * @copyright   Copyright (c) 2018
 * @info Funciona como módulo independiente
 */
class Select extends HtmlElement implements FormElement
{

    /**
     * $name
     *
     * @var string
     */
    protected $name = '';

    /**
     * $defaultOption
     *
     * @var HtmlElement
     */
    protected $defaultOption = null;

    /**
     * $selectedValue
     *
     * @var string
     */
    protected $selectedValue = null;

    /**
     * $hasSelectedValue
     *
     * @var boolean
     */
    protected $hasSelectedValue = false;

    /**
     * __construct
     *
     * @param string $name
     * @param string $selectedValue
     * @param string $defaultText
     * @param string $defaultValue
     * @param Attribute|array $attributes Objeto Attribute o un array asociativo como el siguiente:
     * ['attributo'=>'value'] | ['attributo'=>['value','value2'...]] [Attribute,...]
     * @return void
     */
    public function __construct(string $name = '', string $selectedValue = null, string $defaultText = '', string $defaultValue = '', $attributes = null)
    {
        parent::__construct('select', '', null, $attributes);

        $this->name = $name;
        $this->selectedValue = $selectedValue;

        $this->setAttribute('name', $this->name);

        $option = new parent('option', $defaultText, null, [
            'value' => $defaultValue,
        ]);

        $this->defaultOption = $option;

        $this->appendChild($option);
    }

    /**
     * setOptions
     *
     * @param array $options ['text'=>'value',...]
     * @return this
     * @throws Exception Si El array no corresponde a la estructura esperada
     */
    public function setOptions(array $options = [])
    {

        $this->clearChilds();

        $this->appendChild($this->defaultOption);

        foreach ($options as $text => $value) {
            if (!is_string($text) && !is_scalar($value)) {
                throw new \Exception(__('exceptions', 'unexpected_type'));
            }
            $this->appendOption($text, $value);
        }

        return $this;
    }

    /**
     * appendOption
     *
     * @param string $text
     * @param string $value
     * @return void
     */
    public function appendOption(string $text, string $value)
    {
        $selected = false;

        $attrs['value'] = $value;

        if ($this->selectedValue == $value && !$this->hasSelectedValue) {
            $selected = true;
            $this->hasSelectedValue = true;
            $attrs['selected'] = 'true';
        }

        $option = new parent('option', $text, null, $attrs);

        $this->appendChild($option);
    }
}
