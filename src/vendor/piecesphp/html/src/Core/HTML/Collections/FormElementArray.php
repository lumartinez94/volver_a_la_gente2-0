<?php

/**
 * FormElement.php
 */
namespace PiecesPHP\Core\HTML\Collections;

use PiecesPHP\Core\DataStructures\ArrayOf;
use PiecesPHP\Core\HTML\Interfaces\FormElement;

/**
 * FormElementArray - Array de FormElement
 *
 * @category     HTML/Collections
 * @package     PiecesPHP\Core
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @version     v.1
 * @copyright   Copyright (c) 2018
 */
class FormElementArray extends ArrayOf
{
    public function __construct($input = [])
    {
        parent::__construct($input, self::TYPE_OBJECT, FormElement::class);
    }
}
