<?php

/**
 * FormElement.php
 */
namespace PiecesPHP\Core\HTML\Interfaces;

use PiecesPHP\Core\HTML\Interfaces\Element;

/**
 * FormElement - Input html
 * 
 * @category 	HTML
 * @package     PiecesPHP\Core
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @version     v.1
 * @copyright   Copyright (c) 2018
 */
interface FormElement extends Element
{
}
