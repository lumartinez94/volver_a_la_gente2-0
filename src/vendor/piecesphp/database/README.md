#piecesphp/database
- Interaction with databases.

##Namespace
- PiecesPHP/Core/Database

##Classes
- ActiveRecordModel
    - Used packages:
        - lezhnev74/pasvl
- EntityMapper
    - Used classes:
        - ActiveRecordModel
- SchemeCreator
    - Used classes:
        - ActiveRecordModel
        - EntityMapper

##API
TODO