<?php

namespace Mappers;

use PiecesPHP\Core\Database\Exceptions\DatabaseClassesExceptions;
use PiecesPHP\Core\Database\SchemeCreator;

error_reporting(E_ALL);
ini_set('display_errors', true);

require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/MainTableMapper.php';
require_once __DIR__ . '/SecondTableMapper.php';
require_once __DIR__ . '/SerializableClass.php';

try {

    $options_connection = [ //Configuración de la conexión
        'driver' => 'mysql',
        'database' => "pcs_databases",
        'user' => "admin",
    ];

    MainTableMapper::setOptions($options_connection); //Establecer las configuraciones
    SecondTableMapper::setOptions($options_connection); //Establecer las configuraciones

    $creator = new SchemeCreator(new SecondTableMapper());

    header('Content-Type: text/plain');

    echo $creator->create();

} catch (DatabaseClassesExceptions $e) {
    header('Content-Type: application/json');

    echo json_encode([
        $e->getMessage(),
        $e->getLine(),
        $e->getCodeString(),
    ]);

    die;
}
