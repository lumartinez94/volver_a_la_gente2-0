<?php

/**
 * ActiveRecordModel.php
 */
namespace PiecesPHP\Core\Database;

use PiecesPHP\Core\Database\Enums\CodeStringExceptionsEnum;
use PiecesPHP\Core\Database\Exceptions\DatabaseClassesExceptions;
use PiecesPHP\Core\Database\Exceptions\OptionPatternMismatchException;
use PiecesPHP\Core\Database\Exceptions\RequiredOptionMissedException;
use \PASVL\Traverser\VO\Traverser;
use \PASVL\ValidatorLocator\ValidatorLocator;
use \PDO;

/**
 * ActiveRecordModel - Implementación básica de modelo ActiveRecord.
 *
 * @package     PiecesPHP\Core\Database
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @copyright   Copyright (c) 2018
 */
class ActiveRecordModel implements \JsonSerializable
{

    /** @var string Nombre de la tabla */
    protected $table = null;

    /**
     * $prefix_table
     *
     * Prefijo de la tabla
     *
     * @var string
     */
    protected $prefix_table = null;

    /** @var array $fields Array que representa las columnas con la estructura ['columna1','columna2'[,...]] */
    private $fields = null;

    /**
     * $type_result
     *
     * Define si el resultados de las consultas de selección de tipo objeto o array asociativo
     *
     * @var bool
     */
    protected $type_result_assoc = false;

    /** @var \PDOStatement $prepareStatement Consulta preparada*/
    protected $prepareStatement = null;

    /** @ignore @var $sql */
    protected $sql = "";

    /** @ignore @var $lastSQLExecuted */
    protected $lastSQLExecuted = "";

    /** @ignore @var $selectFields */
    protected $selectFields = "*";

    /** @ignore @var $selectString */
    protected $selectString = "";

    /** @ignore @var $insertString */
    protected $insertString = "";

    /** @ignore @var $updateString */
    protected $updateString = "";

    /** @ignore @var $deleteString */
    protected $deleteString = "";

    /** @ignore @var $joinsString */
    protected $joinsString = [];

    /** @ignore @var $whereString */
    protected $whereString = "";

    /** @ignore @var $havingString */
    protected $havingString = "";

    /** @ignore @var $orderByString */
    protected $orderByString = "";

    /** @ignore @var $action */
    protected $action = null;

    /** @ignore @var $groupByString */
    protected $groupByString = "";

    /** @ignore @var array|null $replacePrepareValues Array con los alias de la consulta preparada y sus valores con la estructura [':alias'=>valor[,...]] */
    protected $replacePrepareValues = [];

    /** @ignore @var array $whereReplacePrepareValues Array con los alias de la consulta preparada y sus valores con la estructura usado para los alias en where[':alias'=>valor[,...]] */
    protected $whereReplacePrepareValues = [];

    /** @ignore @var array $havingReplacePrepareValues Array con los alias de la consulta preparada y sus valores con la estructura usado para los alias en where[':alias'=>valor[,...]] */
    protected $havingReplacePrepareValues = [];

    /** @ignore @var array $resultSet */
    protected $resultSet = null;

    /**
     * $selectClass
     *
     * @var string Clase que intentará simular en los selects cuando se haga con PDO::FETCH_CLASS
     */
    protected $selectClass = null;

    /** @var \PDO[] $db Instancias de \PDO */
    protected static $db = [];

    /**
     * $defaultValueOptions
     *
     * @var array Opciones por defecto
     */
    protected static $defaultValueOptions = [
        'driver' => 'mysql',
        'database' => '',
        'host' => 'localhost',
        'user' => 'root',
        'password' => '',
        'charset' => 'utf8',
        'config' => true,
        'table' => 'NOT_DEFINED_TABLE',
    ];

    /**
     * $patternsOptions
     *
     * @var array Patrones para validar las opciones
     */
    protected static $patternsOptions = [
        'driver' => [
            'driver' => ':string',
        ],
        'database' => [
            'database' => ':string',
        ],
        'host' => [
            'host' => ':string',
        ],
        'user' => [
            'user' => ':string',
        ],
        'password' => [
            'password' => ':string',
        ],
        'charset' => [
            'charset' => ':string',
        ],
        'config' => [
            'config' => ':bool',
        ],
        'table' => [
            'table' => ':string',
        ],
    ];

    /**
     * $instanceOptionsDb
     *
     * @var array Opciones de configuración de la base de datos (de la instancia)
     */
    protected $instanceOptionsDb = [];

    /**
     * $optionsDb
     *
     * @var array Opciones de configuración de la base de datos (generales)
     */
    protected static $optionsDb = [];

    //Acciones
    const INSERT = 'INSERT';
    const SELECT = 'SELECT';
    const UPDATE = 'UPDATE';
    const DELETE = 'DELETE';

    //Comentarios
    const DEFAULT_OPTIONS_COMMENTS = [
        'driver' => '(string) El controlador PDO.  Opcional. Por defecto mysql',
        'database' => '(string) Nombre de la base de datos. Obligatorio',
        'host' => '(string) Servidor. Opcional. Por defecto localhost',
        'user' => '(string) Usuario. Opcional. Por defecto root',
        'password' => '(string) Contraseña. Opcional. Por defecto una cadena vacía',
        'charset' => '(string) El juego de caracteres. Opcional. Por defecto utf8',
        'config' => '(bool) Si se configurará la instancia PDO. Opcional. Por defecto true',
        'table' => '(string) El nombre de la tabla. Opcional. Por defecto NOT_DEFINED_TABLE',
    ];

    /**
     * Configuración del modelo.
     *
     * Usa PDO para la conexión.
     *
     * @param array $options Las opciones de configuración (ver la constante de clase DEFAULT_OPTIONS_COMMENTS)
     * @return static
     * @throws RequiredOptionMissedException|OptionPatternMismatchException
     */
    public function __construct(array $options = [])
    {
        if (!array_key_exists('config', $options)) {
            $options['config'] = static::$defaultValueOptions['config'];
        } elseif (!is_bool($options['config'])) {
            $options['config'] = null;
        }

        $config = $options['config'];

        if ($config) {
            $this->configDb($options);
        } else {

            if (is_string($this->prefix_table)) {
                $this->table = trim($this->prefix_table) . trim($this->table);
            }

            if (is_null($this->fields) && is_string($this->table)) {
                $this->configFields();
            }
        }
    }

    /**
     * Prepara una consulta.
     * @param string $sql Sentencia SQL
     * @return \PDOStatement
     * Devuelve la consulta preparada.
     */
    public function prepare(string $sql)
    {
        try {
            $key = $this->instanceOptionsDb['key'];
            $this->prepareStatement = self::$db[$key]->prepare($sql);
            return $this->prepareStatement;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Prepara la consulta de selección la base de datos.
     * @param array|string $fields
     * Puede ser un array con la estructura ['columna','columna2'[,...]].
     * O un string tal como 'columna1,columna2[,...]'
     *
     * @throws DatabaseClassesExceptions
     * @return static
     * Devuelve el objeto mismo.
     */
    public function select($fields = null)
    {
        if (!is_null($fields)) {
            if (is_array($fields)) {
                if (count($fields) > 0) {
                    $fields_select = [];

                    foreach ($fields as $field) {
                        $fields_select[] = $field;
                    }
                    $fields_select = implode(',', $fields_select);
                    $this->selectFields = $fields_select;
                } else {
                    $this->selectFields = "*";
                }
            } else if (is_string($fields)) {
                $this->selectFields = $fields;
            } else {
                throw new DatabaseClassesExceptions("Unexpected param type", CodeStringExceptionsEnum::TypeError);
            }
        } else {
            $this->selectFields = "*";
        }
        $this->selectString = "SELECT $this->selectFields FROM $this->table ";
        $this->action = self::SELECT;
        return $this;
    }

    /**
     * Prepara la consulta de inserción de un registro en la base de datos.
     * @param array $data un array asociativo con la estructura ['columna'=>valor[,...]]
     * @return static
     * Devuelve el objeto mismo.
     */
    public function insert(array $data)
    {
        try {
            $fields = [];
            $values = [];
            $values_alias = [];

            foreach ($data as $field => $value) {
                $fields[] = $this->table . '.' . $field;
                $values_alias[] = ":$field";
                $values[":$field"] = $value;
            }

            $fields = implode(',', $fields);
            $values_alias = implode(',', $values_alias);
            $this->replacePrepareValues = $values;

            $this->insertString = "INSERT INTO $this->table ($fields) VALUES ($values_alias) ";

            $this->action = self::INSERT;
            return $this;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Prepara la consulta de actualización de registros en la base de datos.
     * Debería ser acompañado con el método where.
     * @param array $update_pair un array asociativo con la estructura ['columna'=>valor[,...]]
     * @return static
     * Devuelve el objeto mismo.
     */
    public function update(array $update_pair)
    {
        try {
            $values = [];
            $fields_values = [];

            foreach ($update_pair as $field => $value) {
                $alias = ":$field";
                $values[$alias] = $value;
                $fields_values[] = $this->table . '.' . $field . "=" . $alias;
            }

            $this->replacePrepareValues = $values;

            $this->updateString = implode(',', $fields_values);
            $this->updateString = "UPDATE $this->table SET $this->updateString ";

            $this->action = self::UPDATE;

            return $this;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Prepara la consulta de la eliminación de registros en la base de datos.
     * @param mixed $where_string Argumento tal como se pasaría a la función where
     * @return static
     * Devuelve el objeto mismo.
     */
    public function delete($where)
    {
        try {
            $this->action = self::DELETE;
            $this->deleteString = " DELETE FROM $this->table ";
            return $this->where($where);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Establece un join de una consulta
     *
     * @param string $table
     * @param string|array|bool $on un string de los criterios de ON o un array como:
     * [
     *         'columna' => ['operator' => 'value', 'and_or' => 'AND|OR'], //Si and_or se omite, por defecto es AND
     *         ...
     * ]
     * o
     * [
     *         'columna' => 'value', //Donde se asume que el operador es = y si hay varios se asume AND
     *         ...
     * ]
     * @param string $type LEFT|INNER|RIGHT|NORMAL
     * @return static
     * @throws DatabaseClassesExceptions
     * Devuelve el objeto mismo.
     */
    public function join(string $table, $on = false, string $type = 'NORMAL')
    {
        $on_string = null;

        if (is_array($on)) {
            //Verifica si el $on es un array

            $on_string = [];

            foreach ($on as $column => $value) {

                //Verifica que $column sea string puesto que debe corresponder al nombre de columna
                if (is_string($column)) {

                    $values = [];

                    if (is_array($value) && array_key_exists('multiple', $value) && is_array($value['multiple'])) {
                        foreach ($value['multiple'] as $multiple_value) {
                            $values[] = $multiple_value;
                        }
                    } else {
                        $values[] = $value;
                    }

                    foreach ($values as $value_processed) {

                        $value_processed = is_array($value_processed) ? $value_processed : [
                            '=' => $value_processed,
                            'and_or' => 'AND',
                        ];

                        //Verifica que $value_processed contenga 2 elementos como máximo
                        if (count($value_processed) > 0 && count($value_processed) < 3) {

                            $and_or = isset($value_processed['and_or']) ? $value_processed['and_or'] : 'AND';
                            $and_or = $and_or == 'AND' || $and_or == 'OR' ? $and_or : 'AND';

                            foreach ($value_processed as $key => $value_need) {

                                if ($key == 'and_or') {
                                    //Ignora la clave 'and_or'
                                    continue;
                                }

                                //Verifica que $key sea un string y que $value_need sea string|numérico
                                if (is_string($key)) {

                                    //Verifica que $value_need sea string|numérico
                                    $valid_value =
                                    is_scalar($value_need) ||
                                    is_string($value_need) ||
                                    is_numeric($value_need);

                                    if ($valid_value) {

                                        $value_need = str_replace(['{THIS}', '{DEFAULT}'], [$table, $this->table], $value_need);
                                        $column = str_replace(['{THIS}', '{DEFAULT}'], [$table, $this->table], $column);

                                        $on_string[] = [
                                            'critery' => "$column $key $value_need",
                                            'logical_connector' => $and_or,
                                        ];
                                    } else {
                                        throw new DatabaseClassesExceptions(
                                            "Error en join(), los valores para comparar deben ser de un tipo escalar",
                                            CodeStringExceptionsEnum::JoinCompareValueIsNotScalar
                                        );
                                    }
                                } else {
                                    throw new DatabaseClassesExceptions
                                        ("Error en join(), el nombre de la opción en la columna $column no es válido",
                                        CodeStringExceptionsEnum::JoinInvalidOptionName
                                    );
                                }
                            }
                        } else {
                            throw new DatabaseClassesExceptions
                                ("Error en join(), la cantidad de opciones en la columna $column no es válida",
                                CodeStringExceptionsEnum::JoinInvalidOptionAmount
                            );
                        }
                    }
                } else {
                    throw new DatabaseClassesExceptions
                        ("Error en join(), el nombre de la columna debe ser un string",
                        CodeStringExceptionsEnum::JoinInvalidColumnName
                    );
                }
            }

            //Volver valores en cadenas
            $count_elements = count($on_string) - 1;
            foreach ($on_string as $index => $element) {
                if ($index < $count_elements) {
                    $on_string[$index] = $element['critery'] . ' ' . $element['logical_connector'] . ' ';
                } else {
                    $on_string[$index] = $element['critery'] . ' ';
                }
            }

            $on_string = str_replace('  ', ' ', trim(implode(' ', $on_string))); //Convertir $on_string en string para la consulta
            $on_string = "ON $on_string";
        } else if (is_string($on)) {
            //Verifica que $on sea un string

            $on_string = "ON $on";
        } else if ($on === false) {
            //Verifica que $on sea un string

            $on_string = '';
        } else {

            throw new DatabaseClassesExceptions(
                "Error en join(), debe proporcionar un array o un string y ha proporcionado un tipo: " . gettype($on),
                CodeStringExceptionsEnum::TypeError
            );
        }

        switch ($type) {
            case 'LEFT':
                $type = 'LEFT';
                break;
            case 'RIGHT':
                $type = 'RIGHT';
                break;
            case 'INNER':
                $type = 'INNER';
                break;
            case 'NORMAL':
            default:
                $type = '';
                break;
        }

        $this->joinsString[$table] = " $type JOIN $table $on_string";

        return $this;
    }

    /**
     * Establece un LEFT join de una consulta
     *
     * @param string $table
     * @param string|array|bool $on un string de los criterios de ON o un array como:
     * [
     *         'columna' => ['operator' => 'value', 'and_or' => 'AND|OR'], //Si and_or se omite, por defecto es AND
     *         ...
     * ]
     * o
     * [
     *         'columna' => 'value', //Donde se asume que el operador es = y si hay varios se asume AND
     *         ...
     * ]
     * @return static
     * @throws DatabaseClassesExceptions
     * Devuelve el objeto mismo.
     */
    public function leftJoin(string $table, $on = false)
    {
        return $this->join($table, $on, 'LEFT');
    }

    /**
     * Establece un RIGHT join de una consulta
     *
     * @param string $table
     * @param string|array|bool $on un string de los criterios de ON o un array como:
     * [
     *         'columna' => ['operator' => 'value', 'and_or' => 'AND|OR'], //Si and_or se omite, por defecto es AND
     *         ...
     * ]
     * o
     * [
     *         'columna' => 'value', //Donde se asume que el operador es = y si hay varios se asume AND
     *         ...
     * ]
     * @return static
     * @throws DatabaseClassesExceptions
     * Devuelve el objeto mismo.
     */
    public function rightJoin(string $table, $on = false)
    {
        return $this->join($table, $on, 'RIGHT');
    }

    /**
     * Establece un INNER join de una consulta
     *
     * @param string $table
     * @param string|array|bool $on un string de los criterios de ON o un array como:
     * [
     *         'columna' => ['operator' => 'value', 'and_or' => 'AND|OR'], //Si and_or se omite, por defecto es AND
     *         ...
     * ]
     * o
     * [
     *         'columna' => 'value', //Donde se asume que el operador es = y si hay varios se asume AND
     *         ...
     * ]
     * @return static
     * @throws DatabaseClassesExceptions
     * Devuelve el objeto mismo.
     */
    public function innerJoin(string $table, $on = false)
    {
        return $this->join($table, $on, 'INNER');
    }

    /**
     * Establece los criterios de WHERE para la consulta.
     * @param string|array $where un string de los criterios de WHERE o un array como:
     * [
     *         'columna' => ['operator' => 'value', 'and_or' => 'AND|OR'], //Si and_or se omite, por defecto es AND
     *         ...
     * ]
     * o
     * [
     *         'columna' => 'value', //Donde se asume que el operador es = y si hay varios se asume AND
     *         ...
     * ]
     * @return static
     * @throws DatabaseClassesExceptions
     * Devuelve el objeto mismo.
     */
    public function where($where)
    {
        $where_string = null;

        $this->whereReplacePrepareValues = [];

        $generate_alias = function ($name) {
            $alias = preg_replace("/[^a-z|A-Z|0-9]+/", '_', $name);
            return (is_string($alias) ? ':' . $alias : ':' . $name) . '_' . uniqid();
        };

        if (is_array($where)) {
            //Verifica si el $where es un array

            $where_string = [];

            foreach ($where as $column => $value) {

                //Verifica que $column sea string puesto que debe corresponder al nombre de columna
                if (is_string($column)) {

                    $values = [];

                    if (is_array($value) && array_key_exists('multiple', $value) && is_array($value['multiple'])) {
                        foreach ($value['multiple'] as $multiple_value) {
                            $values[] = $multiple_value;
                        }
                    } else {
                        $values[] = $value;
                    }

                    foreach ($values as $value_processed) {

                        $value_processed = is_array($value_processed) ? $value_processed : [
                            '=' => $value,
                            'and_or' => 'AND',
                        ];

                        //Verifica que $value_processed contenga 2 elementos como máximo
                        if (count($value_processed) > 0 && count($value_processed) <= 2) {

                            $and_or = isset($value_processed['and_or']) ? $value_processed['and_or'] : 'AND';
                            $and_or = $and_or == 'AND' || $and_or == 'OR' ? $and_or : 'AND';

                            foreach ($value_processed as $key => $value_need) {

                                if ($key == 'and_or') {
                                    //Ignora la clave 'and_or'
                                    continue;
                                }

                                //Verifica que $key sea un string y que $value_need sea string|numérico
                                if (is_string($key)) {

                                    //Verifica que $value_need sea string|numérico
                                    $valid_value =
                                    is_scalar($value_need) ||
                                    is_string($value_need) ||
                                    is_numeric($value_need);

                                    $has_is_null_operator = (
                                        (is_string($value_need) &&
                                            strlen(trim($value_need)) == 0) ||
                                        is_null($value_need)) &&
                                    strtoupper(trim($key)) == 'IS NULL';

                                    $valid_value = $valid_value || $has_is_null_operator;

                                    if ($valid_value) {

                                        if (!$has_is_null_operator) {
                                            $alias = ($generate_alias)($column);

                                            $where_string[] = [
                                                'critery' => "$column $key $alias",
                                                'logical_connector' => $and_or,
                                            ];

                                            $this->whereReplacePrepareValues[$alias] = $value_need;
                                        } else {

                                            $where_string[] = [
                                                'critery' => "$column $key",
                                                'logical_connector' => $and_or,
                                            ];
                                        }
                                    } else {
                                        throw new DatabaseClassesExceptions(
                                            "Error en where(), los valores para comparar deben ser de un tipo escalar",
                                            CodeStringExceptionsEnum::WhereCompareValueIsNotScalar
                                        );
                                    }
                                } else {
                                    throw new DatabaseClassesExceptions(
                                        "Error en where(), el nombre de la opción en la columna $column no es válido",
                                        CodeStringExceptionsEnum::WhereInvalidOptionName
                                    );
                                }
                            }
                        } else {
                            throw new DatabaseClassesExceptions(
                                "Error en where(), la cantidad de opciones en la columna $column no es válida",
                                CodeStringExceptionsEnum::WhereInvalidOptionAmount
                            );
                        }
                    }
                } else {
                    throw new DatabaseClassesExceptions(
                        "Error en where(), el nombre de la columna debe ser un string",
                        CodeStringExceptionsEnum::WhereInvalidColumnName
                    );
                }
            }

            //Volver valores en cadenas
            $count_elements = count($where_string) - 1;
            foreach ($where_string as $index => $element) {
                if ($index < $count_elements) {
                    $where_string[$index] = $element['critery'] . ' ' . $element['logical_connector'] . ' ';
                } else {
                    $where_string[$index] = $element['critery'] . ' ';
                }
            }

            $where_string = str_replace('  ', ' ', trim(implode(' ', $where_string))); //Convertir $where_string en string para la consulta

        } else if (is_string($where)) {
            //Verifica que $where sea un string

            $where_string = $where;
        } else {

            throw new DatabaseClassesExceptions(
                "Error en where(), debe proporcionar un array o un string y ha proporcionado un tipo: " . gettype($where),
                CodeStringExceptionsEnum::TypeError
            );
        }

        $this->whereString = " WHERE $where_string ";

        return $this;
    }

    /**
     * Establece los criterios de HAVING para la consulta.
     * @param string|array $having un string de los criterios de HAVING o un array como:
     * [
     *         'columna' => ['operator' => 'value', 'and_or' => 'AND|OR'], //Si and_or se omite, por defecto es AND
     *         ...
     * ]
     * o
     * [
     *         'columna' => 'value', //Donde se asume que el operador es = y si hay varios se asume AND
     *         ...
     * ]
     * @return static
     * @throws DatabaseClassesExceptions
     * Devuelve el objeto mismo.
     */
    public function having($having)
    {
        $having_string = null;

        $this->havingReplacePrepareValues = [];

        $generate_alias = function ($name) {
            $alias = preg_replace("/[^a-z|A-Z|0-9]+/", '_', $name);
            return (is_string($alias) ? ':' . $alias : ':' . $name) . '_' . uniqid();
        };

        if (is_array($having)) {
            //Verifica si el $having es un array

            $having_string = [];

            foreach ($having as $column => $value) {

                //Verifica que $column sea string puesto que debe corresponder al nombre de columna
                if (is_string($column)) {

                    $values = [];

                    if (is_array($value) && array_key_exists('multiple', $value) && is_array($value['multiple'])) {
                        foreach ($value['multiple'] as $multiple_value) {
                            $values[] = $multiple_value;
                        }
                    } else {
                        $values[] = $value;
                    }

                    foreach ($values as $value_processed) {

                        $value_processed = is_array($value_processed) ? $value_processed : [
                            '=' => $value,
                            'and_or' => 'AND',
                        ];

                        //Verifica que $value_processed contenga 2 elementos como máximo
                        if (count($value_processed) > 0 && count($value_processed) <= 2) {

                            $and_or = isset($value_processed['and_or']) ? $value_processed['and_or'] : 'AND';
                            $and_or = $and_or == 'AND' || $and_or == 'OR' ? $and_or : 'AND';

                            foreach ($value_processed as $key => $value_need) {

                                if ($key == 'and_or') {
                                    //Ignora la clave 'and_or'
                                    continue;
                                }

                                //Verifica que $key sea un string y que $value_need sea string|numérico
                                if (is_string($key)) {

                                    //Verifica que $value_need sea string|numérico
                                    $valid_value =
                                    is_scalar($value_need) ||
                                    is_string($value_need) ||
                                    is_numeric($value_need);

                                    $has_is_null_operator = (
                                        (is_string($value_need) &&
                                            strlen(trim($value_need)) == 0) ||
                                        is_null($value_need)) &&
                                    strtoupper(trim($key)) == 'IS NULL';

                                    $valid_value = $valid_value || $has_is_null_operator;

                                    if ($valid_value) {

                                        if (!$has_is_null_operator) {
                                            $alias = ($generate_alias)($column);

                                            $having_string[] = [
                                                'critery' => "$column $key $alias",
                                                'logical_connector' => $and_or,
                                            ];

                                            $this->havingReplacePrepareValues[$alias] = $value_need;
                                        } else {

                                            $having_string[] = [
                                                'critery' => "$column $key",
                                                'logical_connector' => $and_or,
                                            ];
                                        }
                                    } else {
                                        throw new DatabaseClassesExceptions(
                                            "Error en having(), los valores para comparar deben ser de un tipo escalar",
                                            CodeStringExceptionsEnum::HavingCompareValueIsNotScalar
                                        );
                                    }
                                } else {
                                    throw new DatabaseClassesExceptions(
                                        "Error en having(), el nombre de la opción en la columna $column no es válido",
                                        CodeStringExceptionsEnum::HavingInvalidOptionName
                                    );
                                }
                            }
                        } else {
                            throw new DatabaseClassesExceptions(
                                "Error en having(), la cantidad de opciones en la columna $column no es válida",
                                CodeStringExceptionsEnum::HavingInvalidOptionAmount
                            );
                        }
                    }
                } else {
                    throw new DatabaseClassesExceptions(
                        "Error en having(), el nombre de la columna debe ser un string",
                        CodeStringExceptionsEnum::HavingInvalidColumnName
                    );
                }
            }

            //Volver valores en cadenas
            $count_elements = count($having_string) - 1;
            foreach ($having_string as $index => $element) {
                if ($index < $count_elements) {
                    $having_string[$index] = $element['critery'] . ' ' . $element['logical_connector'] . ' ';
                } else {
                    $having_string[$index] = $element['critery'] . ' ';
                }
            }

            $having_string = str_replace('  ', ' ', trim(implode(' ', $having_string))); //Convertir $having_string en string para la consulta

        } else if (is_string($having)) {
            //Verifica que $having sea un string

            $having_string = $having;
        } else {

            throw new DatabaseClassesExceptions(
                "Error en having(), debe proporcionar un array o un string y ha proporcionado un tipo: " . gettype($having),
                CodeStringExceptionsEnum::TypeError
            );
        }

        $this->havingString = " HAVING $having_string ";

        return $this;
    }

    /**
     * Obtiene todos los registros de la tabla.
     * @param bool $assoc Define si el resultado es un array asociativo o un objeto
     * @return boolean|array
     * Devuelve un array de objetos o false en caso de error.
     */
    public function getAll(bool $assoc = false)
    {
        try {

            $prepareStatement = $this->prepare("SELECT * FROM $this->table");

            $status = $prepareStatement->execute();

            if ($status) {

                if ($this->type_result_assoc === true || $assoc === true) {
                    $fetch = $this->prepareStatement->fetchAll(PDO::FETCH_ASSOC);
                } else {
                    $fetch = $this->prepareStatement->fetchAll(PDO::FETCH_OBJ);
                }

                $this->closeCursor();
                return $fetch;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Ejecuta la consulta de tipo SELECT que esté preparada.
     * @param bool $assoc Define si el resultado es un array asociativo o un objeto
     * @return boolean|object|array|int
     * Devuelve un objeto correspondiente al primer registro que devuelva la consulta.
     * Si la consulta preparada no es de tipo SELECT devuelve false.
     * Si la consulta no da resultados devuelve -1.
     * Si la consulta falla devuelve false.
     */
    public function row(bool $assoc = false)
    {
        switch ($this->action) {

            case self::SELECT:

                $this->structureQuery('SELECT');

                if (count($this->getReplaceWhereAndHavingValues()) > 0) {
                    $status = $this->prepareStatement->execute($this->getReplaceWhereAndHavingValues());
                } else {
                    $status = $this->prepareStatement->execute();
                }

                if ($status) {

                    if ($this->type_result_assoc === true || $assoc === true) {
                        $fetch = $this->prepareStatement->fetchAll(PDO::FETCH_ASSOC);
                    } else {
                        $fetch = $this->prepareStatement->fetchAll(PDO::FETCH_OBJ);
                    }

                    if (count($fetch) > 0) {
                        $fetch = $fetch[0];
                    } else {
                        $fetch = -1;
                    }

                    $this->closeCursor();
                    $this->resetAll();

                    return $fetch;

                } else {
                    return false;
                }

                break;

            default:
                return false;
                break;

        }
    }

    /**
     * Trae un registro según el criterio brindado
     *
     * @param mixed $value El valor de comparación
     * @param string $column_name La columna de comparación
     * @param string|array $select_columns Las columnas que devolverá la consulta
     * @param bool $assoc Define si el resultado es un array asociativo o un objeto
     *
     * @return boolean|object|array|int
     *
     * Devuelve un objeto correspondiente al primer registro que devuelva la consulta.
     * Si la consulta no da resultados devuelve -1.
     * Si la consulta falla devuelve false.
     */
    public function get($value, string $column_name = 'id', $select_columns = '*', bool $assoc = false)
    {
        $fetch = null;

        if (!is_null($select_columns)) {

            if (is_array($select_columns)) {

                if (count($select_columns) > 0) {

                    $fields_select = [];

                    foreach ($select_columns as $field) {
                        $fields_select[] = $field;
                    }

                    $fields_select = implode(',', $fields_select);

                    $select_columns = $fields_select;
                } else {
                    $select_columns = "*";
                }
            } else if (is_string($select_columns)) {

                $select_columns = $select_columns;
            } else {
                throw new DatabaseClassesExceptions("Unexpected param type", CodeStringExceptionsEnum::TypeError);
            }
        } else {
            $select_columns = "*";
        }

        $this->prepareStatement = $this->prepare("SELECT $select_columns FROM $this->table WHERE $column_name = :VALUE_WHERE");

        $status = $this->prepareStatement->execute([
            ':VALUE_WHERE' => $value,
        ]);

        if ($status) {
            if ($this->type_result_assoc === true || $assoc === true) {
                $fetch = $this->prepareStatement->fetchAll(PDO::FETCH_ASSOC);
            } else {
                $fetch = $this->prepareStatement->fetchAll(PDO::FETCH_OBJ);
            }

            if (count($fetch) > 0) {
                $fetch = $fetch[0];
            } else {
                $fetch = -1;
            }
        } else {
            $fetch = false;
        }

        $this->closeCursor();
        return $fetch;
    }

    /**
     * Establece los criterios de GROUP BY para la consulta.
     * @param string $group_string un string de los criterios de GROUP BY
     * @return static
     * Devuelve el objeto mismo.
     */
    public function groupBy(string $group_string)
    {
        try {

            switch ($this->action) {
                case self::SELECT:
                    $this->groupByString = " GROUP BY $group_string";
                    $this->sql = "SELECT $this->selectFields FROM $this->table" . $this->getJoins() . $this->whereString . $this->groupByString . $this->havingString . $this->orderByString;
                    $this->prepareStatement = $this->prepare(trim($this->sql));
                    return $this;
                    break;
            }
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Establece los criterios de ORDER BY para la consulta.
     * @param string $order_string un string de los criterios de ORDER BY
     * @return static
     * Devuelve el objeto mismo.
     */
    public function orderBy(string $order_string)
    {
        try {

            switch ($this->action) {
                case self::SELECT:
                    $this->orderByString = " ORDER BY $order_string";
                    $this->sql = "SELECT $this->selectFields FROM $this->table" . $this->getJoins() . $this->whereString . $this->groupByString . $this->havingString . $this->orderByString;
                    $this->prepareStatement = $this->prepare(trim($this->sql));
                    return $this;
                    break;
            }
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Último registro insertado.
     * @param string $name columna de referencia por defecto NULL
     * @return mixed
     * Devuelve el valor de la columna especificada en $name del último registro insertado.
     */
    public function lastInsertId(string $name = null)
    {
        $key = $this->instanceOptionsDb['key'];
        return self::$db[$key]->lastInsertId($name);
    }

    /**
     * Cantidad de registros de una tabla según una columna.
     * Funciona igual que 'SELECT columna FROM TABLA' por lo que se debe usa sobre una columna
     * de la cual se esté seguro que posea un valor en todos los registros
     * para obtener la cantidad real de registros.
     * @param string $column Columna que se usará de referencia para el conteo
     * @return int|boolean
     * Devuelve el número de registros de una tabla o false en caso de fallar.
     */
    public function rowCount(string $column = 'id')
    {
        try {
            $key = $this->instanceOptionsDb['key'];
            $sql = "SELECT id FROM $this->table";
            $prepare_statement = self::$db[$key]->query($sql);
            $status = $prepare_statement->execute();
            if ($status) {
                $fetch = $prepare_statement->fetchAll();
                if (is_array($fetch)) {
                    return count($fetch);
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * configDb
     *
     * Establece opciones de la configuración de la base de datos del objeto.
     *
     * @param array $options Las opciones de configuración (ver la constante de clase DEFAULT_OPTIONS_COMMENTS)
     *
     * @return void
     * @throws RequiredOptionMissedException|OptionPatternMismatchException
     */
    public function configDb(array $options)
    {
        $options = static::_validateOptions($options);
        $driver = $options['driver'];
        $database = $options['database'];
        $host = $options['host'];
        $user = $options['user'];
        $password = $options['password'];
        $charset = $options['charset'];
        $table = $options['table'];
        $key = $options['key'];

        if ($driver == 'sqlite') {
            $database = strlen($database) == 0 ? ':memory:' : $database;
            self::$db[$key] = new PDO("$driver:$database", null, null, [
                \PDO::ATTR_PERSISTENT => true,
            ]);
        } else {
            self::$db[$key] = new PDO($driver . ":dbname=" . $database . ";host=" . $host, $user, $password, [
                \PDO::ATTR_PERSISTENT => true,
            ]);
        }

        self::$db[$key]->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        if ($driver != 'sqlite') {
            $prepareStatement = self::$db[$key]->prepare("SET character set :CHARSET");
            $prepareStatement->execute([':CHARSET' => $charset]);
        }

        if ($this->table === null) {
            $this->setTable($table);
        } else {
            if (is_string($this->prefix_table)) {
                $this->table = trim($this->prefix_table) . trim($this->table);
            }
        }

        $this->instanceOptionsDb = $options;
    }

    /**
     * Asigna el prefijo tabla del modelo.
     * @param string $prefix_table Prefijo de la tabla
     * @return void
     */
    public function setPrefixTable(string $prefix_table)
    {
        $last_prefix = $this->prefix_table;
        $this->prefix_table = trim($prefix_table);

        if (is_string($this->table)) {

            if (is_string($this->prefix_table)) {

                $this->table = str_replace($last_prefix, '', $this->table);
            }

            $this->setTable($this->table);
        }
    }

    /**
     * Asigna el nombre del tabla del modelo.
     * @param string $table Nombre de la tabla
     * @return void
     */
    public function setTable(string $table)
    {
        if (is_string($this->prefix_table)) {
            $this->table = trim($this->prefix_table) . trim($table);
        } else {
            $this->table = trim($table);
        }
    }

    /**
     * Asigna las columnas del modelo.
     * @param array $fields Array que representa las columnas con la estructura ['columna1','columna2'[,...]]
     * @return void
     */
    public function setFields(array $fields)
    {
        $this->fields = $fields;
    }

    /**
     * setTypeResult
     *
     * Define si el resultados de las consultas de selección de tipo objeto o array asociativo
     *
     * @param mixed $assoc Si es true los resultados de las consultas serán un array asociativo
     * @return void
     */
    public function setTypeResult(bool $assoc = true)
    {
        $this->type_result_assoc = $assoc;
    }

    /**
     * setSelectClass
     *
     * @param string $nameClass
     * @return void
     */
    public function setSelectClass(string $nameClass)
    {
        $this->selectClass = $nameClass;
    }

    /**
     * configFields
     *
     * Obtiene y configura los nombres de las columnas
     *
     * @return boid
     */
    public function configFields()
    {
        $key = $this->instanceOptionsDb['key'];
        $result = self::$db[$key]
            ->query("DESCRIBE $this->table")
            ->fetchAll(PDO::FETCH_OBJ);

        $this->fields = [];

        foreach ($result as $field_data) {
            $field = $field_data->Field;
            $this->fields[] = $field;
        }

        return $result;
    }

    /**
     * getPrefixTable
     *
     * Devuelve el prefijo de la tabla
     *
     * @return string
     */
    public function getPrefixTable()
    {
        return $this->prefix_table;
    }

    /**
     * getTable
     *
     * Devuelve el nombre de la tabla
     *
     * @return string
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * getTableInformation
     *
     * Devuelve la información de la tabla
     *
     * @return string
     */
    public function getTableInformation()
    {
        $key = $this->instanceOptionsDb['key'];
        $result = self::$db[$key]
            ->query("DESCRIBE $this->table")
            ->fetchAll(PDO::FETCH_OBJ);

        return $result;
    }

    /**
     * getFields
     *
     * Devuelve los campos de la tabla
     *
     * @return array
     */
    public function getFields()
    {
        if ($this->fields === null) {
            $this->configFields();
        }
        return $this->fields;
    }

    /**
     * Devuelve el string de la consulta
     *
     * @return string
     */
    public function getCompiledSQL()
    {
        $query = '';

        switch ($this->action) {
            case self::INSERT:
                $query = $this->structureQuery('INSERT', false);
                break;
            case self::SELECT:
                $query = $this->structureQuery('SELECT', false);
                break;
            case self::UPDATE:
                $query = $this->structureQuery('UPDATE', false);
                break;
            case self::DELETE:
                $query = $this->structureQuery('DELETE', false);
                break;
        }

        $replace_values = $this->replacePrepareValues;
        $replace_values_where = $this->whereReplacePrepareValues;
        $replace_values_having = $this->havingReplacePrepareValues;

        foreach ($replace_values_where as $alias => $value) {
            $_alias = str_replace(':', "", $alias);
            $query = str_replace($alias, ":($_alias=$value)", $query);
        }

        foreach ($replace_values_having as $alias => $value) {
            $_alias = str_replace(':', "", $alias);
            $query = str_replace($alias, ":($_alias=$value)", $query);
        }

        foreach ($replace_values as $alias => $value) {
            $_alias = str_replace(':', "", $alias);
            $query = str_replace($alias, ":($_alias=$value)", $query);
        }

        return str_replace('  ', " ", $query);
    }

    /**
     * Devuelve la consulta preparada
     *
     * @return \PDOStatement
     */
    public function getPrepareStatement()
    {
        try {

            return $this->prepare(trim($this->sql));
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * debugDumpParams
     *
     * Vuelca un comando preparado de SQL
     *
     * @return void
     */
    public function debugDumpParams()
    {
        $this->prepareStatement->debugDumpParams();
    }

    /**
     * getReplaceValues
     *
     * @return array
     */
    public function getReplaceValues()
    {
        return array_merge(
            $this->replacePrepareValues,
            $this->whereReplacePrepareValues,
            $this->havingReplacePrepareValues
        );
    }

    /**
     * getReplaceWhereAndHavingValues
     *
     * @return array
     */
    public function getReplaceWhereAndHavingValues()
    {
        return array_merge(
            $this->whereReplacePrepareValues,
            $this->havingReplacePrepareValues
        );
    }

    /**
     * Reinicia todos los JOINS
     * @return void
     */
    public function resetJoins()
    {
        $this->joinsString = [];
    }

    /**
     * Reinicia todos los WHERE
     * @return void
     */
    public function resetWhere()
    {
        $this->whereString = '';
        $this->whereReplacePrepareValues = [];
    }

    /**
     * Reinicia todos los HAVING
     * @return void
     */
    public function resetHaving()
    {
        $this->havingString = '';
        $this->havingReplacePrepareValues = [];
    }

    /**
     * Reinicia todas los valores internos
     * @return void
     */
    public function resetAll()
    {
        $this->type_result_assoc = false;

        $this->prepareStatement = null;

        $this->sql = "";

        $this->selectFields = "*";

        $this->selectString = "";

        $this->insertString = "";

        $this->updateString = "";

        $this->joinsString = [];

        $this->whereString = "";

        $this->havingString = "";

        $this->orderByString = "";

        $this->action = null;

        $this->groupByString = "";

        $this->replacePrepareValues = [];

        $this->whereReplacePrepareValues = [];

        $this->havingReplacePrepareValues = [];
    }

    /**
     * getLastSQLExecuted
     *
     * @return string
     */
    public function getLastSQLExecuted()
    {
        return $this->lastSQLExecuted;
    }

    /**
     * Ejecuta la consulta que esté preparada.
     * @param bool $assoc En caso de SELECT define si el resultado es un array asociativo o un objeto
     * @param int $page En caso de SELECT define la "página de inicio" de la consulta (debe definirse $perPage)
     * @param int $perPage En caso de SELECT define la "cantidad de registros por página" de la consulta (debe definirse $page)
     * @return boolean|null
     * Devuelve null si no se ha ha llamado ninguno de los métodos select|insert|update|delete.
     * Devuelve true en caso de exito o false en caso de error.
     */
    public function execute(bool $assoc = false, int $page = null, int $perPage = null)
    {
        try {

            $result = null;

            switch ($this->action) {

                case self::INSERT:

                    $result = $this->_executeInsert();

                    break;

                case self::SELECT:

                    if ($this->type_result_assoc === true || $assoc === true) {
                        $result = $this->_executeSelect(true, $page, $perPage);
                    } else {
                        $result = $this->_executeSelect(false, $page, $perPage);
                    }

                    break;

                case self::UPDATE:

                    $result = $this->_executeUpdate();

                    break;

                case self::DELETE:

                    $result = $this->_executeDelete();

                    break;

            }
            $this->prepareStatement = null;
            $this->lastSQLExecuted = $this->sql;
            $this->sql = '';
            $this->resetAll();

            return $result;

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * result
     *
     * Obtiene el resultado de la consulta select
     *
     * @return mixed
     */
    public function result()
    {
        return $this->resultSet;
    }

    /**
     * Obtiene el string de JOIN
     * @access private
     * @return string
     */
    private function getJoins()
    {
        $str = '';
        $joins = $this->joinsString;
        if (count($joins) > 0) {
            foreach ($joins as $values) {
                $str .= $values;
            }
        }
        return $str;
    }

    /**
     * Ejecuta la selección.
     * @param bool $assoc Define si el resultado es un array asociativo o un objeto
     * @param int $page Define la "página de inicio" de la consulta (debe definirse $perPage)
     * @param int $perPage Define la "cantidad de registros por página" de la consulta (debe definirse $page)
     * @access private
     * @return array|bool
     * @throws \PDOException
     */
    private function _executeSelect(bool $assoc = false, int $page = null, int $perPage = null)
    {
        $this->structureQuery('SELECT', true, $page, $perPage);

        $replace_values = $this->getReplaceWhereAndHavingValues();

        if (count($replace_values) > 0) {
            $status = $this->prepareStatement->execute($replace_values);
        } else {
            $status = $this->prepareStatement->execute();
        }

        if ($status) {

            if ($this->type_result_assoc === true || $assoc === true) {
                $fetch = $this->prepareStatement->fetchAll(PDO::FETCH_ASSOC);
            } else {
                if ($this->selectClass !== null) {
                    $fetch = $this->prepareStatement->fetchAll(PDO::FETCH_CLASS, $this->selectClass);
                } else {
                    $fetch = $this->prepareStatement->fetchAll(PDO::FETCH_OBJ);
                }
            }

            $this->resultSet = $fetch;

            $this->closeCursor();
        } else {
            $this->resultSet = null;
        }

        return $status;
    }

    /**
     * Ejecuta la inserción.
     * @access private
     * @return bool
     * @throws \PDOException
     */
    private function _executeInsert()
    {
        $this->structureQuery('INSERT');
        $execute = $this->prepareStatement->execute($this->replacePrepareValues);
        $this->closeCursor();
        return $execute;
    }

    /**
     * Ejecuta la actualización.
     * @access private
     * @return bool
     * @throws \PDOException
     */
    private function _executeUpdate()
    {
        $this->structureQuery('UPDATE');

        $replace_values = $this->getReplaceValues();

        $execute = $this->prepareStatement->execute($replace_values);

        $this->closeCursor();

        return $execute;
    }

    /**
     * Ejecuta la eliminación.
     * @access private
     * @return void
     */
    private function _executeDelete()
    {
        try {

            $this->structureQuery('DELETE');

            if (count($this->getReplaceWhereAndHavingValues()) > 0) {
                $execute = $this->prepareStatement->execute($this->getReplaceWhereAndHavingValues());
            } else {
                $execute = $this->prepareStatement->execute();
            }

            $this->closeCursor();
            return $execute;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * structureQuery
     *
     * @param string $type
     * @param bool $prepare
     * @param int $page En caso de SELECT define la "página de inicio" de la consulta (debe definirse $perPage)
     * @param int $perPage En caso de SELECT define la "cantidad de registros por página" de la consulta (debe definirse $page)
     * @return string
     */
    private function structureQuery(string $type, bool $prepare = true, int $page = null, int $perPage = null)
    {
        $type = strtoupper($type);

        switch ($type) {

            case 'SELECT':

                $this->sql = $this->selectString . $this->getJoins() . $this->whereString . $this->groupByString . $this->havingString . $this->orderByString;

                if (!is_null($page) && !is_null($perPage)) {

                    $page = $page > 0 ? $page : 1;
                    $perPage = $perPage > 0 ? $perPage : 1;
                    $from = ($page - 1) * $perPage;
                    $limit = "LIMIT $from,$perPage";
                    $this->sql .= " $limit ";

                }

                break;

            case 'INSERT':

                $this->sql = $this->insertString . $this->getJoins() . $this->whereString . $this->havingString;

                break;

            case 'UPDATE':

                $this->sql = $this->updateString . $this->getJoins() . $this->whereString . $this->havingString;

                break;

            case 'DELETE':

                $this->sql = $this->deleteString . $this->getJoins() . $this->whereString . $this->havingString;

                break;

        }

        $this->sql = trim($this->sql);

        if ($prepare) {
            $this->prepareStatement = $this->prepare(trim($this->sql));
        }

        return $this->sql;
    }

    /**
     * Cierra el cursor
     * @access private
     * @return void
     */
    private function closeCursor()
    {
        $this->prepareStatement->closeCursor();
    }

    /**
     * throwException
     *
     * Devuelve una excepción según el tipo escogido
     *
     * @param string $type Tipo da la excepción
     * @return \Exception
     */
    private function throwException(string $type)
    {
        if ($type = 'malformed_param') {
            return new \Exception('El parámetro recibido no tiene la estructura esperada.');
        }
    }

    /**
     * generateKeyDb
     *
     * @param mixed $host
     * @param mixed $databaseName
     * @return string
     */
    public static function generateKeyDb($host, $databaseName)
    {
        $key = [
            'host' => $host,
            'database' => $databaseName,
        ];
        $key = json_encode($key);
        $key = base64_encode($key);
        return $key;
    }

    /**
     * compareDbKeys
     *
     * @param string $key1
     * @param string $key2
     * @return bool
     */
    public static function compareDbKeys(string $key1, string $key2)
    {

        $key1 = base64_decode($key1);
        $key1 = json_decode($key1);

        $key2 = base64_decode($key2);
        $key2 = json_decode($key2);

        if (!is_array($key1) || !is_array($key2)) {
            return false;
        }

        $key1['host'] = !isset($key1['host']) ? null : $key1['host'];
        $key1['database'] = !isset($key1['database']) ? null : $key1['database'];

        $key2['host'] = !isset($key2['host']) ? null : $key2['host'];
        $key2['database'] = !isset($key2['database']) ? null : $key2['database'];

        if ($key1['host'] === null || $key2['host'] === null) {
            return false;
        }

        if ($key1['database'] === null || $key2['database'] === null) {
            return false;
        }

        return $key1['host'] == $key2['host'] && $key1['database'] == $key2['database'];
    }

    /**
     * Establece la propiedad db.
     * @param \PDO $database Instancia de \PDO
     * @param string $databaseName Nombre de la base de datos
     * @param string $host Sevidor
     * @return void
     */
    public static function setDb(\PDO $database, string $databaseName, string $host = 'localhost')
    {
        $key = self::generateKeyDb($host, $databaseName);
        self::$db[$key] = $database;
    }

    /**
     * setOptionsDb
     *
     * Establece opciones de la configuración de la base de datos
     * Nota: las opciones de la clase, no del objeto que puede ser diferente
     * si se definen en el constructor.
     *
     * @param array $options Las opciones de configuración (ver la constante de clase DEFAULT_OPTIONS_COMMENTS)
     *
     * @return void
     * @throws RequiredOptionMissedException|OptionPatternMismatchException
     */
    public static function setOptionsDb(array $options)
    {
        static::$optionsDb = static::_validateOptions($options);
    }

    /**
     * Devuelve el valor de la propiedad db.
     * @param string $databaseName Nombre de la base de datos
     * @param string $host Servidor
     * @return \PDO
     */
    public static function getDb(string $databaseName, string $host = 'localhost')
    {
        $key = self::generateKeyDb($host, $databaseName);
        return isset(self::$db[$key]) ? self::$db[$key] : null;
    }

    /**
     * getOptionsDb
     *
     * Devuelve las opciones de la configuración de la base de datos
     * Nota: las opciones de la clase, no del objeto que puede ser diferente
     * si se definen en el constructor.
     *
     * @return array
     */
    public static function getOptionsDb()
    {
        if (count(static::$optionsDb) > 0) {
            return static::$optionsDb;
        } else {
            return static::$defaultValueOptions;
        }
    }

    /**
     * validateOptions
     *
     * @param array $options Las opciones de configuración (ver la constante de clase DEFAULT_OPTIONS_COMMENTS)
     * @return bool
     */
    public static function validateOptions(array $options)
    {
        try {
            static::_validateOptions($options);
            return true;
        } catch (RequiredOptionMissedException | OptionPatternMismatchException $e) {
            return false;
        }
    }

    /**
     * _validateOptions
     *
     * @param array $options Las opciones de configuración (ver la constante de clase DEFAULT_OPTIONS_COMMENTS)
     * @return array Las opciones
     * @throws RequiredOptionMissedException|OptionPatternMismatchException
     */
    private static function _validateOptions(array $options)
    {

        $default_options = static::$defaultValueOptions;

        $patterns_options = static::$patternsOptions;

        foreach ($patterns_options as $option => $pattern) {
            if (!array_key_exists($option, $options) || is_null($options[$option])) {
                if (array_key_exists($option, $default_options)) {
                    $options[$option] = $default_options[$option];
                }
            }
        }
        $traverser = new Traverser(new ValidatorLocator());

        foreach ($patterns_options as $option => $pattern) {
            if (!array_key_exists($option, $options)) {
                throw new RequiredOptionMissedException($option);
            }
            $option_validate = [$option => $options[$option]];
            if (!$traverser->check($pattern, $option_validate)) {
                throw new OptionPatternMismatchException($option, $pattern[$option]);
            }
        }

        $options['key'] = self::generateKeyDb($options['host'], $options['database']);

        return $options;
    }

    /**
     * Conversión a string
     * @return string
     */
    public function __toString()
    {
        $data = [];

        $data['db'] = self::$db;

        $data['table'] = $this->table;

        $data['prefix_table'] = $this->prefix_table;

        $data['fields'] = $this->fields;

        $data['type_result_assoc'] = $this->type_result_assoc;

        $data['prepareStatement'] = $this->prepareStatement;

        $data['sql'] = trim($this->sql);
        $data['compiledSQL'] = $this->getCompiledSQL();
        $data['lastSQLExecuted'] = trim($this->lastSQLExecuted);

        $data['selectFields'] = $this->selectFields;

        $data['selectString'] = $this->selectString;
        $data['insertString'] = $this->insertString;
        $data['updateString'] = $this->updateString;
        $data['deleteString'] = $this->deleteString;
        $data['joinsString'] = $this->joinsString;
        $data['whereString'] = $this->whereString;
        $data['orderByString'] = $this->orderByString;

        $data['action'] = $this->action;

        $data['groupByString'] = $this->groupByString;

        $data['replacesValues'] = $this->getReplaceValues();

        $data['resultSet'] = $this->resultSet;

        return json_encode($data);
    }

    /**
     * Conversión a string
     * @return string
     */
    public function jsonSerialize()
    {
        $data = [];

        $data['db'] = self::$db;

        $data['table'] = $this->table;

        $data['prefix_table'] = $this->prefix_table;

        $data['fields'] = $this->fields;

        $data['type_result_assoc'] = $this->type_result_assoc;

        $data['prepareStatement'] = $this->prepareStatement;

        $data['sql'] = trim($this->sql);
        $data['compiledSQL'] = $this->getCompiledSQL();
        $data['lastSQLExecuted'] = trim($this->lastSQLExecuted);

        $data['selectFields'] = $this->selectFields;

        $data['selectString'] = $this->selectString;
        $data['insertString'] = $this->insertString;
        $data['updateString'] = $this->updateString;
        $data['deleteString'] = $this->deleteString;
        $data['joinsString'] = $this->joinsString;
        $data['whereString'] = $this->whereString;
        $data['orderByString'] = $this->orderByString;

        $data['action'] = $this->action;

        $data['groupByString'] = $this->groupByString;

        $data['replacesValues'] = $this->getReplaceValues();

        $data['resultSet'] = $this->resultSet;

        return $data;
    }
}
