<?php

/**
 * CodeStringExceptionsEnum.php
 */

namespace PiecesPHP\Core\Database\Enums;

use Exception;
use PiecesPHP\Core\Database\Exceptions\OptionPatternMismatchException;
use PiecesPHP\Core\Database\Exceptions\RequiredOptionMissedException;
use TypeError;

/**
 * CodeStringExceptionsEnum - Códigos de las excepciones
 *
 * @package     PiecesPHP\Core\Database\Enums
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @copyright   Copyright (c) 2019
 */
class CodeStringExceptionsEnum extends \Exception
{
    const Exception =  Exception::class;
    const TypeError =  TypeError::class;
    const OptionPatternMismatch = OptionPatternMismatchException::class;
    const RequiredOptionMissed = RequiredOptionMissedException::class;
    const UndefinedTable = 'UndefinedTable';
    const UndefinedField = 'UndefinedField';
    const InvalidTypeValueField = 'InvalidTypeValueField';
    const NotAllowedNullField = 'NotAllowedNullField';
    const NotAllowedEmptyField = 'NotAllowedEmptyField';
    const NotValidLengthField = 'NotValidLengthField';
    const UndefinedProperty = 'UndefinedProperty';
    const OnlyReadProperty = 'OnlyReadProperty';
    const PrivateProperty = 'PrivateProperty';
    const InvalidTypeValueProperty = 'InvalidTypeValueProperty';

    const JoinCompareValueIsNotScalar = 'JoinCompareValueIsNotScalar';
    const JoinInvalidOptionName = 'JoinInvalidOptionName';
    const JoinInvalidOptionAmount = 'JoinInvalidOptionAmount';
    const JoinInvalidColumnName = 'JoinInvalidColumnName';

    const WhereCompareValueIsNotScalar = 'WhereCompareValueIsNotScalar';
    const WhereInvalidOptionName = 'WhereInvalidOptionName';
    const WhereInvalidOptionAmount = 'WhereInvalidOptionAmount';
    const WhereInvalidColumnName = 'WhereInvalidColumnName';

    const HavingCompareValueIsNotScalar = 'HavingCompareValueIsNotScalar';
    const HavingInvalidOptionName = 'HavingInvalidOptionName';
    const HavingInvalidOptionAmount = 'HavingInvalidOptionAmount';
    const HavingInvalidColumnName = 'HavingInvalidColumnName';
}
