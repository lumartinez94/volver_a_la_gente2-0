$(document).ready(function(e) {
	let hide = true;
	let linkSm = $(".sub-sm-link");
	let linkSmBefore = $("");
	let idBefore = "";
	let contactButton = $(".menu-button");

	menuClicked();
	try {
		drawNotices();
	} catch (error) {
		//no existe contenedor para noticias
	}

	$(".link-active-sm").click(function() {
		if (hide) {
			hide = false;
			$(".sub-menu-sm").show(900);
		} else {
			hide = true;
			$(".sub-menu-sm").hide(900);
		}
	});

	linkSm.click(e => {
		let id = e.currentTarget.id;
		let container = $(`.${id}-sm`);

		if (id == idBefore) {
			idBefore = "";
			container.hide(500);
		} else {
			linkSmBefore.hide(500);
			linkSmBefore = container;
			idBefore = id;
			container.show(500);
		}
	});

	function menuClicked() {
		contactButton.on("click", function(e) {
			let currentId = e.currentTarget.id;
			
			if (currentId == "contact-trigger") {
				$(".floating-contact").css("display", "flex");
				$(".actions-menu").hide();
				$(".menu-title").hide();
			} else {
				$(".floating-contact").css("display", "none");
			}
		});
	}
	function drawNotices() {
		let data = $(".actions-from-bd").text();

		data = atob(data);
		data = JSON.parse(data);
		if (data.length == 0) {
			$(".noticias-container").hide();
			$(".mas-acciones").hide();
		}

		let template = $("[notice-template]");
		let container = $(".noticias");
		let drawer = "";
		for (const key in data) {
			
			let fecha =  data[key].fecha.split(" ")[0];
			fecha = fecha.split("-");
			fecha = `${fecha[2]}-${fecha[1]}-${fecha[0]}`
			drawer += template
				.html()
				.replace(
					/\{\{TITLE\}\}/gim,
					`${data[key].titulo.substr(0, 90)}...`
				)
				.replace(/\{\{FECHA\}\}/gim, fecha)
				.replace(/\{\{ID\}\}/gim, data[key].friendly_url)
				.replace(/\{\{PORTADA\}\}/gim, data[key].portada);
		}
		$(container).append(drawer);
	}
});
