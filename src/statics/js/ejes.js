$(document).ready(function(e) {
	let page = 1;
	let perPage = 10;
	let showMore = $(".show-more");
	let proyectContainer = $(".articles-container");

	getEjes();

	showMore.click(e => {
		page += 1;
		getEjes();
	});

	function getEjes() {
		let url = $(".ejes-div").attr("route");
		url += `?page=${page}&per_page=${perPage}`;
		let req = getRequest(url);
		req.done(function(res) {
			let data = res;

			if (parseInt(data.page) < data.pages) {
				showMore.show();
			}
			if (parseInt(data.page) == data.pages) {
				showMore.hide();
			}
			drawEjes(data);
			drawNotices(data.noticias);
		});
		req.fail(function(res) {});
		req.always(function(res) {});
	}

	function drawAdvice(categoria) {
		let template = $("[advice-template]");
		proyectContainer.empty();
		let drawer = "";
		drawer = template.html().replace(/\{\{TITULO\}\}/gim, "proyectos");
		$(proyectContainer).append(drawer);
	}

	function drawEjes(data) {
		let template = $("[first-view-ejes-template]");
		let template2 = $("[second-view-ejes-template]");
		let ejesContainer = $(".ejes-div");
		let drawer = "";
		let datos = data.data;

		for (const key in datos) {
			if (key % 2 == 0) {
				let configPortadaP = datos[key].parrafo_portada;
				drawer = template
					.html()
					.replace(/\{\{ID\}\}/gim, datos[key].friendly_url)
					.replace(/\{\{TITULO\}\}/gim, datos[key].titulo)
					.replace(/\{\{PORTADA\}\}/gim, datos[key].portada)
					.replace(/\{\{CONTENIDO\}\}/gim, configPortadaP);

				$(ejesContainer).append(drawer);
			} else {
				let configPortadaP = datos[key].parrafo_portada;
				drawer = template2
					.html()
					.replace(/\{\{ID\}\}/gim, datos[key].friendly_url)
					.replace(/\{\{TITULO\}\}/gim, datos[key].titulo)
					.replace(/\{\{PORTADA\}\}/gim, datos[key].portada)
					.replace(/\{\{CONTENIDO\}\}/gim, configPortadaP);

				$(ejesContainer).append(drawer);
			}
		}
	}
	function drawNotices(notices) {
		let template = $("[notice-template]");
		let container = $(".noticias");
		container.empty();
		let drawer = "";
		if (notices.length == 0) {
			$(".noticias-container").hide();
		} else {
			for (const key in notices) {
				let fecha =  notices[key].fecha.split(" ")[0];
				fecha = fecha.split("-");
				fecha = `${fecha[2]}-${fecha[1]}-${fecha[0]}`
				drawer += template
					.html()
					.replace(
						/\{\{TITLE\}\}/gim,
						`${notices[key].titulo.substr(0, 90)}...`
					)
					.replace(/\{\{FECHA\}\}/gim, fecha)
					.replace(/\{\{ID\}\}/gim, notices[key].friendly_url)
					.replace(/\{\{PORTADA\}\}/gim, notices[key].portada);
			}
			$(container).append(drawer);
		}
	}
});
