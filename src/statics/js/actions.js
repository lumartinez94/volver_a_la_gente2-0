$(document).ready(function(e) {
	let page = 1;
	let perPage = 10;
	let search = $(".search-action");
	let showMore = $(".show-more");
	let proyectContainer = $(".articles-container");
	let showMoreVar = "";
	getActions();
	actionButtons();

	$(".input-search").keypress(e => {
		if (e.keyCode == 13) {
			let whatSearch = $(".input-search").val();
			let url = $(".articles-container").attr("route");
			page = 1;
			perPage = 10;
			showMoreVar = whatSearch;
			url += `?page=${page}&per_page=${perPage}&search=${whatSearch}`;
			let request = getRequest(url);
			request.done(response => {
				proyectContainer.empty();
				if (response.data.length == 0) {
					showMore.hide();
					drawAdvice(whatSearch);
				}
				if (parseInt(response.page) < response.pages) {
					showMore.show();
				}
				if (parseInt(response.page) == response.pages) {
					showMore.hide();
				}
				drawProyects(response);
			});
			request.fail(function(res) {});
			request.always(function(res) {});
		}
	});

	search.click(e => {
		let whatSearch = $(".input-search").val();
		let url = $(".articles-container").attr("route");
		page = 1;
		perPage = 10;
		showMoreVar = whatSearch;
		url += `?page=${page}&per_page=${perPage}&search=${whatSearch}`;
		let request = getRequest(url);
		request.done(response => {
			if (response.data.length == 0) {
				showMore.hide();
				drawAdvice(whatSearch);
			}
			proyectContainer.empty();
			if (parseInt(response.page) < response.pages) {
				showMore.show();
			}
			if (parseInt(response.page) == response.pages) {
				showMore.hide();
			}
			drawProyects(response);
		});
		request.fail(function(res) {});
		request.always(function(res) {});
	});

	showMore.click(e => {
		page += 1;
		getActions(showMoreVar);
	});

	function getActions(categoria = null) {
		if (!categoria) {
			let url = $(".articles-container").attr("route");
			url += `?page=${page}&per_page=${perPage}`;
			let req = getRequest(url);
			req.done(function(res) {
				let data = res;
				
				if (data.data.length == 0) {
					showMore.hide();
					drawAdvice("acciones");
				}

				if (parseInt(data.page) < data.pages) {
					showMore.show();
				}
				if (parseInt(data.page) == data.pages) {
					showMore.hide();
				}
				drawProyects(data);
			});
			req.fail(function(res) {});
			req.always(function(res) {});
		} else {
			showMoreVar = categoria;

			let url = $(".articles-container").attr("route");
			url += `?page=${page}&per_page=${perPage}&categoria=${categoria}`;
			let req = getRequest(url);
			req.done(function(res) {
				let data = res;
			

				if (parseInt(data.page) < data.pages) {
					showMore.show();
				}
				if (parseInt(data.page) == data.pages) {
					showMore.hide();
				}
				if (data.data.length == 0) {
					drawAdvice(categoria);
					showMore.hide();
				} else {
					drawProyects(data);
				}
			});
			req.fail(function(res) {});
			req.always(function(res) {});
		}
	}

	function actionButtons() {
		let actionButton = $(".menu-button");

		actionButton.click(e => {
			$(".input-search").val("");
			proyectContainer.empty();
			page = 1;
			let id = e.currentTarget.id;
			showMoreVar = "";

			$(".sub-menu-sm").hide(900);
			getActions(id);
		});
	}

	function drawProyects(data) {
		let template = $("[first-view-actions-template]");
		let proyectContainer = $(".articles-container");
		let drawer = "";
		let datos = data.data;

		for (const key in datos) {
			if (datos[key].proyectos == "ninguno") {
				datos[key].proyectos = "";
			}
			if (key % 2 == 0) {
				let fecha = datos[key].fecha;
				fecha = fecha.split(" ")[0];
				fecha = fecha.split("-");
				fecha = `${fecha[2]}-${fecha[1]}-${fecha[0]}`;
				drawer = template
					.html()
					.replace(/\{\{ID\}\}/gim, datos[key].friendly_url)
					.replace(/\{\{PORTADA\}\}/gim, datos[key].portada_sup) //cambiar a portada_sup
					.replace(/\{\{FECHA\}\}/gim, fecha)
					.replace(/\{\{CATEGORIA\}\}/gim, datos[key].categoria)
					.replace(/\{\{TITULO\}\}/gim, datos[key].titulo)
					.replace(
						/\{\{STYLE\}\}/gim,
						"actions-container exp2-container"
					);
			} else {
				let fecha = datos[key].fecha;
				fecha = fecha.split(" ")[0];
				fecha = fecha.split("-");
				fecha = `${fecha[2]}-${fecha[1]}-${fecha[0]}`;
				drawer = template
					.html()
					.replace(/\{\{ID\}\}/gim, datos[key].friendly_url)
					.replace(/\{\{PORTADA\}\}/gim, datos[key].portada_sup) //cambiar a portada_sup
					.replace(/\{\{FECHA\}\}/gim, fecha)
					.replace(/\{\{CATEGORIA\}\}/gim, datos[key].categoria)
					.replace(/\{\{TITULO\}\}/gim, datos[key].titulo)
					.replace(/\{\{STYLE\}\}/gim, "actions-container");
			}

			$(proyectContainer).append(drawer);
		}
	}
	function drawAdvice(categoria) {
		let template = $("[advice-template]");
		proyectContainer.empty();
		let drawer = "";
		drawer = template.html().replace(/\{\{TITULO\}\}/gim, categoria);
		$(proyectContainer).append(drawer);
	}
});
