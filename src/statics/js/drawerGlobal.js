$(document).ready(function(e) {
	let data = $(".actions-from-bd").text();
	data = atob(data);
	data = JSON.parse(data);

	drawActions(data);
	function drawActions(data) {
		
		if (data == "ninguno") {
			$('.mas-acciones').hide();
		}else{
		let container = $(".acciones-container");
		let template = $("[actions-cards-template]");
		let drawer = "";
		for (const key in data) {
			drawer += template
				.html()
				.replace(
					/\{\{TITLE\}\}/gim,
					`${data[key].titulo.substr(0, 50)}...`
				)
				.replace(/\{\{AGE\}\}/gim, data[key].fecha)
				.replace(/\{\{ID\}\}/gim, data[key].friendly_url)
				.replace(/\{\{PORTADA\}\}/gim, data[key].portada);
		}
		$(container).append(drawer);
	}
	}
});
