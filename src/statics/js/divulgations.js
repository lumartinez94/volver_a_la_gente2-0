$(document).ready(function (e) {

    let page = 1
    let perPage = 6
    let search = $('.search-action')
    let showMore = $('.show-more')
    let proyectContainer = $('.cards-container')
    let modalTemplate = $('[modal-template]')
    let showMoreVar = ""

    getDivulgations()
    actionButtons()

    search.click((e) => {
        let whatSearch = $('.input-search').val()
        let url = $('.cards-container').attr('route')
        showMoreVar = whatSearch
        url += `?page=${page}&per_page=${perPage}&search=${whatSearch}`
        let request = getRequest(url)
        request.done((response) => {
            proyectContainer.empty()
            if (parseInt(response.page) < response.pages) {
                showMore.show()
            }
            if (parseInt(response.page) == response.pages) {
                showMore.hide()
            }
            drawCards(response)
        })
        request.fail(function (res) { })
        request.always(function (res) { })
    })
    showMore.click((e) => {
        page += 1
        getDivulgations(showMoreVar)
    })


    function getDivulgations(categoria = null) {

        if (!categoria) {
            let url = $('.cards-container').attr('route')
            url += `?page=${page}&per_page=${perPage}`
            let req = getRequest(url)
            req.done(function (res) {
                let data = res;
                if (parseInt(data.page) < data.pages) {
                    showMore.show()
                }
                if (parseInt(data.page) == data.pages) {
                    showMore.hide()
                }
                drawCards(data);
            })
            req.fail(function (res) { })
            req.always(function (res) { })

        } else {
            showMoreVar = categoria;
            let url = $('.cards-container').attr('route')
            url += `?page=${page}&per_page=${perPage}&categoria=${categoria}`
            let req = getRequest(url)
            req.done(function (res) {
                let data = res;
                if (parseInt(data.page) < data.pages) {
                    showMore.show()
                }
                if (parseInt(data.page) == data.pages) {
                    showMore.hide()
                }

                if (data.data.length == 0) {
                    drawAdvice(categoria)
                    showMore.hide()

                } else {
                    drawCards(data);
                }
            })
            req.fail(function (res) { })
            req.always(function (res) {
            })
        }

    }

    function actionButtons() {
        let actionButton = $('.menu-button')

        actionButton.click((e) => {

            proyectContainer.empty();
            page = 1
            let id = e.currentTarget.id
            $('.sub-menu-sm').hide(900)
            getDivulgations(id)
        })
    }

    function drawCards(data) {

        let template = $('[first-view-divulgation-template]')
        let proyectContainer = $('.cards-container')
        let drawer = ""
        let datos = data.data

        for (const key in datos) {
            let titleLength = datos[key].title.length
            let titulo = ""
            if (titleLength >= 100) {
                titulo = `${datos[key].title.substr(0,100)}...`
            }else{
                titulo = datos[key].title
            }
            

            drawer = template.html().replace(/\{\{ID\}\}/gmi, datos[key].id)
                .replace(/\{\{COVER\}\}/gmi, datos[key].cover)
                .replace(/\{\{DATE\}\}/gmi, datos[key].date)
                .replace(/\{\{TITLE\}\}/gmi, titulo )

            proyectContainer.append(drawer)

            let buttonMore = proyectContainer.find(`[data-id="${datos[key].id}"]`).find('.btn-more')

            buttonMore.click((e) => {
                let that = $(e.target)
                let parent = that.parents(`[data-id]`)
                let id = parent.attr('data-id')
                for (const key in datos) {
                    
                    if (datos[key].id == id) {
                        
                        drawModal(datos[key])
                        $('.modal-details').modal('show')

                    }
                }

            })
    

        }

    }
    function drawModal(datos) {
        let container = $('.modal-details')
        container.empty()
        let drawer = ""
        
        drawer = modalTemplate.html().replace(/\{\{COVER\}\}/gmi, datos.cover)
            .replace(/\{\{DATE\}\}/gmi, datos.date)
            .replace(/\{\{ID\}\}/gmi, datos.id)
            .replace(/\{\{TITLE\}\}/gmi, datos.title)
            .replace(/\{\{DESCRIPTION\}\}/gmi, datos.description.replace(/\\/g,''))
            .replace(/\{\{ARCHIVO\}\}/gmi, datos.archivo)
            .replace(/\{\{VIDEO\}\}/gmi, datos.video)

            
        container.append(drawer)
        
        if (datos.archivo === null) {
            container.find(`[data-id='${datos.id}'] .out-btn`).css('display','none')
        }
        if (datos.video === "") {
            container.find(`[data-id='${datos.id}'] .play`).css('display','none')
        }
    }
    function drawAdvice(categoria) {
        let template = $('[advice-template]')
        proyectContainer.empty();
        let drawer = ""
        drawer = template.html().replace(/\{\{TITLE\}\}/gmi, categoria)
        $(proyectContainer).append(drawer)
        
    }

})