$(document).ready(function(e) {
	let page = 1;
	let perPage = 10;
	let showMore = $(".show-more");
	let proyectContainer = $(".articles-container");

	getProyects();

	showMore.click(e => {
		page += 1;
		getProyects();
	});

	function getProyects() {
		let url = $(".articles-container").attr("route");
		url += `?page=${page}&per_page=${perPage}`;
		let req = getRequest(url);
		req.done(function(res) {
			let data = res;

			if (parseInt(data.page) < data.pages) {
				showMore.show();
			}
			if (parseInt(data.page) == data.pages) {
				showMore.hide();
			}
			drawProyects(data);
		});
		req.fail(function(res) {});
		req.always(function(res) {});
	}
	function drawProyects(data) {
		let template = $("[first-view-proyects-template]");
		let drawer = "";
		let datos = data.data;
		if (datos.length == 0) {
			drawAdvice();
			showMore.hide();
		} else {
			for (const key in datos) {
				if (key % 2 == 0) {
					drawer = template
						.html()
						.replace(/\{\{ID\}\}/gim, datos[key].friendly_url)
						.replace(/\{\{PORTADA\}\}/gim, datos[key].portada)
						.replace(
							/\{\{EJETEMATICO\}\}/gim,
							datos[key].eje_tematico
						)
						.replace(/\{\{TITULO\}\}/gim, datos[key].titulo)
						.replace(
							/\{\{STYLE\}\}/gim,
							"actions-container exp2-container"
						);
				} else {
					drawer = template
						.html()
						.replace(/\{\{ID\}\}/gim, datos[key].friendly_url)
						.replace(/\{\{PORTADA\}\}/gim, datos[key].portada)
						.replace(
							/\{\{EJETEMATICO\}\}/gim,
							datos[key].eje_tematico
						)
						.replace(/\{\{TITULO\}\}/gim, datos[key].titulo)
						.replace(/\{\{STYLE\}\}/gim, "actions-container");
				}

				$(proyectContainer).append(drawer);
			}
		}
	}
	function drawAdvice(categoria) {
		let template = $("[advice-template]");
		proyectContainer.empty();
		let drawer = "";
		drawer = template.html().replace(/\{\{TITULO\}\}/gim, "proyectos");
		$(proyectContainer).append(drawer);
	}
});
