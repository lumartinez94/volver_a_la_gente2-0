$(document).ready(function (e) {

    let options = {
        aspectRatio: 16 / 9,
        responsive: true,
        checkCrossOrigin: false,
        center: true,
    }

    $('.ui.top.attached.tabular.menu .item').tab({
		context: 'parent'
	})


    let cropper = new CropperAdapterComponent(
		{
			containerSelector: "[cropper-project]",
			minWidth: 1000,
			outputWidth: 900,
			cropperOptions: {
                aspectRatio: 16/ 9,
                responsive: true,
                checkCrossOrigin: false,
                center: true,
			},
		}
	);
   let form= genericFormHandler('[action-form]', {
        onSetFormData: function (formData) {
            if (cropper.wasChanged()) {
                formData.set('portada', cropper.getFile('portada', 0.7));
                formData.set('portada-sm', cropper.getFile('portada-sm', 0.7, 400))
            }
            
            
            
            return formData
        }
    })

    let quillAdapter = new QuillAdapterComponent({
		containerSelector: '[quill-editor]',
		textareaTargetSelector: "textarea[name='description']",
		urlProcessImage: form.attr('quill'),
		nameOnRequest: 'image',
	})



})