window.addEventListener('load', () => {
	
	let projectStatus = $(".ui.checkbox");
	projectValue = $(".status-value").val();
	if (projectValue == 1 || projectValue == "approved") {
		projectStatus.checkbox("set checked");
	}


	$('.ui.top.attached.tabular.menu .item').tab({
		context: 'parent'
	})

	let options = {
		aspectRatio: 16/ 9,
		responsive: true,
		checkCrossOrigin: false,
		center: true
	};
	let isEdit = false

	let cropper = new CropperAdapterComponent(
		{
			containerSelector: "[cropper-project]",
			minWidth: 1000,
			outputWidth: 1000,
			cropperOptions: {
                
                aspectRatio: 4/ 3,
                responsive: true,
		    checkCrossOrigin: false,
		    center: true
			},
		}
	);


let form=	genericFormHandler("[action-form]", {
		onSetFormData: function (formData) {
			if (cropper.wasChanged()) {
				formData.set("portada", cropper.getFile("portada", 0.7,800));
				formData.set(
					"portada-sm",
					cropper.getFile("portada-sm", 0.7, 400)
				);
			}
			return formData;
		}
	});

	let quillAdapter = new QuillAdapterComponent({
		containerSelector: '[quill-editor]',
		textareaTargetSelector: "textarea[name='description']",
		urlProcessImage: form.attr('quill'),
		nameOnRequest: 'image',
	})

	isEdit = form.find(`[name="id"]`).length > 0;

});
