<?php
/**
 * Rutas
 *
 * En este archivo se definen las rutas de la aplicación.
 *
 * El sistema de rutas usa Slim 3.*
 *
 * Nótese que la forma de agregar las rutas no es la convencional en slim, sino que se usan
 * las clases PiecesPHP\Core\Route y PiecesPHP\Core\RouteGroup. Esto es así para que a PiecesPHP se le
 * facilite un manejo automático más sencillo de sus funciones de permisos basados en roles y de usuarios.
 *
 * Ejemplo:
 *
 * Grupo:
 *
 * string $name
 * $grupo = new PiecesPHP\Core\RouteGroup($name)
 *
 * Ruta:
 *
 * string $route La ruta.
 *
 * string $controller El nombre de la clase seguido de dos puntos y el nombre de método. Ej: '\App\MiClase:index'
 *
 * [string $name = uniqid()] Nombre con el que se registrá la ruta
 * [string $method = 'GET'] Método aceptado por la ruta (método HTTP ej.: GET|POST|PUT...)
 *
 * [bool $require_login = false] Si la ruta necesita de estar logueado o no. Esto si se está usando el sistema de usuarios de PiecesPHP
 *
 * [string $route_alias = null] Un alias para la ruta.
 *
 * [int[]|string[] $roles_allowed Roles = []] Roles que pueden acceder a la vista si es necesario estar logueado.
 * Forma alternativa al archivo roles.php de permitir el acceso a una vista.
 *
 * new PiecesPHP\Core\Route($route, $controller, $name, $method, $requireLogin, $alias, $rolesAllowed, $defaultParamsValues)
 *
 * Agregar ruta
 *
 * $grupo->register([$ruta, ...])
 *
 */
use App\Controller\AdminPanelController;
use App\Controller\AppConfigController;
use App\Controller\AvatarController;
use App\Controller\BlackboardNewsController;
use App\Controller\GenericTokenController;
use App\Controller\ImporterController;
use App\Controller\LoginAttemptsController;
use App\Controller\MessagesController;
use App\Controller\PagesController;
use App\Controller\TimerController;
use App\Locations\Controllers\Locations;
use PiecesPHP\BuiltIn\Action\Controllers\ActionController;
use PiecesPHP\BuiltIn\Article\Controllers\ArticleController;
use PiecesPHP\BuiltIn\Divulgation\Controllers\DivulgationController;
use PiecesPHP\BuiltIn\Eje\Controllers\EjeController;
use PiecesPHP\BuiltIn\Proyect\Controllers\ProyectController;
use PiecesPHP\Core\Route as PiecesRoute;
use PiecesPHP\Core\RouteGroup as PiecesRouteGroup;
use PiecesPHP\Core\ServerStatics;
use PiecesPHP\Core\Test;

$prefix_lang = get_config('prefix_lang');
$slim_app = get_config('slim_app');
PiecesRouteGroup::setRouter($slim_app);

//──── GRUPOS DE RUTAS ───────────────────────────────────────────────────────────────────
$zona_publica = new PiecesRouteGroup($prefix_lang); //Zona pública
$zona_administrativa = new PiecesRouteGroup($prefix_lang . '/admin'); //Zona administrativa
$configurations = new PiecesRouteGroup($prefix_lang . '/configurations'); //Zona administrativa
$sistema_usuarios = new PiecesRouteGroup($prefix_lang . '/users/'); //Sistema de usuarios
$tickets = new PiecesRouteGroup($prefix_lang . '/tickets'); //Sistema de tickets
$timing = new PiecesRouteGroup($prefix_lang . '/timing'); //Temporizadores
$mensajeria = new PiecesRouteGroup($prefix_lang . '/messages'); //Mensajería
$locations = new PiecesRouteGroup($prefix_lang . '/locations'); //Ubicaciones
$sistema_tablero_noticias = new PiecesRouteGroup($prefix_lang . '/blackboard-news/'); //Servido personalizado de archivos estáticos
$importadores = new PiecesRouteGroup($prefix_lang . '/importers'); //Importadores
$articles = new PiecesRouteGroup($prefix_lang . '/articles'); //Blog
$ejes = new PiecesRouteGroup($prefix_lang . '/ejes'); //Ejes
$proyects = new PiecesRouteGroup($prefix_lang . '/proyects'); //Proyectos
$actions = new PiecesRouteGroup($prefix_lang . '/actions'); //Actions
$divulgations = new PiecesRouteGroup($prefix_lang . '/divulgations'); //divulgations
$sistema_avatares = new PiecesRouteGroup($prefix_lang . '/avatars'); //Sistema de usuarios
$servidor_estaticos = new PiecesRouteGroup($prefix_lang . '/statics/'); //Servido personalizado de archivos estáticos
$token_handler = new PiecesRouteGroup($prefix_lang . '/tokens'); //Servido personalizado de archivos estáticos

//──── REGISTRAR RUTAS ───────────────────────────────────────────────────────────────────


//Rutas básicas de la zona administrativa
AdminPanelController::routes($zona_administrativa);

//Personalización de configuraciones
AppConfigController::routes($configurations);

//Informes de inicio de sesión
LoginAttemptsController::routes($zona_administrativa);

//Sistema de usuarios
AdminPanelController::usersRoutes($sistema_usuarios);

//Tickets
AdminPanelController::ticketsRoutes($tickets);

//Temporizador
TimerController::routes($timing);

//Mensajería
MessagesController::routes($mensajeria);

//Ubicaciones
Locations::routes($locations);

//Tablero de noticias
BlackboardNewsController::routes($sistema_tablero_noticias);

//Importadores
ImporterController::routes($importadores);

//Blog
ArticleController::routes($articles);

//Manejador de tokens
GenericTokenController::routes($token_handler);
//Ejes
EjeController::routes($ejes);

//Proyectos
ProyectController::routes($proyects);

//Acciones
ActionController::routes($actions);

//divulgaciones
DivulgationController::routes($divulgations);
$zona_publica->register([
    new PiecesRoute('[/]', PagesController::class . ':home', 'public-home'),
    new PiecesRoute('/sobre-nosotros[/]', PagesController::class . ':sobreNosotros', 'public-nosotros'),
    new PiecesRoute('/ejes-tematicos[/]', PagesController::class . ':ejesTematicos', 'public-ejes'),
    new PiecesRoute('/ejes-tematico/{id}[/]', PagesController::class . ':ejesTematicosDetalles', 'public-ejes-detalle'),
    new PiecesRoute('/sobre-nosotros/corporacion[/]', PagesController::class . ':corporacion', 'public-historia'),
    new PiecesRoute('/sobre-nosotros/indicadores[/]', PagesController::class . ':indicadores', 'public-indicadores'),
    new PiecesRoute('/sobre-nosotros/experiencias[/]', PagesController::class . ':experiencias', 'public-experiencias'),
    new PiecesRoute('/acciones[/[{nombre}[/]]]', PagesController::class . ':acciones', 'public-acciones'),
    new PiecesRoute('/accion/{id}[/]', PagesController::class . ':accionesDetalle', 'public-acciones-detalle'),
	new PiecesRoute('/proyectos[/]', PagesController::class . ':proyectos', 'public-proyectos'),
    new PiecesRoute('/agencia-empleo[/]', PagesController::class . ':agenciasEmpleo', 'public-agencias'),	
    new PiecesRoute('/proyecto/{id}[/]', PagesController::class . ':proyectosDetalles', 'public-proyectos-detalle'),
    new PiecesRoute('/divulgacion[/[{nombre}[/]]]', PagesController::class . ':divulgacion', 'public-divulgacion'),

]);

$sistema_avatares->register(
    [
        //──── GET ───────────────────────────────────────────────────────────────────────────────
        new PiecesRoute('/get[/]', AvatarController::class . ':avatar', 'avatars', 'GET', true, null),
        //──── POST ──────────────────────────────────────────────────────────────────────────────
        new PiecesRoute('/push[/]', AvatarController::class . ':register', 'push-avatars', 'POST', true),
    ]
);

$servidor_estaticos->register(
    [
        //──── GET ───────────────────────────────────────────────────────────────────────────────
        new PiecesRoute('[{params:.*}]', ServerStatics::class . ':serve', 'statics-files'),
    ]
);

//──── RUTAS OPCIONALES ──────────────────────────────────────────────────────────────────

$generacion_imagenes = new PiecesRouteGroup($prefix_lang . '/img-gen/'); //Generación de imágenes
$generacion_imagenes->active(true); //Grupo activo/inactivo
$generacion_imagenes->register(
    [
        //──── GET ───────────────────────────────────────────────────────────────────────────────
        new PiecesRoute('{w}/{h}[/]', Test::class . ':generateImage', 'img-gen'),
    ]
);

$tests = new PiecesRouteGroup($prefix_lang . '/overview'); //Muestra de algunas funciones
$tests->active(true); //Grupo activo/inactivo
$tests->register(
    [
        //──── GET ───────────────────────────────────────────────────────────────────────────────
        new PiecesRoute('[/]', Test::class . ':index', 'home-test'),
        new PiecesRoute('/image-generator/{w}/{h}[/]', Test::class . ':generateImage', 'image-gen'),
        new PiecesRoute('/overview-2[/]', Test::class . ':overviewBack', 'back-test'),
    ]
);
