<?php

/**
 * UsersModel.php
 */
namespace App\Model;

use PiecesPHP\Core\BaseEntityMapper;

/**
 * UsersModel.
 *
 * Modelo de Usuarios.
 *
 * @package     PiecesPHP\Core
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @version     v.1.0
 * @copyright   Copyright (c) 2018
 */
class EjesModel extends BaseEntityMapper
{
    const PROYECT_TABLE = 'ejes';

    protected static $where = [];
    protected $fields = [
        'id'=>[
            'type'=>'int',
            'primary_key'=> true,
        ],
        'titulo'=>[
            'type'=>'varchar',
        ],
        'parrafo_portada'=>[
            'type'=>'varchar',
        ],
        'contenido'=>[
            'type'=>'mediumtext',
        ],
        'portada'=>[
            'type'=>'varchar',
        ]
        ];
        
    protected $table = 'ejes';
    /**
     * __construct
     *
     * @param integer $id
     * @return static
     */
    public function __construct(int $id = null)
    {
        parent::__construct($id);
    }

    public function saveEjes($json)
    {
        $model = $this->getModel();
        $model->resetAll();
        return $model->insert($json)->execute();
    }

    public function bringEjes(){
        $model = $this->getModel();
        $model->resetAll();
        $model->select(['id','titulo','parrafo_portada','portada'])->execute(true);
        $result = $model->result();

        return $result;
    }

    public function ejesDetails($id)
    {
        $model = $this->getModel();
        $model->resetAll();
        $model->select(['id','titulo','contenido','portada'])->where('id = '.$id)->execute();
        $result = $model->result();
        return $result;
    }
}