<?php

/**
 * UsersModel.php
 */
namespace App\Model;

use PiecesPHP\Core\BaseEntityMapper;

/**
 * UsersModel.
 *
 * Modelo de Usuarios.
 *
 * @package     PiecesPHP\Core
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @version     v.1.0
 * @copyright   Copyright (c) 2018
 */
class ProyectsModel extends BaseEntityMapper
{
    const PROYECT_TABLE = 'proyectos';

    protected static $where = [];
    protected $fields = [
        'id'=>[
            'type'=>'int',
            'primary_key'=> true,
        ],
        'eje_tematico'=>[
            'type'=>'text',
        ],
        'titulo'=>[
            'type'=>'varchar',
        ],
        'fecha'=>[
            'type'=>'varchar',
        ],
        'aliados'=>[
            'type'=>'text',
        ],
        'ubicacion'=>[
            'type'=>'varchar',
        ],
        'contenido'=>[
            'type'=>'mediumtext',
        ],
        'autor'=>[
            'type'=>'varchar',
        ]
        ];
        
    protected $table = 'proyectos';
    /**
     * __construct
     *
     * @param integer $id
     * @return static
     */
    public function __construct(int $id = null)
    {
        parent::__construct($id);
    }

    public function saveProyect($json)
    {
        $model = $this->getModel();
        $model->resetAll();
        return $model->insert($json)->execute();
    }

    public function bringProyects(){
        $model = $this->getModel();
        $model->resetAll();
        $model->select(['id','eje_tematico','titulo','autor','portada'])->execute(true);
        $result = $model->result();

        return $result;
    }

    public function proyectDetails($id)
    {
        $model = $this->getModel();
        $model->resetAll();
        $model->select(['id','eje_tematico','titulo','fecha','aliados','ubicacion','autor','contenido','portada'])->where('id = '.$id)->execute();
        $result = $model->result();
        return $result;
    }
}