<?php

namespace App\Controller;

use App\Model\EjesModel;
use PiecesPHP\BuiltIn\Action\Mappers\ActionMapper;
use PiecesPHP\BuiltIn\Divulgation\Mappers\DivulgationMapper;
use PiecesPHP\BuiltIn\Eje\Mappers\EjeMapper;
use PiecesPHP\BuiltIn\Proyect\Mappers\ProyectMapper;
use PiecesPHP\Core\Utilities\Helpers\MetaTags;
use Slim\Exception\NotFoundException;
use \Slim\Http\Request as Request;
use \Slim\Http\Response as Response;

/**
 * Pages Controller
 *
 * @package     App\Controller
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @version     v.1.0
 * @copyright   Copyright (c) 2018
 */
class PagesController extends \PiecesPHP\Core\BaseController
{

    public function __construct()
    {
        //ArticleController::routeName('ajax-single',['article'=>'nihil-enim-dolor-harum-qui'])
        //ArticleController::routeName('ajax-all-category',['category'=>'general'])
        //ArticleController::routeName('ajax-all')
        parent::__construct(false);
        import_jquery();
        import_app_libraries([], true);

    }

    /**
     * home
     *
     * @param Request $req
     * @param Response $res
     * @param array $args
     * @return Response
     * @throws NotFoundException
     */
    public function home(Request $req, Response $res, array $args)
    {

        $acciones = ActionMapper::all(false, 1, 3);
        $ejes = EjeMapper::randomEje(false, 1, 1);
        import_jquery();
        MetaTags::setTitle('Home');
        set_custom_assets([
            base_url('statics/js/global.js'),
        ], 'js');

        set_custom_assets([
            baseurl('statics/css/global.css'),
        ], 'css');

        $data['title'] = "INICIO";
        $data['isHome'] = true;
        $data['active_menu_actions'] = "";
        $data['accion_preview'] = base64_encode(json_encode($acciones));
        $data['link1'] = '';
        $data['link2'] = '';
        $data['link3'] = '';
        $data['ejes'] = $ejes;

        $this->setVariables($data);
        $this->render('layout/header');
        $this->render('layout/menu');
        $this->render("pages/home");
        $this->render('layout/footer');
        return $res;

    }

    /**
     * sobre nosotros
     *
     * @param Request $req
     * @param Response $res
     * @param array $args
     * @return Response
     * @throws NotFoundException
     */
    public function sobreNosotros(Request $req, Response $res, array $args)
    {
        import_jquery();
        set_custom_assets([
            base_url('statics/js/global.js'),
        ], 'js');
        MetaTags::setTitle('Nuestro Fundador');

        set_custom_assets([
            baseurl('statics/css/global.css'),
        ], 'css');
        $data['title'] = "SOBRE NOSOTROS";
        $data['active_menu_actions'] = "";
        $data['link1'] = '';
        $data['link2'] = '';
        $data['link3'] = '';
        $this->setVariables($data);
        $this->render('layout/header');
        $this->render('layout/menu');
        $this->render("pages/sobre-nosotros");
        $this->render('layout/footer');
        return $res;
    }

    /**
     * La corporación
     *
     * @param Request $req
     * @param Response $res
     * @param array $args
     * @return Response
     * @throws NotFoundException
     */
    public function corporacion(Request $req, Response $res, array $args)
    {
        import_jquery();
        set_custom_assets([
            base_url('statics/js/global.js'),
        ], 'js');

        set_custom_assets([
            baseurl('statics/css/global.css'),
        ], 'css');

        MetaTags::setTitle('La corporación');

        $data['title'] = "SOBRE NOSOTROS - LA CORPORACIÓN";
        $data['active_menu_actions'] = "";
        $data['link1'] = '';
        $data['link2'] = '';
        $data['link3'] = '';
        $this->setVariables($data);
        $this->render('layout/header');
        $this->render('layout/menu');
        $this->render("pages/corporacion");
        $this->render('layout/footer');
        return $res;
    }

    /**
     * angencias de empleo
     *
     * @param Request $req
     * @param Response $res
     * @param array $args
     * @return Response
     * @throws NotFoundException
     */
    public function agenciasEmpleo(Request $req, Response $res, array $args)
    {
        import_jquery();
        set_custom_assets([
            base_url('statics/js/global.js'),
        ], 'js');

        set_custom_assets([
            baseurl('statics/css/global.css'),
        ], 'css');

        MetaTags::setTitle('Agencia de empleo');

        $data['title'] = "AGENCIA DE EMPLEO";
        $data['active_menu_actions'] = "";
        $data['link1'] = '';
        $data['link2'] = '';
        $data['link3'] = '';
        $this->setVariables($data);
        $this->render('layout/header');
        $this->render('layout/menu');
        $this->render("pages/agencias-empleo");
        $this->render('layout/footer');
        return $res;
    }

    /**
     * experiencias
     *
     * @param Request $req
     * @param Response $res
     * @param array $args
     * @return Response
     * @throws NotFoundException
     */
    public function experiencias(Request $req, Response $res, array $args)
    {
        import_jquery();
        set_custom_assets([
            base_url('statics/js/global.js'),
        ], 'js');

        set_custom_assets([
            baseurl('statics/css/global.css'),
        ], 'css');

        MetaTags::setTitle('Experiencias');

        $data['title'] = "SOBRE NOSOTROS - EXPERIENCIAS";
        $data['active_menu_actions'] = "";
        $data['link1'] = '';
        $data['link2'] = '';
        $data['link3'] = '';
        $this->setVariables($data);
        $this->render('layout/header');
        $this->render('layout/menu');
        $this->render("pages/experiencias");
        $this->render('layout/footer');
        return $res;
    }

    /**
     * indicadores
     *
     * @param Request $req
     * @param Response $res
     * @param array $args
     * @return Response
     * @throws NotFoundException
     */
    public function indicadores(Request $req, Response $res, array $args)
    {
        import_jquery();
        set_custom_assets([
            base_url('statics/js/global.js'),
        ], 'js');

        set_custom_assets([
            baseurl('statics/css/global.css'),
        ], 'css');

        MetaTags::setTitle('Indicadores');

        $data['title'] = "SOBRE NOSOTROS - INDICADORES";
        $data['active_menu_actions'] = "";
        $data['link1'] = '';
        $data['link2'] = '';
        $data['link3'] = '';
        $this->setVariables($data);
        $this->render('layout/header');
        $this->render('layout/menu');
        $this->render("pages/indicadores");
        $this->render('layout/footer');
        return $res;
    }

    /**
     * acciones
     *
     * @param Request $req
     * @param Response $res
     * @param array $args
     * @return Response
     * @throws NotFoundException
     */
    public function acciones(Request $req, Response $res, array $args)
    {

        $nombre = $req->getAttribute('nombre', null);
        if ($req->isXhr()) {
            $categoria = $req->getQueryParam('categoria', null);
            $search = $req->getQueryParam('search', null);
            $page = $req->getQueryParam('page', 1);
            $perPage = $req->getQueryParam('perPage', 10);

            $acciones = [];
            if (is_null($categoria) && is_null($search)) {
                $total = ActionMapper::model()->rowCount('id');
                $total = $total === false ? -1 : $total;
                $acciones = ActionMapper::all(false, $page, $perPage);
            } else if (!is_null($categoria) && is_null($search)) {
                $model = ActionMapper::model();
                $total = $model->select('count(id) As counter')->where(['categoria' => $categoria])->execute(true);
                $total = $model->result();
                $total = $total[0]['counter'];
                $acciones = ActionMapper::allBy($categoria, 'categoria', false, $page, $perPage);
            } else if (!is_null($search) && is_null($categoria)) {
                $model = ActionMapper::model();
                $total = $model->select('count(id) As counter')->where('titulo LIKE "%' . $search . '%"')->execute(true);
                $total = $model->result();
                $total = $total[0]['counter'];
                $acciones = ActionMapper::search($search, false, $page, $perPage);
            }

            foreach ($acciones as $key => $accion) {
                $array_projects = json_decode($acciones[$key]->proyectos);
                $projects = "";
                for ($i = 0; $i < sizeof($array_projects); $i++) {
                    $projects .= $array_projects[$i] . ", <br>";
                }
                $acciones[$key]->proyectos = $projects;
            }

            return $res->withJson([
                'data' => $acciones,
                'page' => $page,
                'perPage' => $perPage,
                'pages' => ceil($total / $perPage),
                'total' => $total,
            ]);

        } else {

            set_custom_assets([
                base_url('statics/js/global.js'),
                base_url('statics/js/actions.js'),
            ], 'js');

            set_custom_assets([
                baseurl('statics/css/global.css'),
            ], 'css');

            MetaTags::setTitle('Acciones');

            $data['title'] = "ACCIONES";
            $data['route'] = is_null($nombre) ? get_route('public-acciones') : get_route('public-acciones', ['nombre' => $nombre]);
            $data['active_menu_actions'] = "active_menu";
            $data['link1'] = 'NOTICIAS';
            $data['link2'] = 'ARTICULOS';
            $data['link3'] = 'EVENTOS';
            $this->setVariables($data);
            $this->render('layout/header');
            $this->render('layout/menu');
            $this->render("pages/acciones");
            $this->render('layout/footer');

            return $res;
        }
    }

    /**
     * divulgacion
     *
     * @param Request $req
     * @param Response $res
     * @param array $args
     * @return Response
     * @throws NotFoundException
     */
    public function divulgacion(Request $req, Response $res, array $args)
    {

        $nombre = $req->getAttribute('nombre', null);
        if ($req->isXhr()) {
            $categoria = $req->getQueryParam('categoria', null);
            $id = $req->getQueryParam('id', null);
            $search = $req->getQueryParam('search', null);
            $page = $req->getQueryParam('page', 1);
            $perPage = $req->getQueryParam('per_page', 10);
            $acciones = [];
            if (is_null($categoria) && is_null($search)) {
                $total = DivulgationMapper::model()->rowCount('id');
                $total = $total === false ? -1 : $total;
                $acciones = DivulgationMapper::all(false, $page, $perPage);
            } else if (!is_null($categoria) && is_null($search)) {
                $model = DivulgationMapper::model();
                $total = $model->select('count(id) As counter')->where(['category' => $categoria])->execute(true);
                $total = $model->result();
                $total = $total[0]['counter'];
                $acciones = DivulgationMapper::allBy($categoria, 'category', false, $page, $perPage);
            } else if (!is_null($search) && is_null($categoria)) {
                $model = DivulgationMapper::model();
                $total = $model->select('count(id) As counter')->where('title LIKE "%' . $search . '%"')->execute(true);
                $total = $model->result();
                $total = $total[0]['counter'];
                $acciones = DivulgationMapper::search($search, false, $page, $perPage);
            }

            return $res->withJson([
                'data' => $acciones,
                'page' => $page,
                'perPage' => $perPage,
                'pages' => ceil($total / $perPage),
                'total' => $total,
            ]);

        } else {
            import_semantic();
            set_custom_assets([
                base_url('statics/js/global.js'),
                base_url('statics/js/divulgations.js'),
            ], 'js');

            set_custom_assets([
                baseurl('statics/css/global.css'),
            ], 'css');

            $data['title'] = "DIVULGACIÓN";
            MetaTags::setTitle('Divulgación');
            $data['route'] = is_null($nombre) ? get_route('public-divulgacion') : get_route('public-divulgacion', ['nombre' => $nombre]);
            $data['active_menu_actions'] = "active_menu";
            $data['link1'] = 'PIEZAS GRÁFICAS';
            $data['link2'] = 'VIDEOS';
            $data['link3'] = 'PUBLICACIONES';

            $this->setVariables($data);
            $this->render('layout/header');
            $this->render('layout/menu');
            $this->render("pages/divulgacion");
            $this->render('layout/footer');

            return $res;
        }
    }
    /**
     * divulgacionDetails
     *
     * @param Request $req
     * @param Response $res
     * @param array $args
     * @return Response
     * @throws NotFoundException
     */
    public function divulgacionDetails(Request $req, Response $res, array $args)
    {
        $id = $req->getAttribute('id', null);

        $detail = DivulgationMapper::existsByID($id);
        return $res->withJson([
            'data' => $detail,
        ]);

    }
    /**
     * acciones Detalles
     *
     * @param Request $req
     * @param Response $res
     * @param array $args
     * @return Response
     * @throws NotFoundException
     */
    public function accionesDetalle(Request $req, Response $res, array $args)
    {
        $id = $req->getAttribute('id');
        import_jquery();
        set_custom_assets([
            base_url('statics/js/global.js'),
            base_url('statics/js/drawerGlobal.js'),
        ], 'js');

        set_custom_assets([
            baseurl('statics/css/global.css'),
        ], 'css');
        $data['link1'] = '';
        $data['link2'] = '';
        $data['link3'] = '';

        $details = ActionMapper::getByFriendlyURL($id);

        $data['title'] = "ACCIONES";
        $data['active_menu_actions'] = "";
        $data['titulo'] = $details->titulo;
        MetaTags::setTitle($details->titulo);
        if ($details->proyectos == "ninguno") {
            $details->proyectos = "";
        }
        $array_projects = json_decode($details->proyectos);
        $projects = "";
        for ($i = 0; $i < sizeof($array_projects); $i++) {
            $projects .= $array_projects[$i] . ", <br>";
        }
        $show = "";

        if ($array_projects[0] == "ninguno" || !isset($array_projects)) {
            $show = "none";
        }

        $verbose_date = $details->fecha;
        $verbose_date = explode(" ", $verbose_date);
        $verbose_date = $verbose_date[0];
        $verbose_date = explode("-", $verbose_date);
        $verbose_month = ltrim($verbose_date[1], "0");
        $verbose_month = MONTHS_ARRAY[$verbose_month];
        $verbose_date = $verbose_date[2] . " de " . $verbose_month . ", " . $verbose_date[0];

        $data['proyecto'] = $projects;
        $data['show_projects'] = $show;
        $data['fecha'] = $verbose_date;
        $data['categoria'] = ucfirst($details->categoria);
        $data['portada'] = $details->portada;
        $data['contenido'] = stripslashes($details->contenido);
        
        $data['autor'] = $details->autor;

        $acciones = ProyectMapper::getProjectByAction($details->proyectos, false, 1, 3);
        $data['accion_preview'] = base64_encode(json_encode($acciones));

        $this->setVariables($data);
        $this->render('layout/header');
        $this->render('layout/menu');
        $this->render("pages/acciones-detalles");
        $this->render('layout/footer');
        return $res;
    }

    /**
     * proyectos
     *
     * @param Request $req
     * @param Response $res
     * @param array $args
     * @return Response
     * @throws NotFoundException
     */
    public function proyectos(Request $req, Response $res, array $args)
    {
        $data['link1'] = '';
        $data['link2'] = '';
        $data['link3'] = '';
        if ($req->isXhr()) {
            $page = $req->getQueryParam('page', 1);
            $perPage = $req->getQueryParam('perPage', 10);

            $acciones = [];
            $total = ProyectMapper::model()->rowCount('id');
            $total = $total === false ? -1 : $total;
            $acciones = ProyectMapper::all(false, $page, $perPage);
            foreach ($acciones as $key => $accion) {
                $array_projects = json_decode($acciones[$key]->eje_tematico);
                $projects = "";
                for ($i = 0; $i < sizeof($array_projects); $i++) {

					if ($array_projects[$i] == "Victimas del conflicto armado") {
						$array_projects[$i] = "Víctimas del Conflicto Armado";
                    }
                    if($array_projects[$i]!='0'){

                        $projects .= $array_projects[$i] . ", <br>";
                    }
                    
                    
				}
                $acciones[$key]->eje_tematico = $projects;
            }

            return $res->withJson([
                'data' => $acciones,
                'page' => $page,
                'perPage' => $perPage,
                'pages' => ceil($total / $perPage),
                'total' => $total,
            ]);

        } else {
            import_jquery();
            set_custom_assets([
                base_url('statics/js/global.js'),
                base_url('statics/js/proyects.js'),
            ], 'js');

            set_custom_assets([
                baseurl('statics/css/global.css'),
            ], 'css');

            $data['title'] = "PROYECTOS";
            $data['active_menu_actions'] = "";
            $this->setVariables($data);
            $this->render('layout/header');
            $this->render('layout/menu');
            $this->render("pages/proyectos");
            $this->render('layout/footer');
            return $res;
        }
    }

    /**
     * dettalles proyectos
     *
     * @param Request $req
     * @param Response $res
     * @param array $args
     * @return Response
     * @throws NotFoundException
     */
    public function proyectosDetalles(Request $req, Response $res, array $args)
    {
        $id = $req->getAttribute('id');
        import_jquery();
        set_custom_assets([
            base_url('statics/js/global.js'),
            base_url('statics/js/drawerGlobal.js'),
        ], 'js');

        set_custom_assets([
            baseurl('statics/css/global.css'),
        ], 'css');

        $details = ProyectMapper::getByFriendlyURL($id);
        $data['title'] = "PROYECTOS";
        $data['active_menu_actions'] = "";
        $array_projects = json_decode($details->eje_tematico);
      
        $projects = "";
        for ($i = 0; $i < sizeof($array_projects); $i++) {
            if($array_projects[$i]!='0'){
            $projects .= $array_projects[$i] . ", <br>";
            }
        }
        $details->eje_tematico = $projects;
        

        $data['eje_tematico'] = $details->eje_tematico;
        $data['titulo'] = $details->titulo;
        $data['fecha'] = $details->fecha;
        $data['portada'] = $details->portada;
        $data['aliados'] = $details->aliados;
        $data['ubicacion'] = $details->ubicacion;
        $data['autor'] = $details->autor;
        $data['contenido'] = stripslashes($details->contenido);
        MetaTags::setTitle($details->titulo);
        $acciones = ActionMapper::getActionsByProjects($details->titulo, false, 1, 3);
        $data['accion_preview'] = base64_encode(json_encode($acciones));
        $data['link1'] = '';
        $data['link2'] = '';
        $data['link3'] = '';
        $this->setVariables($data);
        $this->render('layout/header');
        $this->render('layout/menu');
        $this->render("pages/proyectos-detalles");
        $this->render('layout/footer');
        return $res;
    }

    /**
     * ejes temáticos
     *
     * @param Request $req
     * @param Response $res
     * @param array $args
     * @return Response
     * @throws NotFoundException
     */
    public function ejesTematicos(Request $req, Response $res, array $args)
    {
        if ($req->isXhr()) {
            $page = $req->getQueryParam('page', 1);
            $perPage = $req->getQueryParam('perPage', 10);

            $acciones = [];
            $total = EjeMapper::model()->rowCount('id');
            $total = $total === false ? -1 : $total;
            $ejes = EjeMapper::all(false, $page, $perPage);
            $acciones = ActionMapper::all(false, 1, 3);

            return $res->withJson([
                'data' => $ejes,
                'noticias' => $acciones,
                'page' => $page,
                'perPage' => $perPage,
                'pages' => ceil($total / $perPage),
                'total' => $total,
            ]);

        } else {
            import_jquery();
            set_custom_assets([
                base_url('statics/js/global.js'),
                base_url('statics/js/ejes.js'),
            ], 'js');

            set_custom_assets([
                baseurl('statics/css/global.css'),
            ], 'css');

            MetaTags::setTitle('Ejes Temáticos');
            $model = new EjesModel();
            $bringEjes = $model->bringEjes();
            $data['firstView'] = base64_encode(json_encode($bringEjes));
            $data['title'] = "EJES TEMÁTICOS";
            $data['active_menu_actions'] = "";
            $data['link1'] = '';
            $data['link2'] = '';
            $data['link3'] = '';
            $this->setVariables($data);
            $this->render('layout/header');
            $this->render('layout/menu');
            $this->render("pages/ejes-tematicos");
            $this->render('layout/footer');
            return $res;
        }
    }

    /**
     * ejes temáticos
     *
     * @param Request $req
     * @param Response $res
     * @param array $args
     * @return Response
     * @throws NotFoundException
     */
    public function ejesTematicosDetalles(Request $req, Response $res, array $args)
    {
        $id = $req->getAttribute('id');
        import_jquery();
        set_custom_assets([
            base_url('statics/js/global.js'),
            base_url('statics/js/drawerGlobal.js'),
        ], 'js');

        set_custom_assets([
            baseurl('statics/css/global.css'),
        ], 'css');

        $details = EjeMapper::getByFriendlyURL($id);
        MetaTags::setTitle($details->titulo);
        $data['title'] = "EJES TEMÁTICOS";
        $data['active_menu_actions'] = "";
        $data['titulo'] = $details->titulo;
        $data['portada'] = $details->portada;
        $data['contenido'] = stripslashes($details->contenido);
        $acciones = ProyectMapper::getActionsByEje($details->titulo, false, 1, 3);
        $data['accion_preview'] = base64_encode(json_encode($acciones));
        $data['link1'] = '';
        $data['link2'] = '';
        $data['link3'] = '';

        $this->setVariables($data);
        $this->render('layout/header');
        $this->render('layout/menu');
        $this->render("pages/ejes-tematicos-detalles");
        $this->render('layout/footer');
        return $res;
    }

}
