<?php defined("BASEPATH") or die("<h1>El script no puede ser accedido directamente</h1>");?>

<div style="max-width:850px;">

    <h3>Agregar <?=$title;?></h3>

    <div class="ui buttons">
        <a href="<?=$back_link;?>" class="ui button blue"><i class="icon left arrow"></i></a>
    </div>

    <br><br>

    <form action-form method='POST' action="<?=$action;?>" class="ui form">

        <div class="field required">
            <label>Título</label>
            <input type="text" name="title" maxlength="255">
        </div>
        <div class="field">
            <label>Proyectos</label>
            <select class='ui dropdown multiple' multiple name="proyecto[]">
                <?=$options_proyectos;?>
            </select>
        </div>
        <div class="field required">
            <label>Categoría</label>
            <select name="categoria" class="ui dropdown">
                <?=array_to_options(CATEGORIAS)?>
            </select>
        </div>
        <div class="field required" cropper-adapter-component>
            <label>Portada</label>
            <input type="file" name="portada" accept="image/*" required>
            <canvas></canvas>
            <br>
            <button class="ui button orange inverted" cut>Recortar</button>
            <br>
            <div preview></div>
        </div>

        <div class="field required" calendar-js>
            <label>Fecha</label>
            <input autocomplete="off" name="fecha" required>
        </div>

        <div class="field required">
            <label>Descripción</label>
            <div image-process="<?=get_route('piecesphp-built-in-proyects-image-handler')?>" image-name="image"
                rich-editor-js editor-target="[name='content']"></div>
            <textarea name="content" required></textarea>
        </div>
        <div class="field">
            <label>Autor</label>
            <input type="text" name="autor" placeholder="Autor">
        </div>

        <div class="field">
            <button type="submit" class="ui button green">Guardar</button>
        </div>

    </form>
</div>

<script>
window.onload = () => {

    let sublinesDropdown = $(`.ui.dropdown.multiple`).dropdown()

}
</script>