<?php defined("BASEPATH") or die("<h1>El script no puede ser accedido directamente</h1>");
$element;
?>

<div style="max-width:850px;">

    <h3>Editar <?=$title;?></h3>

    <div class="ui buttons">
        <a href="<?=$back_link;?>" class="ui button blue"><i class="icon left arrow"></i></a>
    </div>

    <br><br>

    <form action-form method='POST' action="<?=$action;?>" class="ui form">
        <input type="hidden" name="id" value="<?=$element->id;?>">

        <div class="field required">
            <label>Título</label>
            <input type="text" name="title" maxlength="255" value="<?=$element->titulo?>">
        </div>
        <div class="field required">
            <label>Proyecto</label>
            <select class='ui dropdown multiple' multiple name="proyecto[]"><?=$options_proyects;?></select>
        </div>
        <div class="field required">
            <label>categoria</label>
            <select class='ui dropdown' name="categoria"><?=$options_categoria;?></select>
        </div>


        <div class="field" cropper-adapter-component>
            <label>Portada</label>
            <input type="file" name="portada" accept="image/*">
            <canvas data-image='<?=$element->portada;?>'></canvas>
            <br>
            <button class="ui button orange inverted" cut>Vista previa</button>
            <br>
            <div preview></div>
        </div>

        <div class="field required" calendar-js>
            <label>Fecha</label>
            <input autocomplete="off" name="fecha"
                value="<?=!is_null($element->fecha) ? $element->fecha->format('Y-m-d H:i:s') : '';?>" required>
        </div>

        <div class="field required">
            <label>Descripción</label>
            <div image-process="<?=get_route('piecesphp-built-in-proyects-image-handler')?>" image-name="image"
                rich-editor-js editor-target="[name='content']">
                <?=$element->contenido?>
            </div>
            <textarea name="content" required>
                <?=$element->contenido?>
            </textarea>
        </div>
        <div class="field">
            <label>Autor</label>
            <input type="text" name="autor" placeholder="Autor" value="<?=$element->autor?>">
        </div>

        <div class="field">
            <button type="submit" class="ui button green">Guardar</button>
        </div>

    </form>
</div>

<script>
window.onload = () => {

    let sublinesDropdown = $(`.ui.dropdown.multiple`).dropdown()

}
</script>