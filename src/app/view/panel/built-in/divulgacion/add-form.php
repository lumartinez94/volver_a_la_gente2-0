<?php defined("BASEPATH") or die("<h1>El script no puede ser accedido directamente</h1>");?>

<div style="max-width:850px;">

    <h3><?=  __('divulgationBackend','Agregar') ?> <?=$title;?></h3>
    
    <div class="ui buttons">
        <a href="<?=$back_link;?>" class="ui button blue"><i class="icon left arrow"></i></a>
    </div>

    <br><br>

    <form action-form method='POST' action="<?=$action;?>" class="ui form" quill="<?=$quill_proccesor_link;?>" >

        <div class="ui top attached tabular menu">
            <div class="active item" data-tab="item-1"><?= __('divulgationBackend','Datos de Divulgación') ?></div>
            <div class="item" data-tab="item-2"><?=  __('divulgationBackend','Portada de Divulgación') ?></div>
        </div>

<div class="ui bottom attached tab segment" data-tab="item-2">
        <div class="ui form cropper-adapter" cropper-project>

            <div class="field required">
                <label><?= __('divulgationBackend', 'Portada'); ?></label>
                <input type="file" accept="image/*" name="portada" required>
            </div>

            <?php $this->_render('panel/built-in/utilities/cropper/workspace.php', 
        [
            'referenceW'=> '800',
            'referenceH'=> '600',
        ]); 
?>

        </div>
        <div class="field">
            <button type="submit" class="ui button green"><?=__('divulgationBackend','Guardar')?></button>
        </div>
</div>
<div class="ui bottom attached tab segment active" data-tab="item-1">
<div class="field required">
    <label><?= __('divulgationBackend', 'Título'); ?></label>
    <input type="text" name="title" maxlength="255">
</div>
<div class="field required">
    <label><?= __('divulgationBackend', 'Categoría'); ?></label>
    <select name="category" class="ui dropdown">
        <?= array_to_options(CATEGORIAS_DIVULGACION) ?>
    </select>
</div>


<div class="field required" calendar-js>
    <label><?= __('divulgationBackend', 'Fecha'); ?></label>
    <input autocomplete="off" name="date" required>
</div>

<div class="field required">
                <label><?= __('divulgationBackend', 'Descripción'); ?></label>
                <div quill-editor></div>
                <textarea name="description" required></textarea>
            </div>

<div class="field">
    <label><?= __('divulgationBackend', 'Link del video'); ?></label>
    <input type="text" name="link">
</div>
<div class="field">
    <label> <?= __('divulgationBackend', 'Archivo'); ?> </label>
    <input type="file" class="catch-file" name="archivo">
</div>

</div>
</form>
</div>

<script>
window.onload = () => {

    let sublinesDropdown = $(`.ui.dropdown.multiple`).dropdown()

}
</script>