<?php defined("BASEPATH") or die("<h1>El script no puede ser accedido directamente</h1>");?>

<div style="max-width:850px;">

    <h3><?=__('ejeBackend','Agregar') ?> <?=$title;?></h3>

    <div class="ui buttons">
        <a href="<?=$back_link;?>" class="ui button blue"><i class="icon left arrow"></i></a>
    </div>

    <br><br>

    <form action-form method='POST' action="<?=$action;?>" class="ui form" quill="<?=$quill_proccesor_link;?>">

    <div class="ui top attached tabular menu">
            <div class="active item" data-tab="item-1"><?= __('ejeBackend','Datos del Eje Temático') ?></div>
            <div class="item" data-tab="item-2"><?=  __('ejeBackend','Portada del Eje Temático') ?></div>
        </div>

        <div class="ui bottom attached tab segment active" data-tab="item-1">

        <div class="field required">
            <label><?= __('ejeBackend','Título') ?></label>
            <input type="text" name="titulo" maxlength="255">
        </div>

        <div class="field required">
            <label><?= __('ejeBackend','Pequeña descripción') ?></label>
            <input type="text" name="smallDescription">
        </div>

        <div class="field required">
                <label><?= __('ejeBackend', 'Contenido'); ?></label>
                <div quill-editor></div>
                <textarea name="description" required></textarea>
            </div>


        </div>



        <div class="ui bottom attached tab segment" data-tab="item-2">
            <div class="ui form cropper-adapter" cropper-project>

                <div class="field required">
                    <label><?= __('ejeBackend', 'Portada'); ?></label>
                    <input type="file" accept="image/*" name="portada" required>
                </div>

				<?php $this->_render('panel/built-in/utilities/cropper/workspace.php', 
						[
							'referenceW'=> '800',
							'referenceH'=> '600',
						]); 
				?>

            </div>

            <div class="field">
            <button type="submit" class="ui button green"><?= __('ejeBackend','Guardar') ?></button>
        </div>
           
        </div>

    </form>
</div>

<script>
window.onload = () => {

    let sublinesDropdown = $(`.ui.dropdown.multiple`).dropdown()

    $('.ui.top.attached.tabular.menu .item').tab({
		context: 'parent'
	})

}
</script>