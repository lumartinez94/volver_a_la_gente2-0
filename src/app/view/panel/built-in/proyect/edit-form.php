<?php defined("BASEPATH") or die("<h1>El script no puede ser accedido directamente</h1>");
$element;
?>

<div style="max-width:850px;">

<h3><?= __('projectBackend','Editar')?> <?=$title;?></h3>

    <div class="ui buttons">
        <a href="<?=$back_link;?>" class="ui button blue"><i class="icon left arrow"></i></a>
    </div>

    <br><br>

    <form action-form method='POST' action="<?=$action;?>" class="ui form" quill="<?=$quill_proccesor_link;?>">
        <input type="hidden" name="id" value="<?=$element->id;?>">
        <div class="ui top attached tabular menu">
			<div class="active item" data-tab="item-1"><?= __('projectBackend','Datos del proyecto')?></div>
			<div class="item" data-tab="item-2"><?= __('projectBackend','Portada del proyecto')?></div>
        </div>
        
        <div class="ui bottom attached tab segment" data-tab="item-2">	
    
		
            <div class="ui form cropper-adapter" cropper-project>

                <div class="field">
                    <label><?= __('articlesBackend', 'Portada'); ?></label>
                    <input type="file" name="portada" accept="image/*">
                </div>

                <?php $this->_render('panel/built-in/utilities/cropper/workspace.php', [
											'referenceW'=> '800',
											'referenceH'=> '600',
											'image' =>$element->portada,
											]); ?>

            </div>
        
		<div class="field">
            <button type="submit" class="ui button green"><?= __('projectBackend','Guardar')?></button>
		</div>
		
		
        </div>
        <div class="ui bottom attached tab segment active" data-tab="item-1">

        <div class="field required">
            <label><?=__('projectBackend','Título')?></label>
            <input type="text" name="title" maxlength="255" value="<?=$element->titulo?>">
        </div>
        <div class="field required">
            <label><?=__('projectBackend','Eje temático')?></label>
            <select class='ui dropdown multiple' multiple name="eje[]"><?=$options_ejes;?></select>
        </div>



        

        <div class="field required" >
            <label><?=__('projectBackend','Fecha')?></label>
            <input  name="fecha" value="<?=$element->fecha?>" required>
        </div>

        <div class="two fields">
            <div class="field required">
                <label><?=__('projectBackend','Aliados')?></label>
                <input type="text" name="aliados" placeholder="aliados" value="<?=$element->aliados?>" required>
            </div>
            <div class="field required">
                <label><?=__('projectBackend','Ubicación')?></label>
                <input type="text" name="ubicacion" placeholder="ubicacion" value="<?=$element->ubicacion?>" required>
            </div>
        </div>

        <div class="field required">
                <label><?= __('articlesBackend', 'Descripción'); ?></label>
                <div quill-editor><?=$element->contenido; ?></div>
                <textarea name="description" required><?=$element->contenido; ?></textarea>
            </div>
            
        <div class="field">
            <label><?=__('projectBackend','Autor')?></label>
            <input type="text" name="autor" placeholder="Autor" value="<?=$element->autor?>">
        </div>

        </div>
    </form>
</div>

<script>
window.onload = () => {

    let sublinesDropdown = $(`.ui.dropdown.multiple`).dropdown()

}
</script>