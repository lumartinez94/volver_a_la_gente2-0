<?php defined("BASEPATH") or die("<h1>El script no puede ser accedido directamente</h1>");?>

<div style="max-width:850px;">

    <h3><?= __('projectBackend','Agregar') ?> <?=$title;?></h3>

    <div class="ui buttons">
        <a href="<?=$back_link;?>" class="ui button blue"><i class="icon left arrow"></i></a>
    </div>

    <br><br>

    <form action-form method='POST' action="<?=$action;?>" class="ui form" quill="<?=$quill_proccesor_link;?>" >
    <div class="ui top attached tabular menu">
            <div class="active item" data-tab="item-1"><?= __('projectBackend','Datos del proyecto') ?></div>
            <div class="item" data-tab="item-2"><?=  __('projectBackend','Portada del proyecto') ?></div>
    </div>


    <div class="ui bottom attached tab segment" data-tab="item-2">
            <div class="ui form cropper-adapter" cropper-project>

                <div class="field required">
                    <label><?= __('projectBackend', 'Portada'); ?></label>
                    <input type="file" accept="image/*" name="portada" required>
                </div>

				<?php $this->_render('panel/built-in/utilities/cropper/workspace.php', 
						[
							'referenceW'=> '800',
							'referenceH'=> '600',
						]); 
				?>

            </div>
            <div class="field">
                <button type="submit" class="ui button green"><?=__('projectBackend','Guardar')?></button>
            </div>
        </div>

        <div class="ui bottom attached tab segment active" data-tab="item-1">
        <div class="field required">
        <label><?=__('projectBackend','Título')?></label>
            <input type="text" name="title" maxlength="255" required>
        </div>
        <div class="field required">
            <label><?=__('projectBackend','Eje temático (obligatorio seleccionar uno)')?> </label>
            <select class='ui dropdown multiple' multiple name="eje[]" required><?=$options_ejes;?></select>
        </div>
        

        <div class="field required">
            <label><?=__('projectBackend','Fecha')?></label>
            <input type="number" name="fecha" placeholder="Fecha" required>
        </div>

        <div class="two fields">
            <div class="field required">
                <label><?=__('projectBackend','Aliados')?></label>
                <input type="text" name="aliados" placeholder="aliados" required>
            </div>
            <div class="field required">
                <label><?=__('projectBackend','Ubicación')?></label>
                <input type="text" name="ubicacion" placeholder="ubicacion" required>
            </div>
        </div>

        

        <div class="field required">
                <label><?= __('projectBackend', 'Descripción'); ?></label>
                <div quill-editor></div>
                <textarea name="description" required></textarea>
            </div>

        <div class="field">
            <label><?=__('projectBackend','Autor')?></label>
            <input type="text" name="autor" placeholder="Autor">
        </div>

        

        </div>

    </form>
</div>

<script>
window.onload = () => {

    let sublinesDropdown = $(`.ui.dropdown.multiple`).dropdown()

}
</script>