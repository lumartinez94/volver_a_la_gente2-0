<?php defined("BASEPATH") or die("<h1>El script no puede ser accedido directamente</h1>");
$element;
?>

<div style="max-width:850px;">

    <h3>Editar <?=$title;?></h3>

    <div class="ui buttons">
        <a href="<?=$back_link;?>" class="ui button blue"><i class="icon left arrow"></i></a>
    </div>

    <br><br>

    <form action-form method='POST' action="<?=$action;?>" class="ui form" quill="<?=$quill_proccesor_link;?>">
        <input type="hidden" name="id" value="<?=$element->id;?>">
        <div class="ui top attached tabular menu">
            <div class="active item" data-tab="item-1"><?= __('actionBackend','Datos de la Acción')?></div>
            <div class="item" data-tab="item-2"><?= __('actionBackend','Portada de la Acción')?></div>
        </div>

        <div class="ui bottom attached tab segment" data-tab="item-2">	
    
		
            <div class="ui form cropper-adapter" cropper-project>

                <div class="field">
                    <label><?= __('actionBackend', 'Portada'); ?></label>
                    <input type="file" name="portada" accept="image/*">
                </div>

                <?php $this->_render('panel/built-in/utilities/cropper/workspace.php', [
											'referenceW'=> '800',
											'referenceH'=> '600',
											'image' =>$element->portada,
											]); ?>

            </div>
        
		<div class="field">
            <button type="submit" class="ui button green"><?= __('ejeBackend','Guardar')?></button>
		</div>
		
		
        </div>

        <div class="ui bottom attached tab segment active" data-tab="item-1">
        <div class="field required">
            <label><?= __('actionBackend','Título')?></label>
            <input type="text" name="title" maxlength="255" value="<?=$element->titulo?>">
        </div>
        <div class="field required">
            <label><?= __('actionBackend','Proyectos')?></label>
            <select class='ui dropdown multiple' multiple name="proyecto[]"><?=$options_proyects;?></select>
        </div>
        <div class="field required">
            <label><?= __('actionBackend','Categoría')?></label>
            <select class='ui dropdown' name="categoria"><?=$options_categoria;?></select>
        </div>




        <div class="field required" calendar-js>
            <label><?= __('actionBackend','Fecha')?></label>
            <input autocomplete="off" name="fecha"
                value="<?=!is_null($element->fecha) ? $element->fecha->format('Y-m-d H:i:s') : '';?>" required>
        </div>

        <div class="field required">
                <label><?= __('actionBackend', 'Contenido'); ?></label>
                <div quill-editor><?=$element->contenido; ?></div>
                <textarea name="description" required><?=$element->contenido; ?></textarea>
            </div>

        <div class="field">
            <label><?= __('actionBackend','Autor')?></label>
            <input type="text" name="autor" placeholder="Autor" value="<?=$element->autor?>">
        </div>

        
        </div>
    </form>
</div>

<script>
window.onload = () => {

    let sublinesDropdown = $(`.ui.dropdown.multiple`).dropdown()

}
</script>