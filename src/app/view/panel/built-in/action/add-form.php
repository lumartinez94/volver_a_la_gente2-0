<?php defined("BASEPATH") or die("<h1>El script no puede ser accedido directamente</h1>");?>

<div style="max-width:850px;">

    <h3><?= __('actionBackend','Agregar')?> <?=$title;?></h3>

    <div class="ui buttons">
        <a href="<?=$back_link;?>" class="ui button blue"><i class="icon left arrow"></i></a>
    </div>

    <br><br>

    <form action-form method='POST' action="<?=$action;?>" class="ui form" quill="<?=$quill_proccesor_link;?>">


        <div class="ui top attached tabular menu">
            <div class="active item" data-tab="item-1"><?= __('actionBackend','Datos de la Acción')?></div>
            <div class="item" data-tab="item-2"><?= __('actionBackend','Portada de la Acción')?></div>
        </div>

        <div class="ui bottom attached tab segment" data-tab="item-2">


            <div class="ui form cropper-adapter" cropper-project>

                <div class="field">
                    <label><?= __('actionBackend', 'Portada'); ?></label>
                    <input type="file" name="portada" accept="image/*">
                </div>

                <?php $this->_render('panel/built-in/utilities/cropper/workspace.php', [
											'referenceW'=> '800',
											'referenceH'=> '600',
											]); ?>

            </div>

            <div class="field">
                <button type="submit" class="ui button green"><?= __('actionBackend','Guardar')?></button>
            </div>


        </div>

        <div class="ui bottom attached tab segment active" data-tab="item-1">
            <div class="field required">
                <label><?= __('actionBackend','Título')?></label>
                <input type="text" name="title" maxlength="255">
            </div>
            <div class="field">
                <label><?= __('actionBackend','Proyectos')?></label>
                <select class='ui dropdown multiple' multiple name="proyecto[]">
                    <?=$options_proyectos;?>
                </select>
            </div>
            <div class="field required">
                <label><?= __('actionBackend','Categoría')?></label>
                <select name="categoria" class="ui dropdown">
                    <?=array_to_options(CATEGORIAS)?>
                </select>
            </div>


            <div class="field required" calendar-js>
                <label><?= __('actionBackend','Fecha')?></label>
                <input autocomplete="off" name="fecha" required>
            </div>

            <div class="field required">
                <label><?= __('actionBackend', 'Contenido'); ?></label>
                <div quill-editor></div>
                <textarea name="description" required></textarea>
            </div>

            <div class="field">
                <label><?= __('actionBackend','Autor')?></label>
                <input type="text" name="autor" placeholder="Autor">
            </div>


        </div>
    </form>
</div>

<script>
window.onload = () => {

    let sublinesDropdown = $(`.ui.dropdown.multiple`).dropdown()

}
</script>