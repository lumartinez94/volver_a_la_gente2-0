<?php defined("BASEPATH") or die("<h1>El script no puede ser accedido directamente</h1>");?>
<div class="create-proyect-container" style="max-width:1200px">
    <form class="ui form" action="<?= get_route('save-proyect') ?>" id="save-proyect">
        <h4 class="ui dividing header">Título</h4>
        <div class="field required">
            <label>Eje temático</label>
            <input type="text" name="eje-tematico" placeholder="Eje tematico" required>
        </div>
        <div class="field required">
            <label>Título</label>
            <input type="text" name="titulo" placeholder="Título" required>
        </div>
        <div class="field required">
            <label>Fecha</label>
            <input type="text" name="fecha" placeholder="Fecha" required>
        </div>
        <div class="two fields">
            <div class="field required">
                <label>Aliados</label>
                <input type="text" name="aliados" placeholder="aliados" required>
            </div>
            <div class="field required">
                <label>Ubicación</label>
                <input type="text" name="ubicacion" placeholder="ubicacion" required>
            </div>
        </div>
        <h4 class="ui dividing header">Contenido</h4>
        <div id="inputs-container" class="field">
            <div id="p-1">
                <div class="field">
                    <label>Sub titulo</label>
                    <input type="text" name="subtitulo" placeholder="Subtitulo">
                </div>
                <div class="field">
                    <label>contenido</label>
                    <textarea name="contenido" id="contenido-p" cols="30" rows="10"></textarea>
                </div>

                <div class="column field cropper-content-1">
                    <?php $id = uniqid();?>
                    <input id='<?=$id?>' type="file" name='<?=$id?>' accept="image/jpg, image/jpeg, image/png">
                    <label for="<?=$id?>" class="ui small right floated button fluid  botoninput">
                        <i class="ui upload icon"></i>
                        <?='Imagen'?>
                    </label>
                    <br><br>
                    <div class='canvas-container'>
                        <canvas></canvas>
                        <br>
                        <div style="text-align:center">
                            <a class="ui button recortar">Recortar</a>
                        </div>
                        <textarea id="imagen-1"></textarea>
                        <div class="preview"></div>
                    </div>
                </div>
            </div>



            <div class="field" id="btn-container">
                <a class="ui primary button add-p">
                    Agregar otro parrafo
                </a>
            </div>
        </div>
        <div class="field">
            <label>Autor</label>
            <input type="text" name="autor" placeholder="Autor">
        </div>
        <button class="ui green button save-proyect-button " tabindex="0">
            <i class="save outline icon"></i>
            Guardar proyecto
        </button>
    </form>
</div>

<script type='text/html' paragraph-template>
<div class="field">
    <label>Sub titulo</label>
    <input type="text" name="subtitulo" placeholder="Subtitulo">
</div>
<div class="field">
    <label>contenido</label>
    <textarea name="contenido" id="contenido-p" cols="30" rows="10"></textarea>
</div>


<div class="column field cropper-content-{{NUMERADOR}}">
    <input id='imagen-{{NUMERADOR}}' name='imagen-{{NUMERADOR}}' type="file" accept="image/jpg, image/jpeg, image/png" style="display:none">
    <label for="imagen-{{NUMERADOR}}" class="ui small right floated button fluid  botoninput">
        <i class="ui upload icon"></i>
        Imagen
    </label>
    <br><br>
    <div class='canvas-container'>
        <canvas>
        </canvas>
        <br>
        <div style="text-align:center">
            <a class="ui button recortar">Recortar</a>
        </div>
        <textarea id="imagen-{{NUMERADOR}}" style="display:none;"></textarea>
        <div class="preview"></div>
    </div>
</div>
</script>