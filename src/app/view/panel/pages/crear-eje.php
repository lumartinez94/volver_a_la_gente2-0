<?php defined("BASEPATH") or die("<h1>El script no puede ser accedido directamente</h1>");?>
<div class="create-proyect-container" style="max-width:1200px">
    <form class="ui form" action="<?= get_route('save-eje') ?>" id="save-eje">
        <h4 class="ui dividing header">Título</h4>
        <div class="field required">
            <label>Título</label>
            <input type="text" name="titulo" placeholder="Título" required>
        </div>
        <div class="column field cropper-content-1">
            <label>Imagen de portada</label>
            <?php $id = uniqid();?>
            <input id='<?=$id?>' type="file" name='<?=$id?>' accept="image/jpg, image/jpeg, image/png">
            <label for="<?=$id?>" class="ui small right floated button fluid  botoninput">
                <i class="ui upload icon"></i>
                <?='Imagen'?>
            </label>
            <br><br>
            <div class='canvas-container'>
                <canvas></canvas>
                <br>
                <div style="text-align:center">
                    <a class="ui button recortar">Recortar</a>
                </div>
                <textarea id="imagen-portada"></textarea>
                <div class="preview"></div>
            </div>
        </div>
        <h4 class="ui dividing header">Parrafo</h4>
        <div id="inputs-container" class="field">
            <div id="p-1">
                <div class="field">
                    <label>Sub titulo</label>
                    <input type="text" name="subtitulo" placeholder="Subtitulo">
                </div>
                <div class="field">
                    <label>Parrafo</label>
                    <textarea name="contenido" id="contenido-p" cols="30" rows="10"></textarea>
                </div>


            </div>



            <div class="field" id="btn-container">
                <a class="ui primary button add-p">
                    Agregar otro parrafo
                </a>
            </div>
        </div>
        <button class="ui green button save-eje-button " tabindex="0">
            <i class="save outline icon"></i>
            Guardar Eje
        </button>
    </form>
</div>

<script type='text/html' paragraph-template>
<div class="field">
    <label>Sub titulo</label>
    <input type="text" name="subtitulo" placeholder="Subtitulo">
</div>
<div class="field">
    <label>contenido</label>
    <textarea name="contenido" id="contenido-p" cols="30" rows="10"></textarea>
</div>
</script>