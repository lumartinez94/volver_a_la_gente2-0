<?php defined("BASEPATH") or die("<h1>El script no puede ser accedido directamente</h1>");?>

<div class="contacts">
    <div class="img-contacts">
        <img src="<?=base_url('statics/images/icons/logo.png')?>" alt="">
    </div>
    <div class="contact-container">
        <div class="contact-item">
            <h3 class="title">Bogotá D.C.</h3>
            <p class="text">
                Carrera 26 # 41 - 20 <br>
                Barrio La Soledad <br>
                <div class="phones">
                    <div>
                        <img class="tel" src="<?=base_url('statics/images/icons/telefono.svg')?>" alt="">
                    </div>
                    <div>
                        PBX 57(1) 269 4913
                    </div>
                </div>
            </p>
        </div>
        <div class="contact-item">
            <h3 class="title">Medellín</h3>
            <p class="text">
                Casa Zea, Calle 51 # 54 - 71 <br>
                Barrio San Benito <br>
                <div class="phones">
                    <div>
                        <img class="tel" src="<?=base_url('statics/images/icons/telefono.svg')?>" alt="">
                    </div>
                    <div>
                        PBX 57(1) 269 4913
                    </div>
                </div>
            </p>
        </div>
        <div class="contact-item">
            <h3 class="title">Pereira</h3>
            <p class="text">
                Av. Las Américas # 44 - 51 <br>
                Zona Sur<br>
                <div class="phones">
                    <div>
                        <img class="tel" src="<?=base_url('statics/images/icons/telefono.svg')?>" alt="">
                    </div>
                    <div>
                        PBX 57(1) 269 4913
                    </div>
                </div>
            </p>
        </div>
       
        <div class="contact-item">
            <h3 class="title">Barranquilla</h3>
            <p class="text">
                Carrera 58 # 66 – 81. 204 <br>
                Barrio El Prado <br>
                <div class="phones">
                    <div>
                        <img class="tel" src="<?=base_url('statics/images/icons/telefono.svg')?>" alt="">
                    </div>
                    <div>
                        PBX 57(1) 269 4913
                    </div>
                </div>

            </p>
        </div>

    </div>
</div>

<?php load_js(['base_url' => "", 'custom_url' => ""])?>

</body>

</html>
