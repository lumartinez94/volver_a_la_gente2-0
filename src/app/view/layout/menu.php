<?php defined("BASEPATH") or die("<h1>El script no puede ser accedido directamente</h1>");?>
<div class="menu">
    <div class="menu-items-container">
        <div class="menu-icon">
            <a href="<?=get_route('public-home')?>">
                <img src=" <?=base_url('/statics/images/icons/logo.svg')?> ">

            </a>
        </div>
        <div class="menu-items">
            <div class="menu-links">
                <ul class="link-container only-small ">
                    <a>
                        <li class="link-active-sm">
                            <img src="<?=base_url('statics/images/icons/menu.svg')?>">
                        </li>
                    </a>

                    <div class="sub-menu-sm">
                        <ul>
                            <a href="<?=get_route('public-home')?>" class="sub-sm-link" id="inicio">
                                <li>INICIO
                                </li>
                            </a>
                        </ul>
                        <ul>
                            <a class="sub-sm-link" id="us">
                                <li> SOBRE NOSOTROS
                                </li>
                            </a>
                            <div class="sub-menu-content-sm us-sm">
                                <a href=" <?=get_route('public-nosotros')?> ">
                                    <li>
                                        NUESTRO FUNDADOR
                                    </li>
                                </a>
                                <!--<a href="<?=get_route('public-experiencias')?>">
                                    <li>
                                        EXPERIENCIAS
                                    </li>
                                </a>-->
                                <a href="<?=get_route('public-historia')?>">
                                    <li>
                                        LA CORPORACIÓN
                                    </li>
                                </a>
                                <a href="<?=get_route('public-indicadores')?>">
                                    <li>
                                        INDICADORES
                                    </li>
                                </a>
                            </div>
                        </ul>
                        <ul>
                            <a class="sub-sm-link" href=" <?=get_route('public-ejes')?> " id="ejes">
                                <li>EJES TEMÁTICOS
                                </li>
                            </a>
                        </ul>
                        <ul>
                            <a href="<?=get_route('public-proyectos')?>" class="sub-sm-link" id="proyectos">
                                <li>PROYECTOS
                                </li>
                            </a>
                        </ul>
                        <ul>
                            <a class="sub-sm-link" id="acciones">
                                <li>NOTICIAS
                                </li>
                            </a>
                            <div class="sub-menu-content-sm acciones-sm">
                                <a href="<?=get_route('public-acciones')?>">
                                    <li>
                                        TODAS
                                    </li>
                                </a>
                                <a class="menu-button" id="acciones">
                                    <li>
                                        NOTICIAS
                                    </li>
                                </a>
                                <a class="menu-button" id="articulos">
                                    <li>
                                        ARTICULOS
                                    </li>
                                </a>

                                <a class="menu-button" id="eventos">
                                    <li>
                                        EVENTOS
                                    </li>
                                </a>
                            </div>
                        </ul>
                        <ul>
                            <a class="sub-sm-link" id="divulgacion">
                                <li>DIVULGACIÓN
                                </li>
                            </a>
                            <div class="sub-menu-content-sm divulgacion-sm">
                                <a href="<?=get_route('public-divulgacion')?>">
                                    <li>
                                        TODAS
                                    </li>
                                </a>
                                <a class="menu-button" id="piezas">
                                    <li>
                                        PIEZAS GRÁFICAS
                                    </li>
                                </a>
                                <a class="menu-button" id="videos">
                                    <li>
                                        VIDEOS
                                    </li>
                                </a>

                                <a class="menu-button" id="publicaciones">
                                    <li>
                                        PUBLICACIONES
                                    </li>
                                </a>
                            </div>
                        </ul>
                        <ul>
                            <a href="<?=get_route('public-agencias')?>" class="sub-sm-link" id="agencias">
                                <li>AGENCIA DE EMPLEO
                                </li>
                            </a>
                        </ul>
                        <ul>
                            <a class="sub-sm-link" id="contact-trigger">
                                <li>CONTACTO
                                </li>
                            </a>
                            <div class="sub-menu-content-sm contacto-sm">
                                <a>
                                    <li>
                                        <div class="image">
                                            <img class="phone"
                                                src="<?=base_url('statics/images/icons/contacto-telefono.svg')?>">
                                        </div>
                                        <div class="text">
                                            <span> 319 535 9372</span>
                                        </div>
                                    </li>
                                </a>
                                <a>
                                    <li>
                                        <div class="image">
                                            <img src="<?=base_url('statics/images/icons/contacto-mail.svg')?>">
                                        </div>
                                        <div class="text">
                                            <span>volver@gmail.com</span>
                                        </div>
                                    </li>
                                </a>
                                <a>
                                    <li>
                                        <div class="image">
                                            <img src="<?=base_url('statics/images/icons/contacto-instagram.svg')?>">
                                        </div>
                                        <div class="text">
                                            <span> @volvera_lagente</span>
                                        </div>
                                    </li>
                                </a>
                            </div>
                        </ul>
                    </div>
                </ul>
                <ul class="link-container ">
                    <a href="<?=get_route('public-home')?>" class="menu-button" id="inicio">
                        <li class="link-active">

                            INICIO

                        </li>
                    </a>
                </ul>
                <ul class="link-container">
                    <a class="menu-button" id="nosotros">
                        <li class="link-active">
                            SOBRE NOSOTROS
                        </li>
                    </a>

                    <div class="sub-menu">
                        <a href="<?=get_route('public-nosotros')?>">
                            <li>
                                NUESTRO FUNDADOR
                            </li>
                        </a>
                        <a href="<?=get_route('public-historia')?>">
                            <li>
                                LA CORPORACIÓN
                            </li>
                        </a>
                        <!--<a href="<?=get_route('public-experiencias')?>">
                            <li>
                                EXPERIENCIAS
                            </li>
                        </a>-->
                        <a href="<?=get_route('public-indicadores')?>">
                            <li>
                                INDICADORES
                            </li>
                        </a>
                    </div>

                </ul>
                <ul class=" link-container">
                    <a href=" <?=get_route('public-ejes')?> " class="menu-button" id="ejes">
                        <li class="link-active">
                            EJES TEMÁTICOS
                        </li>
                    </a>

                </ul>
                <ul class="link-container">
                    <a href="<?=get_route('public-proyectos')?>" id="proyectos" class="menu-button">
                        <li class="link-active">
                            PROYECTOS
                        </li>
                    </a>

                </ul>
                <ul class="link-container">
                    <a id="divulgacion" class="menu-button">
                        <li class="link-active">
                            DIVULGACIÓN
                        </li>
                    </a>
                    <div class="sub-menu">
                        <a href="<?=get_route('public-divulgacion')?>">
                            <li>
                                PUBLICACIONES Y VIDEOS
                            </li>
                        </a>
                        <a id="acciones" href="<?=get_route('public-acciones')?>">
                            <li>
                                NOTICIAS
                            </li>
                        </a>
                    </div>

                </ul>
                <ul class="link-container">
                    <a href="<?= get_route("public-agencias") ?>" id="agencias" class="menu-button">
                        <li class="link-active">
                            AGENCIA DE EMPLEO
                        </li>
                    </a>
                </ul>
                <ul class="link-container">
                    <a id="contact-trigger" class="menu-button">
                        <li class="link-active">
                            CONTACTO
                        </li>
                    </a>
                </ul>

            </div>
        </div>

    </div>
    <?php if (!isset($isHome)): ?>
    <div class="menu-title">
        <p class="title">
            <?=$title?>
        </p>
    </div>
    <?php endif;?>
    <div class="actions-menu <?=$active_menu_actions?>">
        <div class="actions-links">
            <ul class="link-container ">
                <a class="menu-button">
                    <li class="link-active">
                        TODAS
                    </li>
                </a>
            </ul>
            <ul class="link-container ">
                <a class="menu-button" id="<?=explode(" ", strtolower($link1))[0]?>">
                    <li class="link-active">
                        <?=$link1?>
                    </li>
                </a>
            </ul>
            <ul class="link-container ">
                <a class="menu-button" id="<?=explode(" ", strtolower($link2))[0]?>">
                    <li class="link-active">
                        <?=$link2?>

                    </li>
                </a>
            </ul>

            <ul class="link-container ">
                <a class="menu-button" id="<?=explode(" ", strtolower($link3))[0]?>">
                    <li class="link-active">
                        <?=$link3?>
                    </li>
                </a>
            </ul>

            <ul class="link-container ">
                <li class="link-active">
                    <div class="buscador">
                        <div>
                            <input type="text" class="input-search" placeholder="Buscar">
                        </div>
                        <div class="lupa-container">
                            <a class="search-action">
                                <img src="<?=base_url('statics/images/icons/lupa.svg')?>">
                            </a>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="floating-contact">
    <div>
        <div class="first-paragraph">
            <p>
                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam
                nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam
                erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci
                tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo
                consequat. Duis autem vel eum iriure dolor in hendrerit in .
            </p>
        </div>
        <div class="contact-container">
            <div class="item-container">
                <div class="img">
                    <img class="phone" src="<?=base_url('statics/images/icons/contacto-telefono.svg')?>">
                </div>
                <div class="text">
                    <div>

                        <h2>TÉLEFONO</h2>
                        <p><a href="tel:+57(1) 269 4913">PBX 57(1) 269 4913</a> </p>
                    </div>
                </div>
            </div>
            <div class="item-container">
                <div class="img">
                    <img src="<?=base_url('statics/images/icons/contacto-mail.svg')?>">
                </div>
                <div class="text">
                    <div>
                        <h2>CORREOS</h2>
                        <p><a href="mailto:info@volveralagente.org">info@volveralagente.org</a></p>
                        <p>
                            <a href="mailto:agenciaempleo@volveralagente.org">agenciaempleo@volveralagente.org</a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="item-container">
                <div class="img">
                    <img src="<?=base_url('statics/images/icons/facebook.svg')?>" style="width:40px">
                </div>
                <div class="text">
                    <div>
                        <h2>FACEBOOK</h2>
                        <p>
                            <a
                                href="https://es-la.facebook.com/pages/category/Community-Organization/Corporacion-Volver-a-la-Gente-108825972292/" target="_blank">
                                Corporación Vover a la Gente
                            </a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
