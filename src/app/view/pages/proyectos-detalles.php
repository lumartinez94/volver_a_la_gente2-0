<?php defined("BASEPATH") or die("<h1>El script no puede ser accedido directamente</h1>");?>
<div class="proyectos-detalles-container">
    <div class="title-container">
        <div class="text-container">
            <div>
                <p>
                    <?=$fecha?>
                </p>
                <h1>
                    <?=$titulo?>
                </h1>
                <span>
                    <?= $eje_tematico ?>
                </span>

            </div>

        </div>
        <div class="image-container">

            <div class="draw-image">
                <img class="circles-back" src="<?=base_url('statics/images/circulos.svg')?>">

                <img class="image-circle" src="<?=base_url($portada)?>">


            </div>
        </div>


        <div class="bar">
            <div class="bar-content">
                <div class="img">
                    <img src="<?=base_url('statics/images/icons/manos-aliados.svg')?>" alt="">
                </div>
                <div class="text">
                    <div>
                        <h3>ALIADOS</h3>
                        <p>
                            <?=$aliados?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="bar-content">
                <div class="img ">
                    <img class="ubicacion" src="<?=base_url('statics/images/icons/ubicacion-aliados.svg')?>" alt="">
                </div>
                <div class="text">
                    <div>
                        <h3>UBICACIÓN</h3>
                        <p>
                            <?=$ubicacion?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="detalle-content">
        <?=$contenido?>
    </div>


</div>
<div class="mas-acciones">
    <h1>
        ACCIONES RELACIONADAS
    </h1>
    <p class="actions-from-bd" style="display:none;">
        <?=$accion_preview?>
    </p>
    <div class="acciones-container">

    </div>



    <a href="<?=get_route('public-proyectos')?>">
        <div class="show-more-btn">
            VER MÁS <br> PROYECTOS +
        </div>
    </a>

</div>
<script src="text/html" actions-cards-template>
    <a href = "<?=get_route('public-acciones-detalle', ['id' => '{{ID}}'])?>" >
        <div class = "accion-container" style = "background:url(<?= base_url("{{PORTADA}}") ?>) " >
        <h2 class = "acciones-title" > {{TITLE}} </h2>
        <div class="courtain"></div>
         </div>
    </a>
</script>