<?php defined("BASEPATH") or die("<h1>El script no puede ser accedido directamente</h1>");?>
<div class="nosotros-container">
    <div class="hernando">
        <div class="only-small-text">
            <p>
                HERNANDO
                JIMÉNEZ
                PARDO
            </p>
        </div>
        <div class="img-container">
            <img src="<?=base_url('statics/images/creador.png')?>">

        </div>
        <div class="text-container">
            <div>
                <h1 class="title">
                    HERNANDO
                    JIMÉNEZ <br>
                    PARDO
				</h1>
				<h3>LA DIRECCIÓN</h3>
                <p class="text">
                    Históricamente las Direcciones que Volver a la Gente ha tenido, han logrado la virtud de atender y
                    caracterizar adecuadamente cada una de las Etapas en las que su desarrollo institucional ha
                    requerido en relación con el país. Esta ha sido paralelamente una labor colectiva y de construcción
                    con aportes mutuos, divergencias, consensos y disensos pero que nos ha permitido jugar limpio y
                    jugar bien en el terreno social con eficiencia, efectividad e impacto.
                    <br><br>
                    Ahora bien, en un ejercicio de análisis prospectivo, para enmarcar “los momentos o estrategias en
                    cada Etapa” me permito una breve y apretadísima descripción con algunos ejemplos de proyectos (es
                    imposible enumerarlos todos para esta síntesis)
                </p>
            </div>
        </div>
    </div>
    <div class="strategic-development">
        <h2>
            Desarrollos Estratégicos (Etapas) de la Corporación Volver a la Gente, 1993 - 2017
        </h2>
        <h3>
            PRIMERA ETAPA
        </h3>
        <p>
            Consistió en favorecer y desarrollar los mecanismos de democracia directa, participación ciudadana y
            derechos humanos consagrados en la Constitución Nacional de 1991, para lo cual la Corporación Volver a la
            Gente inició actividades a partir de diversos programas de pedagogía social. Oficialmente tienen vida
            jurídica a partir del 14 de julio de 1993 mediante la personería jurídica emanada del Instituto Colombiano
            de Bienestar Familiar mediante resolución No. 930. En este sentido se asumieron las actuaciones por parte de
            Volver a la Gente.
        </p>
        <p>
            Algunos de los proyectos desarrollados en este período con los anteriores propósitos fueron:
        </p>
        <p>
            Programa de Formación para el Fortalecimiento de la Participación Ciudadana de los Cabildos Verdes,
            Organizaciones Ambientalistas y Ecologistas en el Tránsito Inderena-Minambiente. Realizado en ocho meses en
            cinco regiones del país en 130 municipios.
        </p>
        <p>
            Programa de Formación y Participación Ciudadana para los Derechos de Niños, Niñas y Adolescentes en las
            Políticas Públicas de las recién onstituidas localidades en la ciudad de Bogotá D.C. Se realizó durante un
            año con el Centro Regional del ICBF y la Unicef en 14 localidades con cubrimiento de 300 Hogares de
            Bienestar (madres comunitarias y padres de familia)
        </p>
        <p>
            Programa de Derechos Humanos y Participación Ciudadana en cinco (5) municipios de Cundinamarca durante 7
            meses con la Gobernación de Cundinamarca y las respectivas alcaldías municipales y organizaciones sociales y
            de base.
        </p>
        <p>
            Proyecto de Fortalecimiento y Consolidación Ciudadana en el Ejercicio de la Recuperación Ambiental y
            Ecológica para los derechos en el territorio. Realizado con Min. Ambiente en las ciudades de Sogamoso,
            Bogotá D.C. y Cartagena constituyéndose las Aulas Ambientales Ciudadanas.
        </p>
        <p>
            Proyecto de Formación Ciudadana para constitución de Semilleros de Convivencia de acuerdo con el Plan de
            Desarrollo de Antanas Mockus, participando en el diseño, elaboración y puesta en marcha del programa de
            cultura ciudadana e implementándolo en 34 barrios de Bogotá D.C en la Localidad de Teusaquillo.
        </p>
        <p>
            Proyecto de formación ciudadana con jóvenes en centros educativos hacia la convivencia y el ejercicio de sus
            derechos en la ciudad de Cartagena en cooperación con Unicef en dos localidades de la ciudad.
        </p>
        <h3>
            SEGUNDA ETAPA
        </h3>
        <p>
            De acuerdo con las circunstancias en el país, Volver a la Gente asumió avanzar en acciones y proyectos que
            articulados a su Visión para el Cambio se vincularan a los propósitos de atención a la población desplazada
            y desarraigada por el conflicto armado. De acuerdo con lo anterior se asumió la necesidad de dar un salto
            cualitativo y cuantitativo de articulación con la cooperación internacional y el Gobierno Nacional a quien
            le correspondía asumir las responsabilidades necesarias de acuerdo con los dictámenes de la Corte
            Constitucional.
        </p>
        <p>
            En el primer aspecto se lograron exitosas y perdurables relaciones con organismos y entidades
            internacionales mediante la obtención de fondos logrados a partir de presentar propuestas en las
            convocatorias respectivas. Algunas de las relaciones efectivas con cooperantes fueron: USAID, Unión Europea,
            Irish Aid, BPRM, Unicef, PNUD, Checchi Co, MSD, Fupad y Mercy Corps, asociatividades como las realizadas con
            Fundación Social y Minuto de Dios. Esas alianzas permitieron la implementación de proyectos en la Costa
            Atlántica (Cartagena y Barranquilla) y Bogotá D.C. durante seis años en que se ejecutaron en relación con la
            Etapa.
        </p>
        <p>
            En el caso del Gobierno Nacional se adelantaron gestiones y obtuvieron apoyos presupuestales para ser
            articulados con los Fondos de Cooperación Internacional. Se realizaron los convenios y acuerdos con:
            Presidencia de la República. Acción Social, Minjusticia, Minsalud, Gobernaciones y Alcaldías en las diversas
            ciudades donde actuaba Volver a la Gente. Se tuvieron como eje las sedes en las ciudades ya mencionadas
            durante todo el periodo de años
        </p>
        <h3>
            TERCERA ETAPA
        </h3>
        <p>
            Es el que se ha vivido por parte de Volver a la Gente hasta el 2017. Reconociendo nuevamente la dinámica del
            país y la necesidad de responder a la misma y sin dejar de lado los propósitos esenciales de la Corporación,
            se inició el periodo en donde se caracterizó la importancia de vincular el sector empresarial a los
            propósitos de inclusión social, restablecimiento y estabilización de la población víctima del conflicto
            armado, generación de ingresos, equidad y justicia.
        </p>
        <p>
            Se inició la relación con diversos sectores empresariales en la Costa Atlántica, Bogotá y Medellín con
            agremiaciones como la ANDI, Fenalco, ACOPI, entre otros. El propósito fue que bajo una visión de RSE se
            vincularan, adelantando modalidades de apoyo a emprendimientos vinculados a la producción empresarial,
            fortalecimiento económico asociativo y la colocación de empleo. En este periodo de siete (7) años se
            mantuvieron los proyectos dirigidos a la generación de ingresos y empleo, las acciones de formación,
            organización y consolidación de las organizaciones sociales en sectores urbanos populares, a población
            víctima del conflicto armado.
        </p>
        <p>
            Este periodo también está caracterizado por el diseño, y puesta en marcha del Modelo de Atención Psicosocial
            propio de Volver a la Gente que ha significado un reconocimiento en el ámbito institucional nacional e
            internacional. En particular y aunque ha sido una constante en todos los proyectos, la mujer ha sido eje de
            las acciones. Un proyecto demostrativo realizado con 1.300 mujeres durante dos años, articuló los
            psicosocial a la formación política y social de las mujeres. Este proyecto fue realizado en Bogotá D.C. con
            la Alcaldía Local de Rafael Uribe Uribe, generando intercambios de lecciones aprendidas con organizaciones
            en Medellín y el Oriente Antioqueño.
        </p>
        <p>
            Como resultado de la caracterización efectuada, se articuló de forma efectiva en estos años y al unísono:
            Gobierno Nacional y Territoriales, Cooperación Internacional, Empresarios y Población Participante. Como un
            producto directo de estas experiencias surge la Agencia de Empleo Volver a la Gente que desarrolla la
            gestión y colocación de empleo para víctimas del conflicto armado, constituyéndose su Modelo de Atención en
            un ejemplo de éxito en el panorama nacional, gubernamental, internacional y de satisfacción de las víctimas.
            Se constituyen en este periodo las sedes de Medellín, Apartado, Quibdó, y Pereira de Volver a la Gente
            asentando presencia institucional y ganando reconocimientos de socios estratégicos. Se refrenda como casa
            matriz Bogotá, D.C.
        </p>
        <p>
            Entendiendo el período que se abre a partir de los Acuerdos de la Habana, se propone al ICBF Nacional un
            Modelo de trabajo para el reconocimiento, promoción y restablecimiento de derechos de niños, niñas y
            adolescentes en el cual son partícipes directos de las acciones en concertación con autoridades municipales,
            empresarios, familias y entornos protectores. Se ha realizado durante dos años en Urabá, Chocó y Occidente
            de Antioquia con una cobertura de 1.800 niños, niñas y adolescentes y una amplia producción cultural y de
            medios de comunicación.
        </p>
        <h3>
            CUARTA ETAPA
        </h3>
        <p>
            Es la que se inicia en el 2018 y seguramente estará marcando la próxima década. Esta Etapa está definida por
            el postconflicto, la construcción de paz territorial con desarrollo productivo, cultural y sociopolítico
            incluyente. En este sentido Volver a la Gente debe ser capaz de continuar con sus líneas de acción, pero
            articulándose activa y estratégicamente al horizonte estratégico.
        </p>
        <p>
            ¿Y cuál es este horizonte? Si somos coherentes con la construcción territorial de paz y convivencia, este
            horizonte marcado por acciones precisas de desarrollo enmarcadas en la economía productiva, la presencia de
            nuevos sectores empresariales, impulso a las vías (que ya se hace) pero se requieren políticas sociales y
            económicas ante las nuevas potencialidades evidentes. En este desarrollo territorial es evidente más que
            nunca que se necesita un enfoque, propuestas de articulación ciudadana, conciente y proactiva respecto a los
            temas e infraestructura y ambientales que les puede beneficiar un desarrollo estratégico si su participación
            es activa y consciente, lejos de discursos manipuladores de todos lados. Significa que Volver a la Gente
            tiene un amplio escenario para promocionar y realizar proyectos en donde la concertación y la pedagogía
            social sean el eje para la RSE, la política gubernamental y la cooperación internacional. Todo lo anterior
            bajo la directriz mundial de Naciones Unidas qué impulsa en nuestro país (como en todos) el cumplimiento de
            los ODS (Objetivos de Desarrollo Sostenible) que es el paraguas bajo el cual se suscriben todos los
            convenios, acuerdos y relaciones bilaterales / multilaterales de actores sociales como empresarios,
            entidades, organismos internacionales y Gobierno Nacional en este siglo XXI. Para nuestro caso se
            materializa en el contexto del Acuerdo de Paz con énfasis en las víctimas.
        </p>
        <p>
            En estos parámetros tiene plena vigencia la Visión, así como la Misión y los Valores de Volver a la Gente.
            En consecuencia, se requiere de una Dirección para esta Etapa que la entienda y la comprenda, para nuevas y
            más potentes gestiones, articulando las valiosas experiencias de Volver a la Gente a la misma. Que tenga las
            capacidades de interlocutar con altos niveles institucionales, de cooperación internacional y sectores
            empresariales, para canalizar acciones y recursos hacia y con la población a partir del valioso aporte que
            la experiencia de los asociados(as) a Volver a la Gente tienen y han construido. Ese respeto debe estar por
            encima de todo.
        </p>
    </div>
    <div class="testimonios">
        <div class="title">
            <h2>TESTIMONIOS</h2>
        </div>
        <div class="content">
            <div class="text-container">
                <div class="text-content">
                    <h2>“LOREM IPSUM”</h2>
                    <h4>
                        <span>Por:</span> &nbsp;
                        Jorge De la Hoz Serrano
                    </h4>
                    <p>
                        Corporación Volver a la Gente se proyecta
                        como una organización que contribuirá a la
                        construcción de Paz y la Reconciliación a
                        través de la consolidación de una sociedad
                        justa e incluyente en donde se eliminen las
                        barreras y discriminaciones de todo tipo, con
                    </p>
                    <div class="circle-buttons">
                        <a class="show-button" id="group1"></a>
                        <a class="show-button" id="group2"></a>
                        <a class="show-button" id="group3"></a>
                    </div>
                </div>
            </div>
            <div class="image-container">
                <img src="<?=base_url('/statics/images/morena.jpg')?>">
            </div>
        </div>
    </div>

    <div class="galeria">
        <div class="title">
            <h2>
                GALERÍA DE IMÁGENES
            </h2>
        </div>
        <div class="images-container">
            <div class="first">

                <img class="img1" src="<?=base_url('/statics/images/first.jpg')?>">
                <img src="<?=base_url('/statics/images/second.jpg')?>" alt="">
            </div>
            <div class="second">
                <img class="third" src="<?=base_url('/statics/images/third.jpg')?>" alt="">
                <div>

                    <img src="<?=base_url('/statics/images/fourth.jpg')?>" alt="">
                    <img src="<?=base_url('/statics/images/five.jpg')?>" alt="">
                    <img src="<?=base_url('/statics/images/fourth.jpg')?>" alt="">
                    <img src="<?=base_url('/statics/images/five.jpg')?>" alt="">
                </div>
            </div>

        </div>
    </div>
</div>
