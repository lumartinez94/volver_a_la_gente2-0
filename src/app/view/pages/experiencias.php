<?php defined("BASEPATH") or die("<h1>El script no puede ser accedido directamente</h1>");?>
<div class="experiencias-container">
    <div class="first-paragraph">
        <p>
            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam
            nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam
            erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci
            tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo
            consequat. Duis autem vel eum iriure dolor in hendrerit in .
        </p>
    </div>
    <div class="experiences-year">
        <div class="year">
            <div class="purple-circle">
                <span>
                    2018
                </span>
            </div>
            <div class="fucsia-circle"></div>
        </div>
        <div class="year-container">
            <div class="experience-container">
                <div class="experience">
                    <div class="experience-img">
                        <div class="line">
                            <div class="point"></div>
                        </div>
                    </div>
                    <div class="experience-text">
                        <h3>
                            BONO DE IMPACTO SOCIAL
                            COLOMBIA –BIS ADICIÓN
                        </h3>
                        <p>
                            Fundación Corona-Departamento Administrativo para la
                            Prosperidad Social- Banco Interamericano de Desarrollo
                        </p>
                    </div>

                </div>
            </div>
            <div class="experience-container exp2-container">
                <div class="experience">
                    <div class="experience-img">
                        <div class="line">
                            <div class="point"></div>
                        </div>
                    </div>
                    <div class="experience-text">
                        <h3>
                            INCLUSIÓN LABORAL A VÍCTIMAS
                            DEL CONFLICTO ARMADO EN
                            BOGOTÁ CUNDINAMARCA
                        </h3>
                        <p>
                            Fundación TEXMODAS
                        </p>
                    </div>

                </div>
            </div>
            <div class="experience-container ">
                <div class="experience">
                    <div class="experience-img">
                        <div class="line">
                            <div class="point"></div>
                        </div>
                    </div>
                    <div class="experience-text">
                        <h3>
                            INCLUSIÓN LABORAL A VÍCTIMAS
                            DEL CONFLICTO ARMADO EN
                            BOGOTÁ CUNDINAMARCA
                        </h3>
                        <p>
                            Fundación TEXMODAS
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="experiences-year">
        <div class="year">
            <div class="bar"></div>
            <div class="purple-circle">
                <span>
                    2017
                </span>
            </div>
            <div class="fucsia-circle"></div>
        </div>
        <div class="year-container">
            <div class="experience-container">
                <div class="experience">
                    <div class="experience-img">
                        <div class="line">
                            <div class="point"></div>
                        </div>
                    </div>
                    <div class="experience-text">
                        <h3>
                            BONO DE IMPACTO SOCIAL
                            COLOMBIA –BIS ADICIÓN
                        </h3>
                        <p>
                            Fundación Corona-Departamento Administrativo para la
                            Prosperidad Social- Banco Interamericano de Desarrollo
                        </p>
                    </div>
                </div>
            </div>
            <div class="experience-container exp2-container">
                <div class="experience">
                    <div class="experience-img">
                        <div class="line">
                            <div class="point"></div>
                        </div>
                    </div>
                    <div class="experience-text">
                        <h3>
                            INCLUSIÓN LABORAL A VÍCTIMAS
                            DEL CONFLICTO ARMADO EN
                            BOGOTÁ CUNDINAMARCA
                        </h3>
                        <p>
                            Fundación TEXMODAS
                        </p>
                    </div>

                </div>
            </div>
            <div class="experience-container ">
                <div class="experience">
                    <div class="experience-img">
                        <div class="line">
                            <div class="point"></div>
                        </div>
                    </div>
                    <div class="experience-text">
                        <h3>
                            INCLUSIÓN LABORAL A VÍCTIMAS
                            DEL CONFLICTO ARMADO EN
                            BOGOTÁ CUNDINAMARCA
                        </h3>
                        <p>
                            Fundación TEXMODAS
                        </p>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="experiences-year">
        <div class="year">
            <div class="bar"></div>
            <div class="purple-circle">
                <span>
                    2016
                </span>
            </div>
            <div class="fucsia-circle"></div>
        </div>
        <div class="year-container">
            <div class="experience-container">
                <div class="experience">
                <div class="experience-img">
                        <div class="line">
                        <div class="point"></div>
                        </div>
                    </div>
                    <div class="experience-text">
                        <h3>
                            BONO DE IMPACTO SOCIAL
                            COLOMBIA –BIS ADICIÓN
                        </h3>
                        <p>
                            Fundación Corona-Departamento Administrativo para la
                            Prosperidad Social- Banco Interamericano de Desarrollo
                        </p>
                    </div>
                </div>
            </div>
            <div class="experience-container exp2-container">
                <div class="experience">
                <div class="experience-img">
                        <div class="line">
                        <div class="point"></div>
                        </div>
                    </div>
                    <div class="experience-text">
                        <h3>
                            INCLUSIÓN LABORAL A VÍCTIMAS
                            DEL CONFLICTO ARMADO EN
                            BOGOTÁ CUNDINAMARCA
                        </h3>
                        <p>
                            Fundación TEXMODAS
                        </p>
                    </div>
                </div>
            </div>
            <div class="experience-container ">
                <div class="experience">
                <div class="experience-img">
                        <div class="line">
                        <div class="point"></div>
                        </div>
                    </div>
                    <div class="experience-text">
                        <h3>
                            INCLUSIÓN LABORAL A VÍCTIMAS
                            DEL CONFLICTO ARMADO EN
                            BOGOTÁ CUNDINAMARCA
                        </h3>
                        <p>
                            Fundación TEXMODAS
                        </p>    
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>