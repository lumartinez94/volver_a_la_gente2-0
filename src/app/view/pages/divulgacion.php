<?php defined("BASEPATH") or die("<h1>El script no puede ser accedido directamente</h1>");?>
<div class="divulgacion-container">
    <div class="cards-container" route="<?= get_route('public-divulgacion') ?>">


    </div>
    <a class="show-more">
        <div>
            MOSTRAR MÁS +
        </div>
    </a>
    <div class="ui modal modal-details">

    </div>
</div>
<script type="text/html" first-view-divulgation-template>
<div class="card" data-id='{{ID}}'>
    <div class="card-head">
        <img src="<?= base_url('{{COVER}}') ?>">
        <div class="play">
            <img src="<?= base_url('statics/images/icons/play.svg') ?>">
        </div>
    </div>
    <div class="card-body">
        <span class="card-date">
            {{DATE}}
        </span>
        <h1 class="card-title">
            {{TITLE}}
        </h1>

    </div>
    <div class="card-buttons">
        <a class="btn-more">
            <div>
                CARGAR <br> MÁS
                <img src="<?=base_url('statics/images/icons/flecha.svg')?>">
            </div>
        </a>
    </div>
</div>
</script>


<script type="text/html" advice-template>
<h3 class="advice">No existen {{TITLE}} disponibles</h3>
</script>


<script type="text/html" modal-template>
<i class="close icon close-trigger"></i>
<div class="card-modal-head">
    <img src="<?= base_url('{{COVER}}') ?>">
</div>
<div class="card-modal-body" data-id="{{ID}}" >
    <div class="play">
        <a href="{{VIDEO}}" target="_blank">
            <img src="<?= base_url('statics/images/icons/play.svg') ?>" alt="">
        </a>
    </div>
    <span class="card-modal-date">
        {{DATE}}
    </span>
    <h1 class="card-modal-title">
        {{TITLE}}
    </h1>
    <div class="card-modal-description">
        {{DESCRIPTION}}
    </div>
    <div class="out-btn">
        <a href="<?= base_url('{{ARCHIVO}}') ?>" target="_blank">
            <img src="<?= base_url('statics/images/icons/download.svg') ?>">
        </a>
    </div>

</div>
</script>