<?php defined("BASEPATH") or die("<h1>El script no puede ser accedido directamente</h1>");?>
<div class="ejes-container">
    <div class="ejes-div" route="<?=get_route('public-ejes')?>"></div>
    <a class="show-more">
        <div>
            MOSTRAR MÁS +
        </div>
    </a>
    <div class="noticias-container">
        <div class="noticias">
            
        </div>

        <a href="<?=get_route('public-acciones')?>">
            <div class="show-more-btn">
                VER MÁS <br> NOTICIAS +
            </div>
        </a>

    </div>
</div>

<script type="text/html" first-view-ejes-template>
<div class="conciliacion" style="background-color:white;">
    <div class="content-container">

        <div class="conciliacion-text">
            <div>
                <h1 class="title">
                    {{TITULO}}
                </h1>

                <p class="text">
                    {{CONTENIDO}}
                </p>

                <a class="btn-more" href="<?=get_route('public-ejes-detalle', ['id' => '{{ID}}'])?>">
                    <div>
                        CONOCE <br> MÁS
                        <img src="<?=base_url('statics/images/icons/flecha.svg')?>">
                    </div>
                </a>
            </div>

        </div>
        <div class="conciliacion-img">
            <div class="fucsia-circle">
            <img class="img-inside" src="<?=base_url('{{PORTADA}}')?>">

            </div>
        </div>
    </div>

</div>
</script>

<script type="text/html" second-view-ejes-template>
<div class="conciliacion" style="background-color:white;">
    <div class="content-container">
        <div class="conciliacion-img">
            <div class="fucsia-circle left">
            <img class="img-inside img-left" src="<?=base_url('{{PORTADA}}')?>">

            </div>
        </div>
        <div class="conciliacion-text">
            <div>
                <h1 class="title">
                    {{TITULO}}
                </h1>
                <!--<span class="memoria">La conciliación con equidad.</span>-->

                <p class="text">
                    {{CONTENIDO}}
                </p>

                <a class="btn-more" href="<?=get_route('public-ejes-detalle', ['id' => '{{ID}}'])?>">
                    <div>
                        CONOCE <br> MÁS
                        <img src="<?=base_url('statics/images/icons/flecha.svg')?>">
                    </div>
                </a>
            </div>

        </div>

    </div>

</div>
</script>

<script type="text/html" notice-template>
<a href="<?= get_route('public-acciones-detalle',['id'=>'{{ID}}']) ?>">
    <div class="notice-container" style="background-image: 
    linear-gradient(
      rgba(0, 0, 0, 0.7),
      rgba(0, 0, 0, 0.7)
    ),
    url({{PORTADA}})"; >
        <h2 class="notice-title">
            {{TITLE}}
        </h2>
        <span class="notice-date">
            {{FECHA}}
        </span>
    </div>
</a>
</script>