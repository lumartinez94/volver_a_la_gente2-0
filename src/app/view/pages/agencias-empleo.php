<?php defined("BASEPATH") or die("<h1>El script no puede ser accedido directamente</h1>");?>
<div class="historia-container">
    <div class="title-container">
        <div class="image-container">

            <div class="draw-image">

                <div class="purple-circle"></div>
                <div class="fucsia-circle"></div>
                <div class="image-circle"></div>
            </div>
        </div>

        <div class="text-container">
            <h1>
                AGENCIA DE <br> EMPLEO
            </h1>
        </div>
    </div>
    <div class="historia-content">
        <p>
            En el año 2014 con el fin de fortalecer la gestión de procesos de inclusión productiva propios de su
            misionalidad, la Corporación decide crear una Agencia de Empleo reconocida por el Ministerio de Trabajo bajo
            la Resolución 00598 de 27 de agosto de 2014. Cuenta con experiencia en la aplicación de un Modelo de Gestión
            de Acciones Integradas para la capacitación, formación y vinculación laboral de población víctima del
            conflicto armado y en condición de vulnerabilidad. A su vez, aúna esfuerzos para el fortalecimiento y
            consolidación de alianzas con sectores empresariales y la presencia activa de autoridades territoriales, con
            el propósito de fomentar el diálogo social y la planeación prospectiva territorial en la construcción de paz
            en Colombia.
        </p>
        <p>
            La Agencia cuenta con un equipo interdisciplinario experto en procesos de gestión de empleo, selección,
            colocación, orientación ocupacional, acompañamiento psicosocial y preparación para el empleo. Tiene una
            oficina principal en Bogotá y ha desarrollado proyectos de empleabilidad en los departamentos de
            Cundinamarca, Eje Cafetero: Caldas, Risaralda, Quindío, Antioquia y subregión Urabá, Norte de Santander,
            Región Caribe: Magdalena, Atlántico, Bolívar, entre otras regiones del país.
        </p>

    </div>
    <div class="footer-agencias-empleo">
        <div class="text">
            <p>
                Vinculado a la red de prestadores del Servicio Público de Empleo.
                Autorizado por la Unidad Administrativa Especial del Servicio Público de Empleo
                según resolución N. 000598 del 27 de Agosto de 2014.
            </p>
        </div>
        <div class="images">
		<a href="https://serviciodeempleo.gov.co/"><img src="<?=base_url("statics/images/servicio-empleo.jpg")?>" alt="servicio de empleo"></a>
		<img class="min-trabajo-img" src="<?=base_url("statics/images/Mintrabajo_Colombia.svg")?>" alt="ministerio de trabajo">
        </div>
    </div>
</div>
