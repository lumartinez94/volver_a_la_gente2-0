<?php defined("BASEPATH") or die("<h1>El script no puede ser accedido directamente</h1>");?>
<?php defined("BASEPATH") or die("<h1>El script no puede ser accedido directamente</h1>");?>
<div class="ui cards">
    <div class="card">
        <div class="image">
            <img src="<?=base_url('statics/images/cards/points.jpg')?>">
        </div>
        <div class="content">
            <div class="header">Ejes temáticos</div>
        </div>
        <div class="extra content">
            <div class="ui two buttons">
                <a href="<?=get_route('built-in-ejes-list', [], true)?>" class="ui blue button">Listar</a>
                <a href=" <?=get_route('built-in-ejes-forms-add', [], true)?> "
                    class="ui green button">Agregar</a>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="image">
            <img src="<?=base_url('statics/images/cards/points.jpg')?>">
        </div>
        <div class="content">
            <div class="header">Proyectos</div>
        </div>
        <div class="extra content">
            <div class="ui two buttons">
                <a href="<?=get_route('built-in-proyects-list', [], true)?>" class="ui blue button">Listar</a>
                <a href=" <?=get_route('built-in-proyects-forms-add', [], true)?> "
                    class="ui green button">Agregar</a>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="image">
            <img src="<?=base_url('statics/images/cards/points.jpg')?>">
        </div>
        <div class="content">
            <div class="header">Acciones</div>
        </div>
        <div class="extra content">
            <div class="ui two buttons">
                <a href="<?=get_route('built-in-actions-list', [], true)?>" class="ui blue button">Listar</a>
                <a href=" <?=get_route('built-in-actions-forms-add', [], true)?> "
                    class="ui green button">Agregar</a>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="image">
            <img src="<?=base_url('statics/images/cards/points.jpg')?>">
        </div>
        <div class="content">
            <div class="header">Divulgaciones</div>
        </div>
        <div class="extra content">
            <div class="ui two buttons">
                <a href="<?=get_route('built-in-divulgaciones-list', [], true)?>"
                    class="ui blue button">Listar</a>
                <a href=" <?=get_route('built-in-divulgaciones-forms-add', [], true)?> "
                    class="ui green button">Agregar</a>
            </div>
        </div>
    </div>

</div>