<?php defined("BASEPATH") or die("<h1>El script no puede ser accedido directamente</h1>");?>
<div class="actions-container-page">
    <div class="articles-container" route="<?=get_route('public-acciones')?>">

    </div>
    <a class="show-more">
        <div>
            MOSTRAR MÁS +
        </div>
    </a>
</div>
<script type="text/html" first-view-actions-template>
<div class="{{STYLE}} ">
    <div class="action">
        <div class="action-img">

            <div class="image-circle">
                <img src="<?=base_url('{{PORTADA}}')?>" alt="portada">

            </div>
        </div>
        <div class="action-text">
            <span class="category">
                {{CATEGORIA}}
            </span>
            <h3>
                {{TITULO}}
            </h3>
            <span>
                {{FECHA}}
            </span>
            <a class="btn-more" href="<?=get_route('public-acciones-detalle', ['id' => '{{ID}}'])?>">
                <div>
                    CONOCE <br> MÁS
                    <img src="<?=base_url('statics/images/icons/flecha.svg')?>">
                </div>
            </a>
        </div>

    </div>
</div>
</script>
<script type="text/html" advice-template>
<h3 class="advice">No existen {{TITULO}} disponibles</h3>
</script>