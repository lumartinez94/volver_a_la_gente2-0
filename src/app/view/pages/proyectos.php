<?php defined("BASEPATH") or die("<h1>El script no puede ser accedido directamente</h1>");?>
<div class="projects-container-page">

    <div class="articles-container" route="<?=get_route('public-proyectos')?>">


    </div>
    <a class="show-more">
        <div>
            MOSTRAR MÁS +
        </div>
    </a>

</div>




<script type="text/html" first-view-proyects-template>
<div class="{{STYLE}} ">
    <div class="action">
        <div class="action-img">
            <div class="image-circle" >
                <img src="<?=base_url('{{PORTADA}}')?>" alt="portada">
            </div>
        </div>
        <div class="action-text">
            <p>
                {{EJETEMATICO}}
            </p>
            <h3>
                {{TITULO}}
            </h3>
            <a class="btn-more" href="<?=get_route('public-proyectos-detalle', ['id' => '{{ID}}'])?>">
                <div>
                    CONOCE <br> MÁS
                    <img src="<?=base_url('statics/images/icons/flecha.svg')?>">
                </div>
            </a>
        </div>

    </div>
</div>
</script>
<script type="text/html" advice-template>
<h3 class="advice">No existen {{TITULO}} disponibles</h3>
</script>