<?php defined("BASEPATH") or die("<h1>El script no puede ser accedido directamente</h1>");?>
<div class="home-container">
    <div class="home">
        <div class="home-text">
            <p class="text">
			Construimos paz fortaleciendo capacidades a través del acompañamiento psicosocial, la inclusión productiva, la igualdad de género y  <br>
                <span class="text-2"> el desarrollo sostenible.</span>
            </p>
        </div>
        <div class="home-img">
            <div class="circle">
            </div>
            <img class="image" src="<?=base_url('/statics/images/trabajador.png')?>">
            <div class="circle-text">

                <p>
                    <span class="number">180</span> <br>
                    PERSONAS <br> EMPLEADAS <br>
                    <span class="calidad">
                        Mejoraron su calidad de vida.
                    </span>
                </p>
                <a class="btn-more" href="<?=get_route('public-indicadores')?>">
                    <div>
                        CONOCE <br> MÁS
                        <img src="<?=base_url('statics/images/icons/flecha.svg')?>">
                    </div>
                </a>
            </div>

        </div>
    </div>
    <div class="conciliacion">
        <div class="content-container">

            <div class="conciliacion-text">
                <div>
                    <span class="memoria">
                        <?=$ejes->titulo?>
                    </span>
                    <h1 class="title">
                        <?=strlen($ejes->parrafo_portada) > 150 ? substr($ejes->parrafo_portada, 0, 150) . '...' : $ejes->parrafo_portada?>
                        <a href="<?=get_route('public-ejes-detalle', ['id' => $ejes->friendly_url])?>" class="btn-more">
                            <div>
                                CONOCE <br> MÁS
                                <img src="<?=base_url('statics/images/icons/flecha.svg')?>">
                            </div>
                        </a>
                    </h1>

                </div>

            </div>
            <div class="conciliacion-img">
                <div class="fucsia-img">
                    <img src="<?=base_url($ejes->portada)?>">
                </div>
            </div>
        </div>
        <div class="btn-container">
            <a href="<?=get_route('public-ejes')?>" class="btn-actions">
                VER TODOS LOS EJES TEMÁTICOS
                <img src="<?=base_url('statics/images/icons/flechaFucsia.svg')?>">
            </a>
        </div>

    </div>

    <div class="fundador">
        <div class="only-small-text">
            <h2>NUESTRO FUNDADOR</h2>
            <p>
                HERNANDO
                JIMÉNEZ
                PARDO
            </p>
        </div>
        <div class="img-container">
            <p class="img-text">
                NUESTRO FUNDADOR
            </p>
            <img src="<?=base_url('statics/images/creador.png')?>">
            <h1 class="title">
                HERNANDO
                JIMÉNEZ
                PARDO
            </h1>
        </div>
        <div class="text-container">
            <a class="btn-more" href="<?=get_route('public-nosotros')?>">
                CONOCE <br> MÁS
                <span>+</span>
            </a>
        </div>
    </div>
    <p class="actions-from-bd" style="display:none;">
        <?=$accion_preview?>
    </p>
    <div class="noticias-container" route="<?=get_route('public-home')?>">

        <div class="noticias">


        </div>
        <a href="<?=get_route('public-acciones')?>">
            <div class="show-more-btn">
                VER MÁS <br> ACCIONES +
            </div>
        </a>
    </div>


    <div class="aliados">
        <h1 class="title">
            ALIADOS
        </h1>
        <div class="aliados-container">
            <div class="aliado-img">
                <img src="<?=base_url('statics/images/atlantico.jpg')?>" alt="atlantico">
            </div>
            <div class="aliado-img">
                <img src="<?=base_url('statics/images/bprm.jpg')?>" alt="bprm">
            </div>
            <div class="aliado-img">
                <img src="<?=base_url('statics/images/checchi.jpg')?>" alt="checchi">
            </div>
            <div class="aliado-img">
                <img src="<?=base_url('statics/images/dps.jpg')?>" alt="dps">
            </div>
            <div class="aliado-img">
                <img src="<?=base_url('statics/images/fundacion-corona.jpg')?>" alt="fundacion-corona">
            </div>
            <div class="aliado-img">
                <img src="<?=base_url('statics/images/icbf.jpg')?>" alt="icbf">
            </div>
            <div class="aliado-img">
                <img src="<?=base_url('statics/images/irish.jpg')?>" alt="irish">
            </div>
            <div class="aliado-img">
                <img src="<?=base_url('statics/images/mercy.jpg')?>" alt="mercy">
            </div>
            <div class="aliado-img">
                <img src="<?=base_url('statics/images/servicio-empleo.jpg')?>" alt="servicio-empleo">
            </div>
            <div class="aliado-img">
                <img src="<?=base_url('statics/images/text-moda.jpg')?>" alt="text-moda">
            </div>
            <div class="aliado-img">
                <img src="<?=base_url('statics/images/un-habitat.jpg')?>" alt="un-habitat">
            </div>
            <div class="aliado-img">
                <img class="img-ue" src="<?=base_url('statics/images/UE.jpg')?>" alt="union-europea">
            </div>
            <div class="aliado-img">
                <img src="<?=base_url('statics/images/usaid.jpg')?>" alt="usaid">
			</div>
			<div class="aliado-img">
                <img  src="<?=base_url('statics/images/pnud.png')?>" alt="pnud">
			</div>
			<div class="aliado-img">
                <img src="<?=base_url('statics/images/unicef.png')?>" alt="unicef">
			</div>
			<div class="aliado-img">
                <img src="<?=base_url('statics/images/bogota_humana.jpg')?>" alt="bogota humana">
            </div>
            <div class="aliado-img">
                <img src="<?=base_url('statics/images/Mintrabajo_Colombia.svg')?>" alt="min trabajo">
            </div>
        </div>

    </div>
</div>
<script type="text/html" notice-template>
<a href="<?=get_route('public-acciones-detalle', ['id' => '{{ID}}'])?>">
    <div class="notice-container" style="background-image:
    linear-gradient(
      rgba(0, 0, 0, 0.7),
      rgba(0, 0, 0, 0.7)
    ),
    url({{PORTADA}})" ;>
        <h2 class="notice-title">
            {{TITLE}}
        </h2>
        <span class="notice-date">
            {{FECHA}}
        </span>
    </div>
</a>
</script>

<script type="text/html" accion-preview-template>
<div>
    <span class="memoria">
        {{PROYECTOS}}
    </span>
    <h1 class="title">
        {{TITULO}}
        <a href="<?=get_route('public-acciones-detalle', ['id' => '{{ID}}'])?>" class="btn-more">
            <div>
                CONOCE <br> MÁS
                <img src="<?=base_url('statics/images/icons/flecha.svg')?>">
            </div>
        </a>
    </h1>
</div>
</script>
