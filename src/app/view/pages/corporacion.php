<?php defined("BASEPATH") or die("<h1>El script no puede ser accedido directamente</h1>");?>
<div class="historia-container">
    <div class="title-container">
        <div class="image-container">

            <div class="draw-image">

                <div class="purple-circle"></div>
                <div class="fucsia-circle"></div>
                <div class="image-circle"></div>
            </div>
        </div>

        <div class="text-container">
            <h1>
                LA <br> CORPORACIÓN.
            </h1>
        </div>
    </div>
    <div class="historia-content">
        <p>
            La Corporación Volver a la Gente es una entidad sin ánimo de lucro, con una amplia y exitosa experiencia de
            26 años en el desarrollo de programas y modelos de acompañamiento con personas excluidas por la pobreza y
            con víctimas del conflicto armado, cuyo objetivo es la construcción de paz y la reconciliación en los
            territorios a través del fortalecimiento de capacidades locales, el acompañamiento psicosocial, el
            desarrollo sostenible, el acceso al trabajo digno y la igualdad de género.
        </p>
        <p>
            Por ello hemos implementado durante los últimos 10 años proyectos con enfoque de género y enfoque
            diferencial, construyendo metodologías y procesos que combinen la atención psicosocial, la generación de
            ingresos, la empleabilidad, la resolución pacífica de conflictos, el hábitat y la cooperación del sector
            empresarial, como herramientas claves para el posconflicto.
        </p>
        <p>
            Nuestro enfoque consiste en generar capacidades y nuevos conocimientos para fortalecer el capital social. En
            la actual coyuntura del país, en donde es urgente construir la Agenda de Paz y Posconflicto, implementamos
            Ejercicios de Planeación Prospectiva Territorial en los que los participantes y sus organizaciones, el
            Estado, el sector privado y las comunidades establecen un diálogo social incluyente, de forma novedosa para
            consolidar la etapa de construcción de la paz con la perspectiva más importante que debe tener: la
            reconciliación nacional.
        </p>
        <p>
            Para Volver a la Gente, la gestión y unión de esfuerzos con el sector empresarial es parte fundamental de la
            construcción de paz; por ello generamos alianzas para nuestras iniciativas de paz, en las cuales las
            empresas son actores de cambio que aportan a una sociedad reconciliada y productiva ofertando empleo y
            oportunidades.
        </p>
        <p>
            Gestionamos continuamente recursos a nivel nacional e internacional para implementar procesos conducentes a
            la construcción de Paz en los territorios y la Reconciliación del país.
        </p>
        <br>
        <br>
        <h2>
            Misión
        </h2>
        <p>
            Consolidar la construcción de la paz y la reconciliación mediante la promoción del respeto a los Derechos
            Humanos, la convivencia, la resolución pacífica de los conflictos y la participación ciudadana como ejes
            fundamentales para el desarrollo social, económico y cultural.
        </p>
        <p>
            Nuestros proyectos se enfocan en generar capacidades y nuevos conocimientos para fortalecer el capital
            social, fomentar el desarrollo sostenible, la participación ciudadana, la igualdad de género y la inclusión
            productiva. Gestionamos continuamente recursos a nivel nacional e internacional
            para implementar procesos pedagógicos y sociales que conduzcan a la convivencia en el país.
        </p>
        <p>
            Para los anteriores propósitos se han desarrollado en los últimos quince (15) años proyectos y/o programas
            en las siguientes áreas básicas:
        </p>
        <ul>
            <li>
                Rutas de atención y creación del Modelo de Acciones Integradas para el trabajo con Víctimas del
                Conflicto Armado.
            </li>
            <li>
                Acompañamiento Psicosocial
            </li>
            <li>
                Memoria Histórica
            </li>
            <li>
                Derechos Humanos; Justicia de Paz, Mediación y Conciliación en Equidad
            </li>
            <li>
                Fortalecimiento Institucional (descentralización, gobernabilidad local, políticas públicas)
            </li>
            <li>
                Educación, Cultura y Participación Ciudadana
            </li>
            <li>La Comunicación para el Desarrollo</li>
            <li>La Planeación del Desarrollo con Enfoque de Género (Construcción de nuevas masculinidades e igualdad de
                género)</li>
            <li>La Gestión Ambiental Ciudadana</li>

		</ul>
		<br>
        <br>
        <h2>
		Visión para el cambio
		</h2>
		<p>
		Corporación Volver a la Gente se proyecta como una organización que contribuirá a la construcción de Paz y la Reconciliación a través de la consolidación de una sociedad justa e incluyente en donde se eliminen las barreras y discriminaciones de todo tipo, con producción académica en los temas que desarrolla y un equipo de trabajo comprometido con la realidad del país y el mejoramiento de las condiciones de los seres que lo habitan. Fortalecerá además su capacidad de incidir en los lineamientos y el diseño de políticas públicas, enfocando sus acciones a través de la promoción, protección y defensa de los derechos humanos desde la igualdad de género y el desarrollo a escala humana.
		</p>
		<br>
        <br>
        <h2>
		Nuestros equipos
		</h2>
		<p>
		La Corporación Volver a la Gente tiene cuatro equipos en el país: Centro con sede en Bogotá, Costa Atlántica con sede Barranquilla, Antioquia con sede en Medellín y Norte de Santander con sede en Cúcuta. Está conformada por profesionales de diversas disciplinas, con amplia experiencia y trayectoria en los aspectos anteriormente mencionados.
		</p>
		<p>
		Ha desarrollado diversos proyectos en los departamentos de Cundinamarca, Chocó, Cauca, Eje Cafetero: Caldas, Risaralda, Quindío, Antioquia y subregión Urabá, Norte de Santander, Región Caribe: Magdalena, Atlántico, Bolívar, entre otras regiones del país.
		</p>
		<p>
		Nuestros proyectos se caracterizan por ser realizados a través de Procesos de Educación - Comunicación - Investigación, de manera que quienes participan constituyen modelos pedagógicos, en los cuales se fortalece reflexivamente el sentido de pertenencia, la identidad y la autoestima individual y colectiva, consolidando procesos organizativos y de gestión. 
		</p>
		<p>
		De igual manera, en los últimos 12 años, nuestro énfasis ha sido gestionar iniciativas para el apoyo a la población víctima del conflicto armado, tanto en el tema de Atención Humanitaria de Emergencia como en la etapa de restablecimiento y estabilización, con el fin de apoyar el goce efectivo de derechos y la construcción de nuevos proyectos de vida, a través del acompañamiento psicosocial, emprendimientos y empleabilidad. Para esto hemos contado con el apoyo de entidades nacionales e internacionales.
		</p>
		<p>
		De igual forma, hemos realizado la gestión y sensibilización empresarial hacia la obtención y colocación de empleos, apoyo a emprendimientos e involucramiento en las ideas de proyecto que gestionamos, con el objeto de incentivar en los empresarios de la región y el país hacia la responsabilidad social empresarial como oportunidad para la construcción de paz.
		</p>
		<br>
        <br>
        <h2>
		Socios, aliados y donantes
		</h2>
		<p>
		Hemos recibido donaciones y financiación de organismos internacionales como: Comisión Europea para Colombia, ONU Hábitat, Bureau of Population, Refugees and Migration –BPRM, Irish Aid, Checchi, USAID, Mercy Corps, CHF Internacional, entre otros.  A nivel nacional hemos contado con la contratación de alcaldias municipales y locales, gobernaciones, ministerios y entidades como el Departamento de Prosperidad Social, Gobernación del Atlántico, Alcaldía Mayor de Bogotá, Insitituto Colombiano de Bienestar Familiar, entre otras.
		</p>
    </div>
</div>
