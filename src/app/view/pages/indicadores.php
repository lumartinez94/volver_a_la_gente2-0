<?php defined("BASEPATH") or die("<h1>El script no puede ser accedido directamente</h1>");?>
<div class="indicadores-container">
    <div class="first-paragraph">
        <p>
            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam
            nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam
            erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci
            tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo
            consequat. Duis autem vel eum iriure dolor in hendrerit in .
        </p>
    </div>

    <div class="snake-container">
        <div class="container-circle-1">
            <div class="circle">
                <h2>180</h2>
                <p>
                    <span>
                        PERSONAS <br> EMPLEADAS
                    </span> <br>
                    mejoraron su calidad <br> de vida.
                </p>
            </div>
            <div class="conector">

            </div>
        </div>

        <div class="container-circle-2">
            <div class="circle-purple">
                <h2>180</h2>
                <p>
                    <span>
                        PERSONAS <br> EMPLEADAS
                    </span> <br>
                    mejoraron su calidad <br> de vida.
                </p>
            </div>
            <div class="conector">

            </div>
        </div>

        <div class="container-circle-3">
            <div class="circle-purple-words">
                <h2>180</h2>
                <p>
                    <span>
                        PERSONAS <br> EMPLEADAS
                    </span> <br>
                    mejoraron su calidad <br> de vida.
                </p>
            </div>
            <div class="conector">

            </div>
        </div>

        <div class="container-circle-4">
            <div class="circle-purple">
                <h2>180</h2>
                <p>
                    <span>
                        PERSONAS <br> EMPLEADAS
                    </span> <br>
                    mejoraron su calidad <br> de vida.
                </p>
            </div>
            <div class="conector">

            </div>
        </div>

        <div class="container-circle-5">
            <div class="circle-purple-words">
                <h2>180</h2>
                <p>
                    <span>
                        PERSONAS <br> EMPLEADAS
                    </span> <br>
                    mejoraron su calidad <br> de vida.
                </p>
            </div>
            <div class="conector">

            </div>


            <div class="conector-2">

            </div>
        </div>

        <div class="container-circle-6">
            <div class="circle-fucsia">
                <h2>180</h2>
                <p>
                    <span>
                        PERSONAS <br> EMPLEADAS
                    </span> <br>
                    mejoraron su calidad <br> de vida.
                </p>
            </div>
            <div class="conector">

            </div>

        </div>

        <div class="container-circle-7">
            <div class="circle-purple-words">
                <h2>180</h2>
                <p>
                    <span>
                        PERSONAS <br> EMPLEADAS
                    </span> <br>
                    mejoraron su calidad <br> de vida.
                </p>
            </div>
            <div class="conector">

            </div>
        </div>

        <div class="container-circle-8">
            <div class="circle-purple">
                <h2>180</h2>
                <p>
                    <span>
                        PERSONAS <br> EMPLEADAS
                    </span> <br>
                    mejoraron su calidad <br> de vida.
                </p>
            </div>
        </div>
    </div>
</div>