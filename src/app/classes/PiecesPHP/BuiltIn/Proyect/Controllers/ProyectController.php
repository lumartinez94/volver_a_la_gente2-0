<?php
/**
 * ArticleController.php
 */

namespace PiecesPHP\BuiltIn\Proyect\Controllers;

use App\Controller\AdminPanelController;
use App\Model\UsersModel;
use PiecesPHP\BuiltIn\Eje\Mappers\EjeMapper;
use PiecesPHP\BuiltIn\Proyect\Mappers\ProyectMapper;
use PiecesPHP\Core\Helpers\Directories\DirectoryObject;
use PiecesPHP\Core\Forms\FileUpload;
use PiecesPHP\Core\Forms\FileValidator;
use PiecesPHP\Core\HTML\HtmlElement;
use PiecesPHP\Core\Roles;
use PiecesPHP\Core\Route;
use PiecesPHP\Core\RouteGroup;
use PiecesPHP\Core\Utilities\Helpers\DataTablesHelper;
use PiecesPHP\Core\Utilities\ReturnTypes\Operation;
use PiecesPHP\Core\Utilities\ReturnTypes\ResultOperations;
use Slim\Exception\NotFoundException;
use \Slim\Http\Request as Request;
use \Slim\Http\Response as Response;

/**
 * ArticleController.
 *
 * @package     PiecesPHP\BuiltIn\Article\Controllers
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @copyright   Copyright (c) 2019
 */
class ProyectController extends AdminPanelController
{

    const UPLOAD_DIR = 'proyects';
    const UPLOAD_DIR_TMP = 'proyects/tmp';
    const FORMAT_DATETIME = 'd-m-Y h:i:s';
    

    /**
     * $prefixParentEntity
     *
     * @var string
     */
    protected static $prefixParentEntity = 'built-in';
    /**
     * $prefixEntity
     *
     * @var string
     */
    protected static $prefixEntity = 'proyects';
    /**
     * $prefixSingularEntity
     *
     * @var string
     */
    protected static $prefixSingularEntity = 'proyect';
    /**
     * $title
     *
     * @var string
     */
    protected static $title = 'Proyecto';
    /**
     * $pluralTitle
     *
     * @var string
     */
    protected static $pluralTitle = 'Proyectos';

    /**
     * $uploadDir
     *
     * @var string
     */
    protected $uploadDir = '';
    /**
     * $uploadDir
     *
     * @var string
     */
    protected $uploadTmpDir = '';
    /**
     * $uploadDirURL
     *
     * @var string
     */
    protected $uploadDirURL = '';
    /**
     * $uploadDirTmpURL
     *
     * @var string
     */
    protected $uploadDirTmpURL = '';

    /**
     * __construct
     *
     * @return static
     */
    public function __construct()
    {
        parent::__construct(false); //No cargar ningún modelo automáticamente.
        

        $this->model = (new ProyectMapper)->getModel();
        set_title(self::$title . ' - ' . get_title());
        $this->uploadDir = append_to_url(get_config('upload_dir'), self::UPLOAD_DIR);
        $this->uploadTmpDir = append_to_url(get_config('upload_dir'), self::UPLOAD_DIR_TMP);
        $this->uploadDirURL = str_replace(base_url(), '', append_to_url(get_config('upload_dir_url'), self::UPLOAD_DIR));
        $this->uploadDirTmpURL = str_replace(base_url(), '', append_to_url(get_config('upload_dir_url'), self::UPLOAD_DIR_TMP));
    
    }

    /**
     * addForm
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return void
     */
    public function addForm(Request $request, Response $response, array $args)
    {
        import_swal2();

        set_custom_assets([
            base_url('statics/js/cropperHandler.js'),            
        ], 'js');

        $action = self::routeName('actions-add');
        $quill_proccesor_link = self::routeName('image-handler');
        $back_link = self::routeName('list');

        $data = [];
        $data['action'] = $action;
        $data['back_link'] = $back_link;
        
        $data['quill_proccesor_link'] = $quill_proccesor_link;
        $data['title'] = self::$title;
        $options_ejes = array_to_html_options(EjeMapper::allForSelect(), null);
        $data['options_ejes'] = $options_ejes;

        import_quilljs(['imageResize']);
		import_cropper();


        $this->render('panel/layout/header');
        $this->render('panel/' . self::$prefixParentEntity . '/' . self::$prefixSingularEntity . '/add-form', $data);
        $this->render('panel/layout/footer');
    }

    /**
     * editForm
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return void
     */
    public function editForm(Request $request, Response $response, array $args)
    {
        import_swal2();
        set_custom_assets([
            base_url('statics/js/cropperHandler.js'),
        ], 'js');

      
        $id = $request->getAttribute('id', null);
        $id = !is_null($id) && ctype_digit($id) ? (int) $id : null;

        $element = new ProyectMapper($id);

        if (!is_null($element->id)) {

            $action = self::routeName('actions-edit');
            $back_link = self::routeName('list');
            
            $quill_proccesor_link = self::routeName('image-handler');
            $options_ejes = array_to_html_options(EjeMapper::allForSelect(), $element->eje_tematico, true);

            $data = [];
            $data['action'] = $action;
            $data['element'] = $element;
            $data['back_link'] = $back_link;
            $data['options_ejes'] = $options_ejes;
            $data['quill_proccesor_link'] = $quill_proccesor_link;
            $data['title'] = self::$title;
            import_quilljs(['imageResize']);
			import_cropper();
      

            $this->render('panel/layout/header');
            $this->render('panel/' . self::$prefixParentEntity . '/' . self::$prefixSingularEntity . '/edit-form', $data);
            $this->render('panel/layout/footer');
        } else {
            throw new NotFoundException($request, $response);
        }
    }

    /**
     * list
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return void
     */
    function list(Request $request, Response $response, array $args) {

        $process_table = self::routeName('datatables');
        //$back_link = self::routeName();
        $back_link = get_route('admin');
        $add_link = self::routeName('forms-add');

        $data = [];
        $data['process_table'] = $process_table;
        $data['back_link'] = $back_link;
        $data['add_link'] = $add_link;
        $data['has_permissions_add'] = strlen($add_link) > 0;
        $data['title'] = self::$pluralTitle;

        $this->render('panel/layout/header');
        $this->render('panel/' . self::$prefixParentEntity . '/' . self::$prefixSingularEntity . '/list', $data);
        $this->render('panel/layout/footer');
    }

    /**
     * proyects
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
    public function proyects(Request $request, Response $response, array $args)
    {

        if ($request->isXhr()) {

            $query = $this->model->select();

            $query->execute();

            return $response->withJson($query->result());
        } else {
            throw new NotFoundException($request, $response);
        }
    }

    /**
     * single
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
    public function single(Request $request, Response $response, array $args)
    {

        if ($request->isXhr() || true) {

            $type = $request->getQueryParam('type', 'friendly_url');
            $proyect_value = $request->getAttribute('proyect', null);
            $exists = false;
            $proyect = null;

            if ($type == 'friendly_url') {
                $exists = ProyectMapper::existsByFriendlyURL($proyect_value);
            } elseif ($type == 'id') {
                $proyect_value = !is_null($proyect_value) && ctype_digit($proyect_value) ? $proyect_value : -1;
                $exists = ProyectMapper::existsByID((int) $proyect_value);
            }

            if ($exists) {
                $proyect = ProyectMapper::getBy($proyect_value, $type);
            }

            if ($proyect !== null) {
                return $response->withJson($proyect);
            } else {
                throw new NotFoundException($request, $response);
            }
        } else {
            throw new NotFoundException($request, $response);
        }
    }

    /**
     * proyectsDataTables
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
    public function proyectsDataTables(Request $request, Response $response, array $args)
    {

        if ($request->isXhr()) {
            $custom_order = [
                'fecha' => 'DESC',
            ];
            $columns_order = [
                'fecha',
                'id',
                'titulo',
                'ubicacion',
                'autor',
            ];

            $result = DataTablesHelper::process([
                'columns_order' => $columns_order,
                'custom_order' => $custom_order,
                'mapper' => new ProyectMapper(),
                'request' => $request,
                'on_set_data' => function ($e) {

                    $buttonEdit = new HtmlElement('a', 'Editar');
                    $buttonEdit->setAttribute('class', "ui button green");
                    $buttonEdit->setAttribute('href', self::routeName('forms-edit', [
                        'id' => $e->id,
                    ]));

                    if ($buttonEdit->getAttributes(false)->offsetExists('href')) {
                        $href = $buttonEdit->getAttributes(false)->offsetGet('href');
                        if (strlen(trim($href->getValue())) < 1) {
                            $buttonEdit = '';
                        }
                    }

                    $mapper = new ProyectMapper($e->id);

                    return [
                        $mapper->id,
                        $mapper->titulo,
                        $mapper->fecha,
                        $mapper->ubicacion,
                        $mapper->autor,
                        (string) $buttonEdit,
                    ];
                },
            ]);

            return $response->withJson($result->getValues());
        } else {
            throw new NotFoundException($request, $response);
        }
    }

    /**
     * action
     *
     * Creación/Edición
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
    public function action(Request $request, Response $response, array $args)
    {

        $id = $request->getParsedBodyParam('id', -1);
        $title = $request->getParsedBodyParam('title', null);
        $aliados = $request->getParsedBodyParam('aliados', null);
        $ubicacion = $request->getParsedBodyParam('ubicacion', null);
        $content = $request->getParsedBodyParam('description', null);
        $ejes = $request->getParsedBodyParam('eje', null);
        $autor = $request->getParsedBodyParam('autor', null);
        $fecha = $request->getParsedBodyParam('fecha', null);
        $folder = (new \DateTime)->format('Y/m/d/') . str_replace('.', '', uniqid());
        $imageProject="";
       
        $is_edit = $id !== -1;

        $valid_params = !in_array(null, [
            $title,
            $aliados,
            $content,
            $ejes,
            $autor,
        ]);

        

        $operation_name = $is_edit ? __('projectBackend', 'Modificar Proyecto') : __('projectBackend', 'Agregar Proyecto');

        $result = new ResultOperations([
            new Operation($operation_name),
        ], $operation_name);

        $result->setValue('redirect', false);
        $result->setValue('in', $request->getParsedBody());

        $error_parameters_message = __('projectBackend', 'Los parámetros recibidos son erróneos.');
        $unknow_error_message = __('projectBackend', 'El proyecto que intenta modificar no existe');
        $success_create_message = __('projectBackend', 'Proyecto creado.');
        $success_edit_message = __('projectBackend', 'Datos guardados.');
        $unknow_error_message = __('projectBackend', 'Ha ocurrido un error desconocido.');
        $is_duplicate_message = __('projectBackend', 'Ya existe un proyecto con ese nombre en la categoría seleccionada.');
        $not_exists_message = __('projectBackend', 'El idioma que intenta usar no existe.');

        $redirect_url_on_create = self::routeName('list');

        if ($valid_params) {

            $title = clean_string($title);
            $friendly_url = ProyectMapper::generateFriendlyURL($title, $id);
            $content = clean_string($content);

            $is_duplicate = ProyectMapper::isDuplicate($title, $friendly_url, $id);

            if (!$is_duplicate) {

                if (!$is_edit) {

                    $mapper = new ProyectMapper();

                    try {

                        $mapper->eje_tematico = ' ';
                        $mapper->aliados = $aliados;
                        $mapper->autor = $autor;
                        $mapper->eje_tematico = $ejes;
                        $mapper->ubicacion = $ubicacion;
                        $mapper->titulo = $title;
                        $mapper->friendly_url = $friendly_url;
                        $mapper->contenido = $content;
                        $mapper->fecha = $fecha;
                        $mapper->folder = $folder;

                        //Imagenes
						$imageProject=self::handlerUploadImage('portada',$folder);

						$mapper->portada=$imageProject;

                        $saved = $mapper->save();

                        if ($saved) {

                            $mapper->id = $mapper->getLastInsertID();
                            $this->moveTemporaryImages($mapper);

                            $result->setMessage($success_create_message)
                                ->operation($operation_name)
                                ->setSuccess(true);

                            $result->setValue('redirect', true);
                            $result->setValue('redirect_to', $redirect_url_on_create);
                        } else {
                            $result->setMessage($unknow_error_message);
                        }
                    } catch (\Exception $e) {
                        $result->setMessage($e->getMessage());
                    }
                } else {

                    $mapper = new ProyectMapper((int) $id);
                    $exists = !is_null($mapper->id);

                    if ($exists) {

                        try {

                            $oldText = $mapper->contenido;
                            $mapper->aliados = $aliados;
                            $mapper->autor = $autor;
                            $mapper->eje_tematico = $ejes;
                            $mapper->ubicacion = $ubicacion;
                            $mapper->titulo = $title;
                            $mapper->friendly_url = $friendly_url;
                            $mapper->contenido = $content;
                            $mapper->fecha = $fecha;


                            $exist_file=$mapper->folder;
							


							if($exist_file!=""){



								$imageProject= self::handlerUploadImage(
									"portada",
									$mapper->folder,
									$mapper->portada
								);

							}else{

								$mapper->folder=$folder;
								$imageProject= self::handlerUploadImage('portada',$folder);
							}

                            
							
							$mapper->portada = strlen($imageProject)> 0 ? $imageProject : $mapper->portada;
							

                            $updated = $mapper->update();

                            if ($updated) {

                                $this->moveTemporaryImages($mapper, $oldText);

                                $result->setValue('reload', true);

                                $result->setMessage($success_edit_message)
                                    ->operation($operation_name)
                                    ->setSuccess(true);
                            } else {
                                $result->setMessage($unknow_error_message);
                            }
                        } catch (\Exception $e) {
                            $result->setMessage($e->getMessage());
                        }
                    } else {
                        $result->setMessage($not_exists_message);
                    }
                }
            } else {

                $result->setMessage($is_duplicate_message);
            }
        } else {
            $result->setMessage($error_parameters_message);
        }

        return $response->withJson($result);
    }

     /**
     * handlerUploadImage
     *
     * @param string $nameOnFiles
     * @param string $folder
     * @param string $currentRoute
     * @param bool $setNameByInput
     * @return string
     */
    protected static function handlerUploadImage(string $nameOnFiles, string $folder, string $currentRoute = null, bool $setNameByInput = true)
    {
        $handler = new FileUpload($nameOnFiles, [
            FileValidator::TYPE_ALL_IMAGES,
        ]);
        $valid = false;
        $relativeURL = '';

        $name = 'file_' . uniqid();
        $oldFile = null;

        if ($handler->hasInput()) {

            try {

                $valid = $handler->validate();

                $uploadDirPath = (new static )->uploadDir;
                $uploadDirRelativeURL = (new static )->uploadDirURL;

                if ($setNameByInput && $valid) {

                    $name = $_FILES[$nameOnFiles]['name'];
                    $lastPointIndex = strrpos($name, '.');

                    if ($lastPointIndex !== false) {
                        $name = substr($name, 0, $lastPointIndex);
                    }

                }

                if (!is_null($currentRoute)) {
                    //Si ya existe
                    $oldFile = append_to_url(basepath(), $currentRoute);
                    $oldFile = file_exists($oldFile) ? $oldFile : null;
                }

                $uploadDirPath = append_to_url($uploadDirPath, $folder);
                $uploadDirRelativeURL = append_to_url($uploadDirRelativeURL, $folder);

                if ($valid) {

                    $locations = $handler->moveTo($uploadDirPath, $name, null, false, true);

                    if (count($locations) > 0) {

                        $url = $locations[0];
                        $nameCurrent = basename($url);
                        $relativeURL = trim(append_to_url($uploadDirRelativeURL, $nameCurrent), '/');

                        //Eliminar archivo anterior
                        if (!is_null($oldFile)) {

                            if (basename($oldFile) != $nameCurrent) {
                                unlink($oldFile);
                            }

                        }

                        //Se elimina cualquier otro archivo
                        foreach ($locations as $file) {
                            if ($url != $file) {
                                if (is_string($file) && file_exists($file)) {
                                    unlink($file);
                                }
                            }
                        }

                    }

                } else {
                    throw new \Exception(implode('<br>', $handler->getErrorMessages()));
                }

            } catch (\Exception $e) {
                throw new \Exception($e->getMessage());
            }

        }

        return $relativeURL;
    }

    /**
     * quillImageHandler
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
    public function quillImageHandler(Request $request, Response $response, array $args)
    {
        $files_uploaded = $request->getUploadedFiles();
        $image = isset($files_uploaded['image']) ? $files_uploaded['image'] : null;

        $result = new ResultOperations([
            'uploadImage' => new Operation('uploadImage'),
        ]);

        $result->setValue('path', null);

        if (!is_null($image)) {

            $images = is_array($image) ? $image : [$image];

            foreach ($images as $image) {

                if ($image->getError() === UPLOAD_ERR_OK) {

                    $filename = move_uploaded_file_to($this->uploadTmpDir, $image, uniqid());

                    $url = append_to_url(base_url($this->uploadDirTmpURL), $filename);

                    if (!is_null($filename)) {
                        $result
                            ->operation('uploadImage')
                            ->setMessage(__('projectBackend', 'Imagen subida'))
                            ->setSuccess(true);
                        $result->setValue('path', $url);
                    } else {
                        $result
                            ->operation('uploadImage')
                            ->setMessage(__('projectBackend', 'La imagen no pudo ser subida, intente después.'));
                    }
                }
            }
        } else {
            $result
                ->operation('uploadImage')
                ->setMessage(__('projectBackend', 'No se ha subido ninguna imagen.'));
        }

        return $response->withJson($result);
    }

    /**
     * moveTemporaryImages
     *
     * @param ProyectMapper $entity
     * @param string $oldText
     * @return void
     */
    protected function moveTemporaryImages(ProyectMapper &$entity, string $oldText = null)
    {
        $imagesOnText = [];
        $imagesOnOldText = [];
        $currentImagesOnText = [];

        $isEdit = !is_null($oldText) && strlen($oldText) > 0;
        $id = $entity->id;

        $regex = '/https?\:\/\/[^\",]+/i';

        preg_match_all($regex, $entity->contenido, $imagesOnText);

        $imagesOnText = $imagesOnText[0];

        if (count($imagesOnText) > 0) {

            foreach ($imagesOnText as $url) {

                if (strpos($url, $this->uploadDirTmpURL) !== false) {

                    $filename = basename($url);

                    $oldPath = append_to_url($this->uploadTmpDir, "$filename");

                    $newFolder = append_to_url($this->uploadDir, "$id");

                    $newPath = append_to_url($newFolder, "$filename");

                    if (!file_exists($newFolder)) {
                        make_directory($newFolder);
                    }

                    if (file_exists($oldPath)) {
                        rename($oldPath, $newPath);
                    }

                    $_url = append_to_url($this->uploadDirURL, "$id/$filename");
                    $_url = trim($_url, '/');

                    $entity->contenido = str_replace($url, $_url, $entity->contenido);

                    $currentImagesOnText[] = $_url;
                } elseif (strpos($url, $this->uploadDirURL) !== false) {

                    $currentImagesOnText[] = trim($url, '/');
                }
            }
        }

        $updated = $entity->update();

        if ($isEdit) {

            preg_match_all($regex, $oldText, $imagesOnOldText);
            $imagesOnOldText = $imagesOnOldText[0];

            if ($updated && count($imagesOnOldText) > 0) {

                foreach ($imagesOnOldText as $url) {

                    if (!in_array($url, $currentImagesOnText)) {

                        $filename = str_replace($this->uploadDirURL, '', $url);

                        $path = append_to_url($this->uploadDir, $filename);

                        if (file_exists($path)) {
                            unlink($path);
                        }
                    }
                }
            }
        }
    }

    /**
     * routeName
     *
     * @param string $name
     * @param array $params
     * @param bool $silentOnNotExists
     * @return string
     */
    public static function routeName(string $name = null, array $params = [], bool $silentOnNotExists = false)
    {
        if (!is_null($name)) {
            $name = trim($name);
            $name = strlen($name) > 0 ? "-{$name}" : '';
        }

        $name = !is_null($name) ? self::$prefixParentEntity . '-' . self::$prefixEntity . $name : self::$prefixParentEntity;

        $allowed = false;
        $current_user = get_config('current_user');

        if ($current_user != false) {
            $allowed = Roles::hasPermissions($name, (int) $current_user->type);
        } else {
            $allowed = true;
        }

        if ($allowed) {
            return get_route(
                $name,
                $params,
                $silentOnNotExists
            );
        } else {
            return '';
        }
    }

    /**
     * deleteOrphanFiles
     *
     * @return void
     */
    protected function deleteOrphanFiles()
    {
        $temporary_directory = new DirectoryObject($this->uploadTmpDir);
        $temporary_directory->process();
        $temporary_directory->delete();
    }

    /**
     * routes
     *
     * @param RouteGroup $group
     * @return RouteGroup
     */
    public static function routes(RouteGroup $group)
    {
        if (PIECES_PHP_BLOG_ENABLED) {

            $from = \DateTime::createFromFormat('d-m-Y h:i:s A', date('d-m-Y') . ' 2:00:00 AM');
            $to = \DateTime::createFromFormat('d-m-Y h:i:s A', date('d-m-Y') . ' 4:00:00 AM');
            $now = new \DateTime();
            $valid_interval = $now >= $from && $now <= $to;

            if ($valid_interval) {
                $instance = new static;
                $instance->deleteOrphanFiles();
            }

            $routes = [];

            $groupSegmentURL = $group->getGroupSegment();

            $lastIsBar = last_char($groupSegmentURL) == '/';
            $startRoute = $lastIsBar ? '' : '/';

            $permisos_estados_gestion = [
                UsersModel::TYPE_USER_ROOT,
                UsersModel::TYPE_USER_ADMIN,
            ];

            $group->active(PIECES_PHP_BLOG_ENABLED);
            $group->register($routes);

            //Rutas básicas
            $group->register(
                self::genericManageRoutes($startRoute, self::$prefixParentEntity, self::class, self::$prefixEntity, $permisos_estados_gestion, true)
            );

            //Otras rutas
            $namePrefix = self::$prefixParentEntity . '-' . self::$prefixEntity;
            $startRoute .= self::$prefixEntity;
            $group->register([
                new Route(
                    "{$startRoute}/list/{eje}",
                    self::class . ":proyectsByCategory",
                    "{$namePrefix}-ajax-all-eje",
                    'GET'
                ),
            ]);
            $group->register([
                new Route(
                    "{$startRoute}/single/{proyect}",
                    self::class . ":single",
                    "{$namePrefix}-ajax-single",
                    'GET'
                ),
            ]);

            //Rutas categorías
        }

        return $group;
    }

    /**
     * genericManageRoutes
     *
     * @param string $startRoute
     * @param string $namePrefix
     * @param string $handler
     * @param string $uriPrefix
     * @param array $rolesAllowed
     * @return Route[]
     */
    protected static function genericManageRoutes(string $startRoute, string $namePrefix, string $handler, string $uriPrefix, array $rolesAllowed = [], bool $withQuillHandler = false)
    {
        $namePrefix .= '-' . $uriPrefix;
        $startRoute .= $uriPrefix;

        $routes = [
            new Route(
                "{$startRoute}",
                "{$handler}:{$uriPrefix}",
                "{$namePrefix}-ajax-all",
                'GET'
            ),
            new Route(
                "{$startRoute}/datatables[/]",
                "{$handler}:{$uriPrefix}DataTables",
                "{$namePrefix}-datatables",
                'GET'
            ),
            new Route(
                "{$startRoute}/list[/]",
                "{$handler}:list",
                "{$namePrefix}-list",
                'GET'
            ),
            new Route(
                "{$startRoute}/forms/add[/]",
                "{$handler}:addForm",
                "{$namePrefix}-forms-add",
                'GET',
                true,
                null,
                $rolesAllowed
            ),
            new Route(
                "{$startRoute}/action/add[/]",
                "{$handler}:action",
                "{$namePrefix}-actions-add",
                'POST',
                true,
                null,
                $rolesAllowed
            ),
            new Route(
                "{$startRoute}/forms/edit/{id}[/]",
                "{$handler}:editForm",
                "{$namePrefix}-forms-edit",
                'GET',
                true,
                null,
                $rolesAllowed
            ),
            new Route(
                "{$startRoute}/action/edit[/]",
                "{$handler}:action",
                "{$namePrefix}-actions-edit",
                'POST',
                true,
                null,
                $rolesAllowed
            ),
        ];

        if ($withQuillHandler) {
            $routes[] = new Route(
                "{$startRoute}/quill-image-upload[/]",
                "{$handler}:quillImageHandler",
                "{$namePrefix}-image-handler",
                'POST',
                true,
                null,
                $rolesAllowed
            );
        }

        return $routes;
    }
}