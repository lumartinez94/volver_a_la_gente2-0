<?php
/**
 * ArticleMapper.php
 */

namespace PiecesPHP\BuiltIn\Proyect\Mappers;

use App\Model\UsersModel;
use PiecesPHP\Core\BaseEntityMapper;

/**
 * ArticleMapper.
 *
 * @package     PiecesPHP\BuiltIn\Article\Mappers
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @copyright   Copyright (c) 2019
 * @property int $id
 * @property int|UsersModel $author
 * @property int|CategoryMapper $category
 * @property string $title
 * @property string $friendly_url
 * @property string $content
 * @property array|object|null $meta
 * @property string|\DateTime|null $start_date
 * @property string|\DateTime|null $end_date
 * @property string|\DateTime $created
 * @property string|\DateTime $updated
 */
class ProyectMapper extends BaseEntityMapper
{
    const TABLE = 'proyectos';

    /**
     * @var string $table
     */
    protected $table = self::TABLE;

    protected $fields = [
        'id' => [
            'type' => 'int',
            'primary_key' => true,
        ],
        'eje_tematico' => [
            'type' => 'json',
        ],
        'titulo' => [
            'type' => 'varchar',
        ],
        'portada' => [
            'type' => 'varchar',
        ],
        'folder' => [
            'type' => 'varchar',
            'length' => 225,
		],
        'friendly_url' => [
            'type' => 'varchar',
        ],
        'fecha' => [
            'type' => 'int',
        ],
        'aliados' => [
            'type' => 'text',
        ],
        'ubicacion' => [
            'type' => 'varchar',
        ],
        'contenido' => [
            'type' => 'text',
        ],
        'autor' => [
            'type' => 'varchar',
        ],
    ];
    /**
     * __construct
     *
     * @param int $value
     * @param string $field_compare
     * @return static
     */
    public function __construct(int $value = null, string $field_compare = 'primary_key')
    {
        parent::__construct($value, $field_compare);
    }

    /**
     * getVerboseDate
     * @return string
     */
    public function getVerboseDate()
    {
        return $this->fecha->format('d') . ' de ' . num_month_to_text($this->fecha->format('d-m-Y')) . ' de ' . $this->fecha->format('Y');
    }

    /**
     * all
     *
     * @param bool $as_mapper
     *
     * @return static[]|array
     */
    public static function all(bool $as_mapper = false, int $page = null, int $perPage = null)
    {
        $model = self::model();

        $model->select()->orderBy('fecha DESC')->execute(false, $page, $perPage);

        $result = $model->result();

        if ($as_mapper) {
            $result = array_map(function ($e) {
                return new static($e->id);
            }, $result);
        }

        return $result;
    }

    /**
     * getProjectByAction
     *
     * @param bool $as_mapper
     *
     * @return static[]|array
     */
    public static function getProjectByAction(string $name, bool $as_mapper = false, int $page = null, int $perPage = null)
    {   
        $name = json_decode($name);
        $where = "";
        for ($i=0; $i < sizeof($name) ; $i++) { 
            $item = $name[$i];
            if ($i+1 == sizeof($name)) {
                $where .= "titulo = '$item'";
            }else{
                $where .= "titulo = '$item' OR ";
            }
        }
        
        $model = self::model();
        if ($where != "") {
            $model->select()->where($where)->orderBy('fecha DESC')->execute(false, $page, $perPage);
            $result = $model->result();
            
        }else{
            $result = "ninguno";
        }


        if ($as_mapper) {
            $result = array_map(function ($e) {
                return new static($e->id);
            }, $result);
        }

        return $result;
    }

    /**
     * all
     *
     * @param bool $as_mapper
     * @param int $page
     * @param int $perPage
     *
     * @return static[]|array
     */
    public static function getActionsByEje(string $name, bool $as_mapper = false, int $page = null, int $perPage = null)
    {   

       
        $model = self::model();

        $model->select()->where("eje_tematico LIKE '%$name%'")->orderBy('fecha DESC')->execute(false, $page, $perPage);

        $result = $model->result();

        if ($as_mapper) {
            $result = array_map(function ($e) {
                return new static($e->id);
            }, $result);
        }

        return $result;
    }

    /**
     * allForSelect
     *
     * @param string $defaultLabel
     * @param string $defaultValue
     * @return array
     */
    public static function allForSelect(string $defaultLabel = 'Ninguno', string $defaultValue = 'ninguno')
    {
        $options = [];
        $options[$defaultValue] = $defaultLabel;

        array_map(function ($e) use (&$options) {
            $options[$e->titulo] = $e->titulo;
        }, self::all());

        return $options;
    }

    /**
     * allByCategory
     *
     * @param int $friendly_url
     * @param bool $as_mapper
     * @return object|static[]
     */
    public static function allByCategory(int $category, bool $as_mapper = false)
    {
        $model = self::model();

        $where = [
            "category = $category",
        ];
        $where = trim(implode(' ', $where));

        $model->select()->where($where);

        $model->execute();

        $result = $model->result();

        if ($as_mapper) {
            $result = array_map(function ($e) {
                return new static($e->id);
            }, $result);
        }

        return $result;
    }

    /**
     * getBy
     *
     * @param mixed $value
     * @param string $column
     * @param boolean $as_mapper
     * @return static|object|null
     */
    public static function getBy($value, string $column = 'id', bool $as_mapper = false)
    {
        $model = self::model();

        $where = [
            $column => $value,
        ];

        $model->select()->where($where);

        $model->execute();

        $result = $model->result();

        $result = count($result) > 0 ? $result[0] : null;

        if (!is_null($result) && $as_mapper) {
            $result = new static($result->id);
        }

        return $result;
    }

    /**
     * existsByID
     *
     * @param int $id
     * @return bool
     */
    public static function existsByID(int $id)
    {
        $model = self::model();

        $where = [
            "id = $id",
        ];
        $where = trim(implode(' ', $where));

        $model->select()->where($where);

        $model->execute();

        $result = $model->result();

        return count($result) > 0;
    }

    /**
     * existsByFriendlyURL
     *
     * @param string $friendly_url
     * @return bool
     */
    public static function existsByFriendlyURL(string $friendly_url)
    {
        $model = self::model();

        $where = [
            "friendly_url = '$friendly_url'",
        ];
        $where = trim(implode(' ', $where));

        $model->select()->where($where);

        $model->execute();

        $result = $model->result();

        return count($result) > 0;
    }

    /**
     * getByFriendlyURL
     *
     * @param string $friendly_url
     * @param bool $as_mapper
     * @return object|static
     */
    public static function getByFriendlyURL(string $friendly_url, bool $as_mapper = false)
    {
        $model = self::model();

        $where = [
            "friendly_url = '$friendly_url'",
        ];
        $where = trim(implode(' ', $where));

        $model->select()->where($where);

        $model->execute();

        $result = $model->result();

        if (count($result) > 0) {
            $result = $result[0];
            if ($as_mapper) {
                $result = new static($result->id);
            }
        }

        return $result;
    }

    /**
     * isDuplicate
     *
     * @param string $title
     * @param string $friendly_url
     * @param int $category
     * @param int $ignore_id
     * @return bool
     */
    public static function isDuplicate(string $title, string $friendly_url, int $ignore_id)
    {
        $model = self::model();

        $where = [
            "(titulo = '$title'",
            "OR friendly_url = '$friendly_url')",
            "AND id != $ignore_id",
        ];
        $where = trim(implode(' ', $where));

        $model->select()->where($where);

        $model->execute();

        $result = $model->result();

        return count($result) > 0;
    }

    /**
     * friendlyURLCount
     *
     * @param string $friendly_url
     * @param int $ignore_id
     * @return bool
     */
    public static function friendlyURLCount(string $friendly_url, int $ignore_id)
    {

        $model = self::model();

        $where = [
            'friendly_url' => $friendly_url,
            'id' => [
                '!=' => $ignore_id,
            ],
        ];

        $model->select('COUNT(id) AS total')->where($where)->execute();

        $result = $model->result();

        return count($result) > 0 ? (int) $result[0]->total : 0;
    }

    /**
     * generateFriendlyURL
     *
     * @param string $friendly_url
     * @param int $ignore_id
     * @return string
     */
    public static function generateFriendlyURL(string $name, int $ignore_id)
    {
        $friendly_url = friendly_url($name);
        $count_friendly_url = self::friendlyURLCount($friendly_url, $ignore_id);

        if ($count_friendly_url > 0) {
            $friendly_url = $friendly_url . '-' . $count_friendly_url;
        }

        return $friendly_url;
    }

    /**
     * model
     *
     * @return BaseModel
     */
    public static function model()
    {
        return (new static )->getModel();
    }

    /**
     * catchTitle
     *
     * @param mixed $value
     * @param string $column
     * @param boolean $as_mapper
     * @return static|object|null
     */
    public static function catchTitle($id, bool $as_mapper = false)
    {
        $model = self::model();

        $where = [
            'id' => $id,
        ];

        $model->select(['titulo'])->where($where);

        $model->execute();

        $result = $model->result();

        $result = count($result) > 0 ? $result[0] : null;

        if (!is_null($result) && $as_mapper) {
            $result = new static($result->titulo);
        }

        return $result;
    }
}