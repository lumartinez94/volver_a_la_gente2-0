<?php
/**
 * ArticleMapper.php
 */

namespace PiecesPHP\BuiltIn\Action\Mappers;

use App\Model\UsersModel;
use PiecesPHP\Core\BaseEntityMapper;

/**
 * ArticleMapper.
 *
 * @package     PiecesPHP\BuiltIn\Article\Mappers
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @copyright   Copyright (c) 2019
 * @property int $id
 * @property int|UsersModel $author
 * @property int|CategoryMapper $category
 * @property string $title
 * @property string $friendly_url
 * @property string $content
 * @property array|object|null $meta
 * @property string|\DateTime|null $start_date
 * @property string|\DateTime|null $end_date
 * @property string|\DateTime $created
 * @property string|\DateTime $updated
 */
class ActionMapper extends BaseEntityMapper
{
    const TABLE = 'acciones';

    /**
     * @var string $table
     */
    protected $table = self::TABLE;

    protected $fields = [
        'id' => [
            'type' => 'int',
            'primary_key' => true,
        ],
        'proyectos' => [
            'type' => 'json',
        ],
        'titulo' => [
            'type' => 'varchar',
        ],
        'portada' => [
            'type' => 'varchar',
        ],
        'portada_sup' => [
            'type' => 'varchar',
        ],
        
        
        'friendly_url' => [
            'type' => 'varchar',
        ],
        
        'fecha' => [
            'type' => 'datetime',
        ],
        'contenido' => [
            'type' => 'text',
        ],
        'autor' => [
            'type' => 'varchar',
        ],
        'categoria' => [
            'type' => 'varchar',
        ],
        
        'folder' => [
            'type' => 'varchar',
            'length' => 225,
		],
    ];
    /**
     * __construct
     *
     * @param int $value
     * @param string $field_compare
     * @return static
     */
    public function __construct(int $value = null, string $field_compare = 'primary_key')
    {
        parent::__construct($value, $field_compare);
    }

    /**
     * getVerboseDate
     * @return string
     */
    public function getVerboseDate()
    {
        return $this->fecha->format('d') . ' de ' . num_month_to_text($this->fecha->format('d-m-Y')) . ' de ' . $this->fecha->format('Y');
    }

    /**
     * all
     *
     * @param bool $as_mapper
     * @param int $page
     * @param int $perPage
     *
     * @return static[]|array
     */
    public static function all(bool $as_mapper = false, int $page = null, int $perPage = null)
    {
        $model = self::model();

        $model->select()->orderBy('id DESC')->execute(false, $page, $perPage);

        $result = $model->result();

        if ($as_mapper) {
            $result = array_map(function ($e) {
                return new static($e->id);
            }, $result);
        }
        return $result;
    }

    /**
     * all
     *
     * @param bool $as_mapper
     * @param int $page
     * @param int $perPage
     *
     * @return static[]|array
     */
    public static function getActionsByProjects(string $name, bool $as_mapper = false, int $page = null, int $perPage = null)
    {   

       
        $model = self::model();

        $model->select()->where("proyectos LIKE '%$name%'")->orderBy('fecha DESC')->execute(false, $page, $perPage);

        $result = $model->result();

        if ($as_mapper) {
            $result = array_map(function ($e) {
                return new static($e->id);
            }, $result);
        }

        return $result;
    }
    
    



    /**
     * allBy
     *
     * @param mixed $value
     * @param string $column
     * @param boolean $as_mapper
     * @param int $page
     * @param int $perPage
     * @return static|object|null
     */
    public static function allBy($value, string $column = 'id', bool $as_mapper = false, int $page = null, int $perPage = null)
    {
        $model = self::model();

        $where = [
            $column => $value,
        ];

        $model->select()->where($where);

        $model->execute(false, $page, $perPage);

        $result = $model->result();

        if ($as_mapper) {
            $result = array_map(function ($e) {
                return new static($e->id);
            }, $result);
        }

        return $result;
    }

    /**
     * search
     *
     * @param mixed $value
     * @param string $column
     * @param boolean $as_mapper
     * @param int $page
     * @param int $perPage
     * @return static|object|null
     */
    public static function search($value, bool $as_mapper = false, int $page = null, int $perPage = null)
    {
        $model = self::model();

        $model->select()->where('titulo LIKE "%' . $value . '%"');

        $model->execute(false, $page, $perPage);

        $result = $model->result();

        if ($as_mapper) {
            $result = array_map(function ($e) {
                return new static($e->id);
            }, $result);
        }

        return $result;
    }

    /**
     * allForSelect
     *
     * @param string $defaultLabel
     * @param string $defaultValue
     * @return array
     */
    public static function allForSelect(string $defaultLabel = 'Acciones', string $defaultValue = '')
    {
        $options = [];
        $options[$defaultValue] = $defaultLabel;

        array_map(function ($e) use (&$options) {
            $options[$e->id] = $e->nombre;
        }, self::all());

        return $options;
    }

    /**
     * getBy
     *
     * @param mixed $value
     * @param string $column
     * @param boolean $as_mapper
     * @return static|object|null
     */
    public static function getBy($value, string $column = 'id', bool $as_mapper = false)
    {
        $model = self::model();

        $where = [
            $column => $value,
        ];

        $model->select()->where($where);

        $model->execute();
        var_dump($model);

        $result = $model->result();

        $result = count($result) > 0 ? $result[0] : null;

        if (!is_null($result) && $as_mapper) {
            $result = new static($result->id);
        }

        return $result;
    }

    /**
     * existsByID
     *
     * @param int $id
     * @return bool
     */
    public static function existsByID(int $id)
    {
        $model = self::model();

        $where = [
            "id = $id",
        ];
        $where = trim(implode(' ', $where));

        $model->select()->where($where);

        $model->execute();

        $result = $model->result();

        return count($result) > 0;
    }

    /**
     * existsByFriendlyURL
     *
     * @param string $friendly_url
     * @return bool
     */
    public static function existsByFriendlyURL(string $friendly_url)
    {$model = self::model();

        $where = [
            "friendly_url = '$friendly_url'",
        ];
        $where = trim(implode(' ', $where));
        $model->select()->where($where);

        $model->execute();

        $result = $model->result();

        return count($result) > 0;
    }

    /**
     * getByFriendlyURL
     *
     * @param string $friendly_url
     * @param bool $as_mapper
     * @return object|static
     */
    public static function getByFriendlyURL(string $friendly_url, bool $as_mapper = false)
    {
        $model = self::model();

        $where = [
            "friendly_url = '$friendly_url'",
        ];
        $where = trim(implode(' ', $where));

        $model->select()->where($where);

        $model->execute();

        $result = $model->result();

        if (count($result) > 0) {
            $result = $result[0];
            if ($as_mapper) {
                $result = new static($result->id);
            }
        }

        return $result;
    }

    /**
     * isDuplicate
     *
     * @param string $title
     * @param string $friendly_url
     * @param int $category
     * @param int $ignore_id
     * @return bool
     */
    public static function isDuplicate(string $title, string $friendly_url, int $ignore_id)
    {
        $model = self::model();

        $where = [
            "(titulo = '$title'",
            "OR friendly_url = '$friendly_url')",
            "AND id != $ignore_id",
        ];
        $where = trim(implode(' ', $where));

        $model->select()->where($where);

        $model->execute();

        $result = $model->result();

        return count($result) > 0;
    }
    public function bringActions()
    {
        $model = $this->getModel();
        $model->resetAll();
        $model->select(['id', 'proyectos', 'titulo', 'autor', 'portada'])->execute(true);
        $result = $model->result();

        return $result;
    }

    public function actionDetails($id)
    {
        $model = $this->getModel();
        $model->resetAll();
        $model->select(['id', 'proyectos', 'titulo', 'fecha', 'autor', 'contenido', 'portada'])->where('id = ' . $id)->execute();
        $result = $model->result();
        return $result;
    }
    /**
     * friendlyURLCount
     *
     * @param string $friendly_url
     * @param int $ignore_id
     * @return bool
     */
    public static function friendlyURLCount(string $friendly_url, int $ignore_id)
    {

        $model = self::model();

        $where = [
            'friendly_url' => $friendly_url,
            'id' => [
                '!=' => $ignore_id,
            ],
        ];

        $model->select('COUNT(id) AS total')->where($where)->execute();

        $result = $model->result();

        return count($result) > 0 ? (int) $result[0]->total : 0;
    }

    /**
     * generateFriendlyURL
     *
     * @param string $friendly_url
     * @param int $ignore_id
     * @return string
     */
    public static function generateFriendlyURL(string $name, int $ignore_id)
    {
        $friendly_url = friendly_url($name);
        $count_friendly_url = self::friendlyURLCount($friendly_url, $ignore_id);

        if ($count_friendly_url > 0) {
            $friendly_url = $friendly_url . '-' . $count_friendly_url;
        }

        return $friendly_url;
    }

    /**
     * model
     *
     * @return \PiecesPHP\Core\BaseModel
     */
    public static function model()
    {
        return (new static )->getModel();
    }
}